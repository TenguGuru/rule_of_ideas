import { Board } from './board.model';
import { Fielded } from "./fielded.interface";
import { Thought } from './thought.interface';
import { User } from "./user.model";

export class Idea implements Fielded,Thought {
  getFields: Function;
  getClass: Function;
  id: string;
  number: number;
  text: string;
  title: string;
  author: User;
  status:number;
  board:Board;
  static getFields() {
    return ["id", "number", "text", "title", "author","status","board"];
  }
  static getClass(){
    return 'idea';
  }
}
