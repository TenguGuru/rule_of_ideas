
import { User } from "./user.model";
import { Board, Idea } from '.';
import { Initiative } from './initiative.model';

export class InitiativeIdeas{
id:string;
number:number;
initiative:Initiative;
author:User;
idea:Idea;
relationship:number;
}