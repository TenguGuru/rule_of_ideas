export interface Fielded {
  getFields: Function;
  getClass: Function;
}
