import {Fielded} from "./fielded.interface";
import {Initiative, Party} from "./index";

export class PartyIdea implements Fielded{
  getFields: Function;
  getClass: Function;
  id: string;
  initiative: Initiative;
  party: Party;

  static getFields(){
    return ["id", "initiative", "party"];
  }

  static getClass(){
    return "PartyInitiative";
  }
}
