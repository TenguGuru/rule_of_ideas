import { Board } from './board.model';
import { User } from "./user.model";

export interface Thought{
  id: string;
  number: number;
  text: string;
  title: string;
  author: User;
  status:number;
  board:Board;
}
