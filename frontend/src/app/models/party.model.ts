import { Board } from './board.model';
import { Fielded } from "./fielded.interface";
import { User } from './user.model';

export class Party implements Fielded {
  getFields: Function;
  getClass: Function;
  id: number;
  color: string;
  title: string;
  creator:User;
  leader:User;
  board:Board;
  ideaLoyalty:number;
  initiativeLoyalty:number;
  electDate:Date;
  createDate:Date;
  static getFields() {
    return ["id", "color", "title","creator","leader","board"];
  }
  static getClass(){
    return "Party";
  }
}
