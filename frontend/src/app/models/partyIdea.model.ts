import {Fielded} from "./fielded.interface";
import {Idea, Party} from "./index";

export class PartyIdea implements Fielded{
  getFields: Function;
  getClass: Function;
  id: string;
  idea: Idea;
  party: Party;

  static getFields(){
    return ["id", "idea", "party"];
  }

  static getClass(){
    return "PartyIdea";
  }
}
