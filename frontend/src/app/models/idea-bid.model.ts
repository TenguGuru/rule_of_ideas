import { Party } from ".";
import { Fielded } from "./fielded.interface";
import { ThoughtBid } from './thought-bid.interface';
import { Thought } from "./thought.interface";

export class IdeaBid implements Fielded, ThoughtBid{
  getFields: Function;
  getClass: Function;
  id: string;
  thought: Thought;
  party: Party;
  loyaltyPercent: number;
  attempt: number;

  static getFields() {
    return ["id", "thoughtId", "partyId", "loyaltyPercent", "attempt"];
  }
  static getClass(){
    return 'ideaBid';
  }
}
