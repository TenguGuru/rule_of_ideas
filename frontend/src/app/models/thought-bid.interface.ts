import { Party } from ".";
import { Thought } from "./thought.interface";

export interface ThoughtBid{
  id: string;
  thought: Thought;
  party: Party;
  loyaltyPercent: number;
  attempt: number;
}
