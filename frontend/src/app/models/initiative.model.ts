import { Fielded } from "./fielded.interface";
import { User } from "./user.model";
import { Board, Idea } from '.';
import { Thought } from "./thought.interface";

export class Initiative implements Fielded,Thought {
  getFields: Function;
  getClass: Function;
  id: string;
  number:number;
  status:number;
  text: string;
  title: string;
  author: User;
  board:Board;
  static getFields() {
    return ["id", "number", "text", "title", "author","status","board"];
  }
  static getClass(){
    return 'initiative';
  }
}
