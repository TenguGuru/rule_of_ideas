import { Idea } from "./idea.model";
import { Initiative } from "./initiative.model";
import { Thought } from "./thought.interface";
import { User } from "./user.model";

export interface MapListObj{
    title:string;
    number:number;
    rate:number;
    id:string;
    absRate:number;
}
export interface PartyMapInfo{
    title:string;
    color:string;
    membersCount:number;
}
export interface RateInfo{
    rate:number;
    averageNegRate:number;
    averagePlusRate:number;
}
export interface AverageAcceptRate{
    id:string;
    rate:number;
}
export interface AcceptRate{
    plus:number;
    minus:number;
}
export interface InitiativeIdeasInfo{
    thought:Thought;
    averageRate:number;
    relationship:number;
    averagePlusRate:number;
    averageNegRate:number;
}
export class RequestInfoInterfaces{

}

export interface PartyInfo{
    title:string;
    creator:User;
    leader:User;
    membersCount:number;
}