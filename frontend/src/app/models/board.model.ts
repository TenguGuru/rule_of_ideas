import { User } from '.';

export class Board{
   id:string;
   number:number;
   name:string;
   creator:User;
   code:string;
   createDate:Date;
   members:any[];
}
