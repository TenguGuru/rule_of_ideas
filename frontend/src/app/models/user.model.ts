import { Fielded } from "./fielded.interface";
import { Party } from '.';
import { Board } from './board.model';

export enum Role {
  ADMIN="ADMIN",
  USER="USER",
  ORGANIZER="ORGANIZER",
}

export class User implements Fielded {
  getFields: Function;
  getClass: Function;
  id: string;
  email: string;
  userRole: Role;
  name: string;
  party: Party;
  currBoard:Board;
  boardRole:string;
  static getFields() {
    return ["id", "email", "userRole", "name"];
  }
  static getClass(){
    return "User";
  }
}
