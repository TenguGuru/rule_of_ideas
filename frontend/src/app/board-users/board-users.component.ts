import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: 'app-board-users',
  templateUrl: './board-users.component.html',
  styleUrls: ['./board-users.component.css']
})
export class BoardUsersComponent implements OnInit {
  @Input() boardId: string;
  boardMembers: any[];
  displayedColumns: string[] = ['Имя', 'E-mail', 'Роль','Действия'];
  constructor(public boardService: BoardService, public userService: UserService) { }

  ngOnInit(): void {
    this.boardService.getMembers(this.boardId).subscribe((boardMembers) => {
      this.boardMembers = boardMembers;
    })
  }

  changeRole(member,role){
    let changer ={
      memberId:member.id,
      role:role
    }
    this.boardService.changeRole(changer).subscribe((cngMem:any)=>{
     this.boardMembers.find(mem=>mem.id==member.id).role = cngMem.role;
    })
  }

}
