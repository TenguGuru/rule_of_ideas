import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuardService } from "./security/auth-guard.service";

import {
  MainComponent,
  ResetPasswordComponent,
  SignInComponent,
  SignUpComponent,
  AdminComponent,
  SignInBoardComponent,
  OrganaizerComponent,
} from "./components";
import { BoardEditComponent } from "./components/board-edit/board-edit.component";

const routes: Routes = [
  {
    path: "",
    component: MainComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "reset-password",
    component: ResetPasswordComponent,
  },
  {
    path: "sign-in",
    component: SignInComponent,
  },
  {
    path: "sign-up",
    component: SignUpComponent,
  },
  {
    path: "sign-in-board",
    canActivate: [AuthGuardService],
    component: SignInBoardComponent,
  },
  {
    path: "admin",
    canActivate: [AuthGuardService],
    component: AdminComponent,
  },
  {
    path: "organaizer",
    canActivate: [AuthGuardService],
    component: OrganaizerComponent,
  },
  {
    path: "sign-in-board/:orgId/:boardName",
    canActivate: [AuthGuardService],
    component: SignInBoardComponent,
  },
  {
    path: "boardEdit",
    component: BoardEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
