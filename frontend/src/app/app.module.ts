import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppMaterialModule } from "./app-material.module";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PartyComponent } from "./components/main.component/party/party.component/party.component"
import { PartyListComponent } from "./components/main.component/party/party-list.component/party-list.component"
import { PartyEditComponent } from "./components/main.component/party/party-edit.component/party-edit.component"
import {
  HeaderComponent,
  MainComponent,
  ResetPasswordComponent,
  SignInComponent,
  SignUpComponent,
  AdminComponent,
  CrudTableComponent,
  UserEditComponent,
  DeleteDialogComponent,
  PartyInfoDialogComponent,
  JoinedPartyComponent,
  JoinElectComponent,
  CandidateInfoDialog,
  MapComponent,
  WarningMessageDialogComponent,
  SignInBoardComponent,
  CreateBoardComponent,
  OrganaizerComponent,
  PartyBan,
  PartyBlacklistComponent,
  AboutProjComponentComponent,
  IdeaComponent,
  InitiativeComponent
} from "./components";

import { httpInterceptorProviders } from "./security/auth-interceptor";
import { AuthGuardService } from "./security/auth-guard.service";

//import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MAT_COLOR_FORMATS,
  NgxMatColorPickerModule,
  NGX_MAT_COLOR_FORMATS,
} from "@angular-material-components/color-picker";
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { BoardChangerComponent } from './components/board-changer.component/board-changer.component';
import {MatSortModule} from '@angular/material/sort';
import { GestureConfig } from '@angular/material';
import { ThoughtEstimateDialogComponent } from "./components/main.component/thought/thought-estimate-dialog.component/thought-estimate-dialog.component";
import { IdeaEstimateComponent } from "./components/main.component/thought/thought-estimate.component/idea-estimate.component/idea-estimate.component";
import { IdeaEstimateListrowComponent } from "./components/main.component/thought/thought-estimate-listrow.component/idea-estimate-listrow.component";
import { IdeaDetailInfoComponent } from "./components/main.component/thought/thougth-detail-info.component/idea-detail-info.component";
import { IdeaEditComponent } from "./components/main.component/thought/thought-edit.component/idea-edit.component/idea-edit.component";
import { InitiativeEditComponent } from "./components/main.component/thought/thought-edit.component/initiative-edit.component/initiative-edit.component";
import { InitiativeEstimateComponent } from "./components/main.component/thought/thought-estimate.component/initiative-estimate.component/initiative-estimate.component";
import { InitiativeEstimateListrowComponent } from "./components/main.component/thought/thought-estimate-listrow.component/initiative-estimate-listrow.component";
import { InitiativeDetailInfoComponent } from "./components/main.component/thought/thougth-detail-info.component/initiative-detail-info.component";
import { IdeaListComponent } from "./components/main.component/thought/thought-list.component/idea-list.component/idea-list.component";
import { InitiativeListComponent } from "./components/main.component/thought/thought-list.component/initiative-list.component/initiative-list.component";
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { BoardEditComponent } from './components/board-edit/board-edit.component';
import { GeneralBoardSettingsComponent } from './components/general-board-settings/general-board-settings.component';
import { BoardUsersComponent } from './board-users/board-users.component';
import { BoardFunctionalityComponent } from './board-functionality/board-functionality.component';
@NgModule({
  declarations: [
    AppComponent,
    ResetPasswordComponent,
    SignInComponent,
    SignUpComponent,
    MainComponent,
    PartyComponent,
    PartyListComponent,
    PartyEditComponent,
    HeaderComponent,
    AdminComponent,
    CrudTableComponent,
    InitiativeEditComponent,
    UserEditComponent,
    DeleteDialogComponent,
    PartyInfoDialogComponent,
    JoinedPartyComponent,
    JoinElectComponent,
    CandidateInfoDialog,
    MapComponent,
    WarningMessageDialogComponent,
    SignInBoardComponent,
    CreateBoardComponent,
    OrganaizerComponent,
    PartyBan,
    PartyBlacklistComponent,
    BoardChangerComponent,
    AboutProjComponentComponent,
    IdeaComponent,
    InitiativeComponent,
    IdeaEstimateComponent,
    IdeaEstimateListrowComponent,
    IdeaDetailInfoComponent,
    InitiativeEstimateComponent,
    InitiativeEstimateListrowComponent,
    IdeaEditComponent,
    ThoughtEstimateDialogComponent,
    IdeaListComponent,
    InitiativeListComponent,
    InitiativeDetailInfoComponent,
    ConfirmationDialogComponent,
    BoardEditComponent,
    GeneralBoardSettingsComponent,
    BoardUsersComponent,
    BoardFunctionalityComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    DragDropModule,
    HttpClientModule,
    NgxMatColorPickerModule,
    MatSnackBarModule,
    MatSortModule,
    HammerModule,
  ],
  providers: [
    httpInterceptorProviders,
    AuthGuardService,
    { provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS },
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
  ],
  entryComponents: [
    PartyEditComponent,
    InitiativeEditComponent,
    IdeaEditComponent,
    UserEditComponent,
    DeleteDialogComponent,
    PartyInfoDialogComponent,
    JoinElectComponent,
    CandidateInfoDialog,
    WarningMessageDialogComponent,
    CreateBoardComponent,
    PartyBan,
    PartyBlacklistComponent,
    AboutProjComponentComponent,
    IdeaDetailInfoComponent,
    InitiativeDetailInfoComponent,
    ThoughtEstimateDialogComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
