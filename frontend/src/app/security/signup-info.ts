export class SignUpInfo {
  nickName: string;
  email: string;
  password: string;

  constructor(nickname: string, email: string, password: string) {
    this.nickName = nickname;
    this.email = email;
    this.password = password;
  }
}
