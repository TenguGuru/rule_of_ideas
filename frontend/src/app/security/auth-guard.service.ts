import { Injectable } from "@angular/core";
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { Role, User } from "../models";
import { UserService } from "../services";
import { TokenStorageService } from "./token-storage.service";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private token: TokenStorageService, private router: Router, private userService:UserService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.token.logged){
      if((state.url=="/organaizer" && this.userService.currentUser.userRole==Role.ORGANIZER) 
      || (state.url=="/admin" && this.userService.currentUser.userRole==Role.ADMIN)){
        return true;
      }
      if(this.token.hasBoard || state.url=="/sign-in-board" || route.params["boardName"]!=null){
        return true;
      }else{
        this.router.navigate(["/sign-in-board"]);
        return false;
      }
     
    }else{
      this.router.navigate(["/sign-in"], {
        queryParams: {
          return: state.url,
        },
      });
      return false;
    }
  }
}
