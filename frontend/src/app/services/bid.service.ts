import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from '.';

@Injectable({
  providedIn: 'root'
})
export class BidService {

  constructor(private http: HttpClient, private userService:UserService) { }

  updateBidding(boardId){
    return this.http.get(`api/bid/updateBidding/${boardId}`).subscribe();
  }
}
