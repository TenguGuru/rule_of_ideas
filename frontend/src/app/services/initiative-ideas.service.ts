import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InitiativeIdeas } from "../models/initiativeIdeas.model";

@Injectable({
    providedIn: "root",
})
export class InitiativeIdeasService {

    constructor(private http: HttpClient) {}

    getInitiativeIdeasPartyList(initiativeId, partyId):any{
      return this.http.get(`api/initiativeIdeas/getInitiativeIdeasPartyList/${initiativeId}/${partyId}`)
    }

    getIdeaInitiativesPartyList(ideaId, partyId):any{
      return this.http.get(`api/initiativeIdeas/getIdeaInitiativesPartyList/${ideaId}/${partyId}`)
    }

    getIdeaInitiativesList(ideaId):any{
        return this.http.get(`api/initiativeIdeas/getIdeaInitiativesList/${ideaId}`)
      }

    getInitiativeIdeasList(initiativeId):any{
        return this.http.get(`api/initiativeIdeas/getInitiativeIdeasList/${initiativeId}`)
      }

    getInitiativeIdeas(userId,initiativeId): Observable<InitiativeIdeas[]> {
        return this.http.get<InitiativeIdeas[]>(`/api/initiativeIdeas/get/${userId}/${initiativeId}`);
      }

    deleteInitiativeIdea(initiativeIdeaId:string){
        return this.http.delete(`api/initiativeIdeas/delete/${initiativeIdeaId}`);
      }

    saveInitiativeIdeas(initiativeIdeas:InitiativeIdeas) {
        return this.http.post(`api/initiativeIdeas/save`,initiativeIdeas);
      }

}
