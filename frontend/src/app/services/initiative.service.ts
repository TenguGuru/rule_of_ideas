import { Injectable } from "@angular/core";
import { Board, Initiative, User } from "../models";
import { DeleteDialogComponent, InitiativeEditComponent } from "../components";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject, ReplaySubject } from "rxjs";
import { UserService } from './user.service';
import { MapListObj } from "../models/req-info-interfaces";
import { ThoughtEstimateDialogComponent } from "../components/main.component/thought/thought-estimate-dialog.component/thought-estimate-dialog.component";
import { Thought } from "../models/thought.interface";
import { ThoughtService } from "./thought.service";
import { InitiativeDetailInfoComponent } from "../components/main.component/thought/thougth-detail-info.component/initiative-detail-info.component";
import { BoardService } from "./board.service";
import { BoardSettingsService } from "./board.settings.service";

@Injectable({
  providedIn: "root",
})
export class InitiativeService implements ThoughtService {
  thoughts_for_estimation:any[] = [];
  good_thoughts:any[] = [];
  bad_thoughts:any[] = [];
  used_thoughts:any[]=[];
  accept_rate:any[] = [];
  my_thoughts:any[] = [];
  uninteresting_thoughts:any[] = [];
  thoughts_for_judge:any[] = [];
  votedThoughts:any[] = [];
  public userThoughts:Initiative[] = [];
  public thoughtsSub: BehaviorSubject<Initiative[]> = new BehaviorSubject<Initiative[]>([]);
  public estimateSortSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient, public dialog: MatDialog, private userService:UserService,
    private boardService: BoardService, private boardSettingsService: BoardSettingsService) {
      this.getThoughtsSub().subscribe((thoughts:Thought[]) => {
        if (this.userService.currentUser == null || this.userService.currentUser.currBoard == null) return;
        this.thoughts_for_judge= [];
        this.getAvgAcceptRate().subscribe(
          rates=>{
              this.accept_rate = rates;
              thoughts.forEach((thought:Thought)=>{
              if((thought.status == 2)
              && !(this.thoughts_for_estimation.map((old)=>{return old.thought.id;}).includes(thought.id)) 
              && !(this.used_thoughts.includes(thought.id))){
                let element = {id:null,//??? Надо поменять, мб все таки сделать отдельный класс
                  title:thought.title,
                  thought:thought,
                  user:this.userService.currentUser,
                  rate:null,
                  userRate:0}; 
                this.thoughts_for_estimation.push(element);
            }else if((thought.status == 1)
            && !(this.thoughts_for_judge.map((old)=>{return old.thought.id;}).includes(thought.id)) 
            && !(this.used_thoughts.includes(thought.id))){
              let accRate = this.accept_rate.find(value => value.id ==thought.id);
              let element = {
                thought:thought,
                userRate:null,//такое название, чтобы новую сортировку лишний раз не писать
              };
              if(accRate!=null){
                element.userRate = accRate.rate;
              }
              this.thoughts_for_judge.push(element);
            }else if(this.thoughts_for_judge.map((old)=>{return old.thought.id;}).includes(thought.id)){
              let accRate = this.accept_rate.find(value => value.id ==thought.id);
              if(accRate!=null){
                let element = this.thoughts_for_judge.find(value => value.thought.id == thought.id);
                if(element.userRate!=accRate.rate){
                  this.thoughts_for_judge.splice(
                    this.thoughts_for_judge.findIndex(value => value.thought.id ==thought.id),1);
                    element.userRate = accRate.rate;
                  this.thoughts_for_judge.push(element);
                }
              }
            }
          });
          this.sendToSort();
        });
      });
    }
  deleteThoughtDialog(thought: any) {
    let data = thought ? thought.id : {};
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.componentInstance.type = "initiative";
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.updateThougthSubscribe();
      this.getAllUserThoughts();
    });
  }

  getData(){
    this.getRated(this.userService.currentUser.id).subscribe((rated:any[])=>{
      this.sortRated(rated);
      this.used_thoughts = rated.map(item=>{return item.thought.id});
    });
    this.updateThougthSubscribe();
    this.getAcceptingInfoByUser(this.userService.currentUser.id, this.userService.currentUser.currBoard.id).subscribe((data:any[]) => {
      this.votedThoughts = data;
    })
  }

  subForSort(){
    const sortSub = this.getEstimatSortSub().subscribe((srt)=>{
      this.sortByUserRate(this.bad_thoughts);
      this.sortByUserRate(this.good_thoughts);
      this.sortByUserRate(this.thoughts_for_judge);
      this.sortByNumber(this.thoughts_for_estimation);
    })
  }

  sortByNumber(array) {
    array.sort((n1,n2) => {
      if (n1.thought.number > n2.thought.number) {
        return 1;
      }
      if (n1.thought.number < n2.thought.number) {
        return -1;
      }
      return 0;
    });
  }

  sortByUserRate(array){
    array.sort((n1,n2) => {
      if(n1.userRate<0 && n2.userRate<0){
      if (n1.userRate > n2.userRate) {
        return 1;
    } 
    if (n1.userRate < n2.userRate) {
        return -1;
    }
    return 0;
    }else{
      if (n1.userRate < n2.userRate) {
        return 1;
    } 
    if (n1.userRate > n2.userRate) {
        return -1;
    }
    return 0;
    }
  });
  }
  sortRated(rated:any[]){
      rated.forEach(element=>{
        switch(element.rate){
          case 0:{
            if(!this.bad_thoughts.map((item:any)=>{return item.id}).includes(element.id)){
            this.bad_thoughts.push(element);
            }
            break;
          }
          case 1:{
            if(!this.uninteresting_thoughts.map((item:any)=>{return item.id}).includes(element.id)){
            this.uninteresting_thoughts.push(element);
            }
            break;
          }
          case 2:{
            if(!this.good_thoughts.map((item:any)=>{return item.id}).includes(element.id)){
            this.good_thoughts.push(element);
            }
            break;
          }
        }
      })
      this.sortByUserRate(this.bad_thoughts);
      this.sortByUserRate(this.good_thoughts);
      this.sortByUserRate(this.thoughts_for_judge);
      this.sortByNumber(this.thoughts_for_estimation);
  }

  getEstimatSortSub(): BehaviorSubject<boolean> {
    return this.estimateSortSub;
  }

  getThoughtsSub():BehaviorSubject<Initiative[]> {
    return this.thoughtsSub;
  }

  updateThougthSubscribe() {
    this.http.get<any[]>(`/api/thoughts/initiatives/board/${this.userService.currentUser.currBoard.id}`).subscribe((value: any[]) => {
        this.setThoughts(value);
    });
  }
  getAllByBoard(boardId){
    return this.http.get<Initiative[]>(`/api/thoughts/initiatives/board/${boardId}`);
  }
  loadThoughts(){
    return this.http.get<Initiative[]>(`/api/thoughts/initiatives/`);
  }
  getAllValidatedForMap(){
    return this.http.get<MapListObj[]>(`/api/thoughts/initiatives/forValidatedMap/${this.userService.currentUser.currBoard.id}`)
  }
  getThoughtsPartyRateInfo(thougtId, partyId):any{
    return this.http.get(`api/thoughts/initiatives/getThoughtPartyRateInfo/${thougtId}/${partyId}`)
  }
  getPartyMaxBidInfo(boardId, initiativeId, partyId){
    return this.http.get(`api/thoughts/initiatives/getPartyMaxBidInfo/${boardId}/${initiativeId}/${partyId}`);
  }
  searchForContaining(value){
    return this.http.get<MapListObj[]>(`/api/thoughts/initiatives/search/${this.userService.currentUser.currBoard.id}/${value}`)
  }
  setThoughts(initiatives:Initiative[]){
    this.thoughtsSub.next(initiatives);
  }
  getAllUserThoughts(){
   this.http.get<any[]>(`/api/thoughts/initiatives/getByUser/${this.userService.currentUser.currBoard.id}/${this.userService.currentUser.id}`).subscribe(value=>{
      this.userThoughts = value;
   });
  }
  getByStatus(status:number){
    return this.http.get<Initiative[]>(`/api/thoughts/initiatives/getByStatus/${this.userService.currentUser.currBoard.id}/${status}`);
  }
  get(id): Observable<Initiative> {
    return this.http.get<Initiative>(`/api/thoughts/initiatives/${id}`);
  }
  save(thought:Thought) {
    if(thought.id==null){
      return this.http.post(`/api/thoughts/initiatives/save`, thought);
    }else{
      return this.http.post(`/api/thoughts/initiatives/update`, thought);
    }
  }
  getThoughtRateInfo(thoughtId):any{
    return this.http.get(`api/thoughts/initiatives/getThoughtRateInfo/${thoughtId}`)
  }
  rank(rank) {//Cнизу небольшой булщит надо поменять
      rank = {id:rank.id,thought:rank.thought,user:rank.user,rate:rank.rate,userRate:rank.userRate};
    if(rank.id==null){
      return this.http.post(`/api/thoughts/initiatives/save/rate`,rank);
    }else{
      return this.http.post(`/api/thoughts/initiatives/update/rate`,rank);
    }
  }
  getRated(userId:string){
    return this.http.get(`api/thoughts/initiatives/get/rate/${this.userService.currentUser.currBoard.id}/${userId}`);
  }
  getAcceptingInfo(thoughtId){
    return this.http.get(`api/thoughts/initiatives/get/acceptingRate/${thoughtId}`)
  }
  getAcceptingInfoByUser(userId, boardId){
    return this.http.get(`api/thoughts/initiatives/get/acceptingRateByUser/${userId}/${boardId}`);
  }
  getAcceptingPartyInfo(thougtId, partyId){
    return this.http.get(`api/thoughts/initiatives/get/acceptingPartyRate/${thougtId}/${partyId}`)
  }
  saveAcceptRate(rate){
    return this.http.post(`api/thoughts/initiatives/save/acceptingRate`,rate);
  }
  accept(thoughtId){
    return this.http.get(`api/thoughts/initiatives/acceptThought/${thoughtId}`)
  }
  getAvgAcceptRate(){
    return this.http.get<any[]>(`api/thoughts/initiatives/get/AvgAcceptRate/${this.userService.currentUser.currBoard.id}`);
  }

  updateLoyalty(partyId, thoughtId, loyalty){
    return this.http.get(`api/thoughts/initiativeBids/update/loyalty/{thoughtId}/{partyId}`, loyalty);
  }

  concludeVoting(thoughtId: any) {
    return this.http.get(`api/thoughtVoting/initiativeVoting/get/concludeVoting/${thoughtId}`);
  }

  createEditDialog(row?:Thought): void {
    let data = row ? row : null;
    const dialogRef = this.dialog.open(InitiativeEditComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
      this.updateThougthSubscribe();
      this.getAllUserThoughts();
    });
  }
  createEstimateDialog(row:any, rate?:number): void {
    let data ={thoughtRate: row,addRate: rate ? rate:null};
    let dialogRef = this.dialog.open(ThoughtEstimateDialogComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.rank(row).subscribe(res=>{
        this.sendToSort();
      });
    });
  }
  createInfoDialog(row:Thought): void {
    let data = row;
    let dialogRef = this.dialog.open(InitiativeDetailInfoComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  sendToSort(){//передает данные и триггерит сортировку в estimate
    this.estimateSortSub.next(!this.estimateSortSub.value);
  }
  closeDialog(){
    this.dialog.closeAll();
  }

  clearData() {
    this.used_thoughts = [];
    this.thoughts_for_estimation = [];
    this.bad_thoughts = [];
    this.good_thoughts = [];
    this.uninteresting_thoughts = [];
    this.thoughts_for_judge = [];
  }

}
