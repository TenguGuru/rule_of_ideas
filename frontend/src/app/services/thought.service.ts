import { ReplaySubject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { Thought } from '../models/thought.interface';

export interface ThoughtService {
  updateThougthSubscribe();
  getThoughtsSub();//Нужены для передачи подписок в компонент оценки
  getEstimatSortSub():BehaviorSubject<boolean>;
  getAllByBoard(boardId);
  loadThoughts();
  searchForContaining(value);
  setThoughts(ideas:Thought[]);
  getAllUserThoughts();
  getByStatus( status:number);
  get(id):Observable<any>;
  save(thought:Thought);
  getThoughtRateInfo( thoughtId):any;
  getPartyMaxBidInfo(boardId, thoughtId, partyId);
  rank(rank);
  getRated(userId:string)
  getAcceptingInfo(thoughtId);
  getAcceptingInfoByUser(userId, boardId)
  saveAcceptRate(rate)
  accept(thoughtId)
  getAvgAcceptRate();
  createEditDialog(row?:Thought):void;
  createEstimateDialog(row:any, rate?:number):void;
  createInfoDialog(row:Thought): void;
  sendToSort();
  closeDialog();
  getThoughtsPartyRateInfo(thoughtId, partyId);
  getAcceptingPartyInfo(thoughtId, partyId);
  concludeVoting(thoughtId);
  deleteThoughtDialog(thought);
}
