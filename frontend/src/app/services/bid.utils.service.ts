import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class BidUtilsService{

  getPercentageDifference(loyaltyPercent: number, lastLoyaltyPercent: number){
    if (lastLoyaltyPercent){
      return loyaltyPercent - lastLoyaltyPercent;
    }
    return loyaltyPercent;
  }     
}