import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { Board } from '../models/board.model';
import { MatDialog } from '@angular/material';
import { CreateBoardComponent } from '../components';
import { UserService } from './user.service';
import { User } from '../models';

export enum MemberRole {
  OBSERVER = 1, MEMBER, ORGANIZER, CREATOR
}

@Injectable({
  providedIn: "root",
})
export class BoardService {
  public orgSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public boardChangeSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public boardList:Board[] = [];

    constructor(private http: HttpClient, public dialog: MatDialog,private userService:UserService) {}

    save(board:Board){
      return this.http.post("/api/boards/save",board);
    }
    findById(id){
      return this.http.get<Board>(`/api/boards/${id}`)
    }
    findByName(name){
      return this.http.get<Board>(`/api/boards/findByName/${name}`)
    }
    findByCreator(){
       return this.http.get<Board[]>(`/api/boards/findByCreator/${this.userService.currentUser.id}`)
    }
    getMembers(boardId){
      return this.http.get<any[]>(`/api/boards/getMembers/${boardId}`)
    }
    getBoards(userId){
      return this.http.get<Board[]>(`/api/boards/getBoards/${userId}`).subscribe((boards: Board[]) => {
        boards.forEach((brd:any)=>{
          if(brd.id == (this.userService.currentUser.currBoard? this.userService.currentUser.currBoard.id:null)){
            boards.splice(boards.findIndex(val=>val.id==brd.id),1);
          }
         });
         this.boardList = boards;
      })
    }
    findMember(userId,boardId){
      return this.http.get<any>(`/api/boards/findMember/${userId}/${boardId}`)
    }
    addBoard(addBoard){
      return this.http.post("/api/boards/addBoard",addBoard);
    }
    getBoardQuota(boardId){
      return this.http.get<any>(`/api/boards/getQuota/${boardId}`);
    }
    getCreator(boardId){
      return this.http.get<User>(`/api/boards/getCreator/${boardId}`);
    }
    changeRole(changer){
      return this.http.post('/api/boards/changeRole/',changer);
    }
    changeBoard(isChange:boolean){
      this.boardChangeSub.next(isChange);
    }
    createBoardDialog(){
      let dialogRef = this.dialog.open(CreateBoardComponent, {
        width: "100%",
        maxWidth: "none",
      });
      dialogRef.afterClosed().subscribe((result) => {
        this.orgSub.next(true);
        this.getBoards(this.userService.currentUser.id);
      });
    }
    closeDialog(){
      this.dialog.closeAll();
    }
    getCurrentUserRole() {
      this.findMember(this.userService.currentUser.id, this.userService.currentUser.currBoard.id).subscribe(( member => {
        this.userService.currentUser.boardRole = member.role;
      } ));
    }

    hasUserWriteGrants(): boolean {
      return (this.userService.currentUser.boardRole == "MEMBER" 
        || this.userService.currentUser.boardRole == "ORGANIZER"
        || this.userService.currentUser.boardRole == "CREATOR")
    }
}
