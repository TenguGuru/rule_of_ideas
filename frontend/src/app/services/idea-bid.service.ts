import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ThoughtBidService } from './thought-bid.service';
import { IdeaBid } from "../models/idea-bid.model";

@Injectable({
  providedIn: "root",
})
export class IdeaBidService implements ThoughtBidService {

  constructor(private http: HttpClient){
  }

  getLoyaltyPercent(partyId, thoughtId){
    return this.http.get<number>(`/api/thoughtBids/ideaBids/get/loyaltyPercent/${partyId}/${thoughtId}`);
  }

  getAttempts(partyId, thoughtId){
    return this.http.get<number>(`/api/thoughtBids/ideaBids/get/attempts/${partyId}/${thoughtId}`)
  }

  save(thoughtBid){
    return this.http.post<IdeaBid>(`/api/thoughtBids/ideaBids/save`, thoughtBid);
  }

  getByPartyAndThought(partyId, thoughtId){
    return this.http.get<IdeaBid>(`/api/thoughtBids/ideaBids/get/thoughtBid/byPartyAndThought/${partyId}/${thoughtId}`)
  }

  getAllThoughtBidsForMap(boardId){
    return this.http.get(`/api/thoughtBids/ideaBids/get/thoughtBid/allThoughtBidsForMap/${boardId}`);
  }
}
