import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public dataChanged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  

  constructor(private http: HttpClient) { }

  public getDataChangedAsObservable() {
    return this.dataChanged.asObservable();
  }

  deleteIssue(data : any, type : any){
    return this.http.post(`/api/data/delete`, {
      id: data,
      type: type
    }).subscribe(() => this.dataChanged.next(true));
  }
}
