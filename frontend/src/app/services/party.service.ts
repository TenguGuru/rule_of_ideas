import { Injectable } from "@angular/core";
import { Party, User } from "../models";
import { CandidateInfoDialog, JoinElectComponent, PartyBan, PartyBlacklistComponent, PartyEditComponent, PartyInfoDialogComponent, WarningMessageDialogComponent } from "../components";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { UserService } from './user.service';
import { UniqueSelectionDispatcher } from "@angular/cdk/collections";
import { PartyInfo } from "../models/req-info-interfaces";

@Injectable({
  providedIn: "root",
})
export class PartyService {
  public parties: Party[] = [];
  public partyInfoMap = new Map<number, PartyInfo>();
  public joinedPartySub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient, public dialog: MatDialog,private userService:UserService) {}

  getAll() {
    this.http.get<Party[]>(`/api/parties/board/${this.userService.currentUser.currBoard.id}`).subscribe((value: Party[]) => {
      this.parties = value;
    });
  }

  loadParties(){
    return this.http.get(`/api/parties/`);
  }

  getInfo(partyId){// TODO: refactor
    return this.http.get(`/api/parties/getInfo/${partyId}`);
  }
  getPartyInfo(partyId){ 
    this.http.get(`/api/parties/getInfo/${partyId}`).subscribe((data:PartyInfo)=>{
      this.partyInfoMap.set(partyId, data)
    });
  }
  getAllPartiesValidatedForMap() {
    return this.http.get(`/api/parties/getAllPartiesValidatedForMap/${this.userService.currentUser.currBoard.id}`);
  }

  getPartyByIdea(ideaId){
    return this.http.get(`/api/parties/getPartyByIdea/${ideaId}`);
  }

  getPartyByInitiative(initiativeId){
    return this.http.get(`/api/parties/getPartyByInitiative/${initiativeId}`);
  }

  getRatesMatching(userId,partyId){
    return this.http.get(`/api/parties/getRatesMatching/${userId}/${partyId}`);
  }
  getPartyIdeas(partyId){
    return this.http.get(`api/parties/getPartyIdeas/${partyId}`);
  }

  getPartyInitiatives(partyId){
    return this.http.get(`api/parties/getPartyInitiatives/${partyId}`);
  }

  savePartyIdea(partyIdea){
    return this.http.post('api/parties/savePartyIdea/', partyIdea);
  }

//   savePartyInitiative(partyInitiative){
//     return this.http.post('api/parties/savePartyInitiative/', partyInitiative);
//   }

  getIdeaLoyaltyPercent(partyId){
    return this.http.get<number>(`/api/parties/getIdeaLoyaltyPercent/${partyId}`);
  }

  getInitiativeLoyaltyPercent(partyId){
    return this.http.get<number>(`/api/parties/getInitiativeLoyaltyPercent/${partyId}`);
  }

  spendIdeaLoyaltyPercent(partyId, loyaltyPercent){
    return this.http.post(`api/parties/spendIdeaLoyaltyPercent/${partyId}`, loyaltyPercent);
  }

  spendInitiativeLoyaltyPercent(partyId, loyaltyPercent){
    return this.http.post(`api/parties/spendInitiativeLoyaltyPercent/${partyId}`, loyaltyPercent);
  }

  getPartyMembersCount(partyId){
    return this.http.get<number>(`/api/parties/getPartyMembersCount/${partyId}`);
  }

  refreshSub(){
    this.joinedPartySub.next(!this.joinedPartySub.value);
  }

  deleteElect(voterId){
    this.http.delete(`/api/parties/deletePartyElect/${voterId}`).subscribe();
  }
  getPartyMember(userId,boardId){
    return this.http.get(`/api/parties/getPM/${userId}/${boardId}`);
  }

  isPartyLeader(userId, boardId){
    return this.http.get(`/api/parties/isPartyLeader/${userId}/${boardId}`);
  }
  isPartyCreator(userId, boardId){
    return this.http.get(`/api/parties/isPartyCreator/${userId}/${boardId}`);
  }
  getPartyElect(voterId){
    return this.http.get(`/api/parties/getPartyElect/${voterId}`);
  }

  unban(userId,partyId){
    return this.http.get(`/api/parties/unban/${userId}/${partyId}`);
  }

  getBanned(partyId){
    return this.http.get(`/api/parties/banned/${partyId}`);
  }

  getPartyMembers(partyId){
    return this.http.get(`/api/parties/getMembers/${partyId}`);
  }

  savePartyMember(partyMember){
    return this.http.post('api/parties/savePartyMember/', partyMember);
  }

  get(id): Observable<Party> {
    return this.http.get<Party>(`/api/parties/${id}`);
  }

  save(party) {
    return this.http.post(`/api/parties/save`, party);
  }

  join(party: Party, user: User) {
    return this.http.get(`/api/parties/join/${party.id}/${user.id}`);
  }

  likeCandidate(voterId, candidateId){
    return this.http.get(`/api/parties/savePartyElect/${voterId}/${candidateId}`);
  }
  ban(ban){
    return this.http.post('/api/parties/ban/',ban);
  }

  exitParty(userId){
    this.http.get(`/api/parties/exitFromParty/${this.userService.currentUser.currBoard.id}/${userId}`).subscribe(
      res=>{
        console.log(res);
      }
    )
  }

  createEditDialog(row?): void {
    let data = row ? { id: row.id } : {};
    const dialogRef = this.dialog.open(PartyEditComponent, {
      data,
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAll();
      console.log("The dialog was closed");
    });
  }
  createBlackListDialog(row): void {
    let data = row;
    const dialogRef = this.dialog.open(PartyBlacklistComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
    });
  }
  createBanDialog(row?): void {
    let data = row ? row : null;
    const dialogRef = this.dialog.open(PartyBan, {
      data,
    });
    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  createInfoDialog(row:Party): void {
    let data =row;
    let dialogRef = this.dialog.open(PartyInfoDialogComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }

  createCandidateInfoDialog(row): void {
    let data =row ? row:null;
    let dialogRef = this.dialog.open(CandidateInfoDialog, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }

  createJoinElectDialog(row):MatDialogRef<JoinElectComponent, any>{
    let data =row ? row:null;
    let dialogRef = this.dialog.open(JoinElectComponent, {
      data,
      width: "15%",
      maxWidth: "30%",
      height: "60%",
      maxHeight: "100%",
    });
    return dialogRef;
  }
  createWarnDialog(user){
    let data = user;
    let dialogRef = this.dialog.open(WarningMessageDialogComponent, {
        data,
        width: "100%",
        maxWidth: "none",
      });
      dialogRef.afterClosed().subscribe((result) => {
        console.log("The dialog was closed");
      });
  }

  closeDialog(){
    this.dialog.closeAll();
  }
}
