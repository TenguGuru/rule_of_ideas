import { Observable } from 'rxjs';
import { ThoughtBid } from '../models/thought-bid.interface';

export interface ThoughtBidService {
  getAttempts(partyId: string, thoughtId: string);
  save(thoughtBid: ThoughtBid);
  getByPartyAndThought(partyId: string, thoughtId: string);
  getAllThoughtBidsForMap(boardId: string);
}
