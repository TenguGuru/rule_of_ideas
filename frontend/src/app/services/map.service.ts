import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { UserService } from './user.service';


@Injectable({
  providedIn: "root",
})
export class MapService {

    public mapSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient,private userService:UserService) {}

    getPartyMatrixColorData(){
        return this.http.get<any[]>(`api/parties/getMatrixMap/${this.userService.currentUser.currBoard.id}`)
    }
    getIdeaMatrixColorData(id){
        return this.http.get<any[]>(`api/thoughts/ideas/getMatrixMap/${id}`)
    }
    getInitiativeMatrixColorData(id){
        return this.http.get<any[]>(`api/thoughts/initiatives/getMatrixMap/${id}`)
    }

    setVotingIdeaResult(thoughtId){
        return this.http.post<any[]>("api/thoughts/ideas/setVotingResultIdea",thoughtId)
    }

    refreshSub(){
        this.mapSub.next(!this.mapSub.value);
    }
}
