import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ThoughtBidService } from './thought-bid.service';
import { InitiativeBid } from "../models/initiative-bid.model";

@Injectable({
  providedIn: "root",
})
export class InitiativeBidService implements ThoughtBidService {

  constructor(private http: HttpClient){
  }

  getLoyaltyPercent(partyId, thoughtId){
    return this.http.get<number>(`/api/thoughtBids/initiativeBids/get/loyaltyPercent/${partyId}/${thoughtId}`);
  }

  getAttempts(partyId, thoughtId){
    return this.http.get<number>(`/api/thoughtBids/initiativeBids/get/attempts/${partyId}/${thoughtId}`)
  }

  save(thoughtBid){
    return this.http.post<InitiativeBid>(`/api/thoughtBids/initiativeBids/save`, thoughtBid);
  }

  getByPartyAndThought(partyId, thoughtId){
    return this.http.get<InitiativeBid>(`/api/thoughtBids/initiativeBids/get/thoughtBid/byPartyAndThought/${partyId}/${thoughtId}`)
  }

  getAllThoughtBidsForMap(boardId){
    return this.http.get(`/api/thoughtBids/initiativeBids/get/thoughtBid/allThoughtBidsForMap/${boardId}`);
  }
}
