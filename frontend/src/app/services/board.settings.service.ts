import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import { Observable, BehaviorSubject, ReplaySubject } from "rxjs";
import {BoardSettings} from "../models/board-settings.model";

@Injectable({
  providedIn: "root",
})
export class BoardSettingsService{
  public bidding: boolean;

  constructor(private http: HttpClient) {
  }

  updateBiddingStatus(boardSettings: BoardSettings){
    return this.http.post(`/api/boards/bidding/update/`, boardSettings).subscribe((value: BoardSettings) => this.bidding = boardSettings.bidding)
  }

  getBiddingStatus(boardId: string){
    this.http.get(`api/boards/bidding/getStatus/${boardId}/`).subscribe((value: boolean) => {
      this.bidding = value
    });
  }
}
