import { Injectable } from "@angular/core";
import { Board, Idea, User } from "../models";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject, ReplaySubject } from "rxjs";
import { ThoughtService, UserService } from '.';
import { MapListObj } from "../models/req-info-interfaces";
import { Thought } from "../models/thought.interface";
import { ThoughtEstimateDialogComponent } from "../components/main.component/thought/thought-estimate-dialog.component/thought-estimate-dialog.component";
import { DeleteDialogComponent, IdeaEditComponent } from "../components";
import { IdeaDetailInfoComponent } from "../components/main.component/thought/thougth-detail-info.component/idea-detail-info.component";
import { BoardService } from "./board.service";
import { BoardSettingsService } from "./board.settings.service";

@Injectable({
  providedIn: "root",
})
export class IdeaService implements ThoughtService {
  thoughts_for_estimation:any[] = [];
  good_thoughts:any[] = [];
  bad_thoughts:any[] = [];
  used_thoughts:any[]=[];
  accept_rate:any[] = [];
  my_thoughts:any[] = [];
  uninteresting_thoughts:any[] = [];
  thoughts_for_judge:any[] = [];
  votedThoughts:any[] = [];
  public userThoughts:Idea[] = [];
  public estimateSortSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public thoughtsSub: BehaviorSubject<Idea[]> = new BehaviorSubject<Idea[]>([]);;
  avgAcceptRateSub: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);;

  constructor(private http: HttpClient, public dialog: MatDialog, public userService:UserService,
    public boardService: BoardService, public boardSettingsService: BoardSettingsService) {
      this.getThoughtsSub().subscribe((thoughts:Thought[]) => {
          if (this.userService.currentUser == null || this.userService.currentUser.currBoard == null) return;
          this.thoughts_for_judge= [];
          this.getAvgAcceptRate().subscribe(
            rates=>{
              this.accept_rate = rates;
              thoughts.forEach((thought:Thought)=>{
              if((thought.status == 2)
              && !(this.thoughts_for_estimation.map((old)=>{return old.thought.id;}).includes(thought.id)) 
              && !(this.used_thoughts.includes(thought.id))){
                let element = {id:null,//??? Надо поменять, мб все таки сделать отдельный класс
                  title:thought.title,
                  thought:thought,
                  user:this.userService.currentUser,
                  rate:null,
                  userRate:0};
                this.thoughts_for_estimation.push(element);
            }else if((thought.status == 1)
            && !(this.thoughts_for_judge.map((old)=>{return old.thought.id;}).includes(thought.id)) 
            && !(this.used_thoughts.includes(thought.id))){
              let accRate = this.accept_rate.find(value => value.id ==thought.id);
              let element = {
                thought:thought,
                userRate:null,//такое название, чтобы новую сортировку лишний раз не писать
              };
              if(accRate!=null){
                element.userRate = accRate.rate;
              }
              this.thoughts_for_judge.push(element);
            }else if(this.thoughts_for_judge.map((old)=>{return old.thought.id;}).includes(thought.id)){
              let accRate = this.accept_rate.find(value => value.id ==thought.id);
              if(accRate!=null){
                let element = this.thoughts_for_judge.find(value => value.thought.id == thought.id);
                if(element.userRate!=accRate.rate){
                  this.thoughts_for_judge.splice(
                    this.thoughts_for_judge.findIndex(value => value.thought.id ==thought.id),1);
                    element.userRate = accRate.rate;
                  this.thoughts_for_judge.push(element);
                }
              }
            }
          });
          this.sendToSort();
        });
      });

        
    }

  deleteThoughtDialog(thought: any) {
    let data = thought ? thought.id : {};
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.componentInstance.type = "idea";
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.updateThougthSubscribe();
      this.getAllUserThoughts();
    });
  }
  
  getEstimatSortSub(): BehaviorSubject<boolean> {
    return this.estimateSortSub;
  }

  getThoughtsSub() {
    return this.thoughtsSub;
  }

  getAvgAcceptRateSub() {
    return this.avgAcceptRateSub;
  }

  updateThougthSubscribe() {
    this.getAllByBoard(this.userService.currentUser.currBoard.id).subscribe((value: Idea[]) => {
      this.setThoughts(value);
    })
  }
  getAllByBoard(boardId){
    return this.http.get<Idea[]>(`/api/thoughts/ideas/board/${boardId}`);
  }
  loadThoughts(){
    return this.http.get<Idea[]>(`/api/thoughts/ideas/`);
  }
  getAllValidatedForMap(){
    return this.http.get<MapListObj[]>(`/api/thoughts/ideas/forValidatedMap/${this.userService.currentUser.currBoard.id}`)
  }
  searchForContaining(value){
    return this.http.get<MapListObj[]>(`/api/thoughts/ideas/search/${this.userService.currentUser.currBoard.id}/${value}`)
  }
  setThoughts(ideas:Idea[]){
    this.thoughtsSub.next(ideas);
  }

  setAvgAcceptRates(rates: any[]){
    this.avgAcceptRateSub.next(rates);
  }

  getAllUserThoughts(){
   this.http.get<any[]>(`/api/thoughts/ideas/getByUser/${this.userService.currentUser.currBoard.id}/${this.userService.currentUser.id}`).subscribe(value=>{
      this.userThoughts = value;
   });
  }
  getByStatus(status:number){
    return this.http.get<Idea[]>(`/api/thoughts/ideas/getByStatus/${this.userService.currentUser.currBoard.id}/${status}`);
  }
  get(id): Observable<Idea> {
    return this.http.get<Idea>(`/api/thoughts/ideas/${id}`);
  }
  getThoughtsPartyRateInfo(ideaId, partyId):any{
    return this.http.get(`api/thoughts/ideas/getThoughtPartyRateInfo/${ideaId}/${partyId}`)
  }
  save(thought:Thought) {
    if(thought.id==null){
      return this.http.post(`/api/thoughts/ideas/save`, thought);
    }else{
      return this.http.post(`/api/thoughts/ideas/update`, thought);
    }
  }
  getThoughtRateInfo(thoughtId):any{
    return this.http.get(`api/thoughts/ideas/getThoughtRateInfo/${thoughtId}`)
  }
  rank(rank) {//Cнизу небольшой булщит надо поменять
      rank = {id:rank.id,thought:rank.thought,user:rank.user,rate:rank.rate,userRate:rank.userRate};
    if(rank.id==null){
      return this.http.post(`/api/thoughts/ideas/save/rate`,rank);
    }else{
      return this.http.post(`/api/thoughts/ideas/update/rate`,rank);
    }
  }
  getRated(userId:string){
    return this.http.get(`api/thoughts/ideas/get/rate/${this.userService.currentUser.currBoard.id}/${userId}`);
  }
  getAcceptingInfo(thoughtId){
    return this.http.get(`api/thoughts/ideas/get/acceptingRate/${thoughtId}`)
  }
  getAcceptingInfoByUser(userId, boardId){
    return this.http.get(`api/thoughts/ideas/get/acceptingRateByUser/${userId}/${boardId}`);
  }
  getAcceptingPartyInfo(ideaId, partyId):any{
    return this.http.get<any[]>(`api/thoughts/ideas/get/acceptingPartyRateInfo/${ideaId}/${partyId}`)
  }
  getPartyMaxBidInfo(boardId, ideaId, partyId){
    return this.http.get(`api/thoughts/ideas/getPartyMaxBidInfo/${boardId}/${ideaId}/${partyId}`);
  }
  saveAcceptRate(rate){
    return this.http.post(`api/thoughts/ideas/save/acceptingRate`,rate);
  }
  accept(thoughtId){
    return this.http.get(`api/thoughts/ideas/acceptThought/${thoughtId}`)
  }
  getAvgAcceptRate(){
    return this.http.get<any[]>(`api/thoughts/ideas/get/AvgAcceptRate/${this.userService.currentUser.currBoard.id}`);
  }

  concludeVoting(thoughtId: any) {
    return this.http.get(`api/thoughtVoting/ideaVoting/get/concludeVoting/${thoughtId}`);
  }

  getData(){
    this.getRated(this.userService.currentUser.id).subscribe((rated:any[])=>{
      this.sortRated(rated);
      this.used_thoughts = rated.map(item=>{return item.thought.id});
      
    });
    this.updateThougthSubscribe();
    this.getAcceptingInfoByUser(this.userService.currentUser.id, this.userService.currentUser.currBoard.id).subscribe((data : any[]) => {
      this.votedThoughts = data;
    });
  }


  createEditDialog(row?:Thought): void {
    let data = row ? row : null;
    const dialogRef = this.dialog.open(IdeaEditComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
      this.updateThougthSubscribe();
      this.getAllUserThoughts();
    });
  }
  createEstimateDialog(row:any, rate?:number): void {
    let data ={thoughtRate: row,addRate: rate ? rate:null};
    let dialogRef = this.dialog.open(ThoughtEstimateDialogComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((isSave) => {
      if(isSave){
        this.rank(row).subscribe(res=>{
          this.sendToSort();
        });
      }
    });
  }
  createInfoDialog(row:Thought): void {
    let data = row;
    let dialogRef = this.dialog.open(IdeaDetailInfoComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
  sendToSort(){//передает данные и триггерит сортировку в estimate
    this.estimateSortSub.next(!this.estimateSortSub.value);
  }
  closeDialog(){
    this.dialog.closeAll();
  }

  subForSort(){
    const sortSub = this.getEstimatSortSub().subscribe((srt)=>{
      this.sortByUserRate(this.bad_thoughts);
      this.sortByUserRate(this.good_thoughts);
      this.sortByUserRate(this.thoughts_for_judge);
      this.sortByNumber(this.thoughts_for_estimation);
    })
  }

  sortRated(rated:any[]){
    rated.forEach(element=>{
      switch(element.rate){
        case 0:{
          if(!this.bad_thoughts.map((item:any)=>{return item.id}).includes(element.id)){
          this.bad_thoughts.push(element);
          }
          break;
        }
        case 1:{
          if(!this.uninteresting_thoughts.map((item:any)=>{return item.id}).includes(element.id)){
          this.uninteresting_thoughts.push(element);
          }
          break;
        }
        case 2:{
          if(!this.good_thoughts.map((item:any)=>{return item.id}).includes(element.id)){
          this.good_thoughts.push(element);
          }
          break;
        }
      }
    })
    this.sortByUserRate(this.bad_thoughts);
    this.sortByUserRate(this.good_thoughts);
    this.sortByUserRate(this.thoughts_for_judge);
    this.sortByNumber(this.thoughts_for_estimation);
}

sortByUserRate(array){
  array.sort((n1,n2) => {
    if(n1.userRate<0 && n2.userRate<0){
      return n1.userRate - n2.userRate;
    }
    else{
      return n2.userRate - n1.userRate;
    }
});
}

sortByNumber(array) {
  array.sort((n1,n2) => {
    return n1.thought.number - n2.thought.number;
  });
}

clearData() {
  this.accept_rate = [];
  this.my_thoughts = [];
  this.used_thoughts = [];
  this.thoughts_for_estimation = [];
  this.bad_thoughts = [];
  this.good_thoughts = [];
  this.uninteresting_thoughts = [];
  this.thoughts_for_judge = [];
  this.votedThoughts = [];
}

}
