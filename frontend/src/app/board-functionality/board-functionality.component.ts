import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-board-functionality',
  templateUrl: './board-functionality.component.html',
  styleUrls: ['./board-functionality.component.css']
})
export class BoardFunctionalityComponent implements OnInit {
  @Input() boardId: string;
  constructor() { }

  ngOnInit(): void {
  }

}
