import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { Role } from "../../models";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { SignUpInfo } from "../../security/signup-info";
import { Observable } from "rxjs";

@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.css"],
})
export class SignUpComponent {
  //userRoles = Object.keys(Role).filter((obj) => isNaN(Number(obj)));
  hide: boolean = true;
  signUpForm: FormGroup;
  usernameValidation:FormControl;
  signupInfo: SignUpInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  userAlreadyExists = false;
  errorMessage = "";

  constructor(
    public fb: FormBuilder,
    public userService: UserService,
    private router: Router
  ) {
    this.usernameValidation = new FormControl("",Validators.email);
    this.signUpForm = this.fb.group({
      nickName: ["", []],
      email: ["", [Validators.email, Validators.required]],
      password: [
        "",
        [
          /*Validators.pattern('regex'),
           Validators.maxLength(25),
           Validators.minLength(6),*/
        ],
      ],
      userRole: ["", [Validators.required]],
    });
  }

  get nickName() {
    return this.signUpForm.get("nickName");
  }

  get email() {
    return this.signUpForm.get("email");
  }

  get password() {
    return this.signUpForm.get("password");
  }

  /*get userRole() {
    return this.signUpForm.get("userRole");
  }*/

  onSubmit() {
    this.usernameValidation.setValue(this.nickName.value);
    if(!this.usernameValidation.valid){
      this.signupInfo = new SignUpInfo(
        this.nickName.value,
        this.email.value,
        this.password.value
      );
      this.userService.signUp(this.signupInfo).subscribe(
        (data) => {
          this.isSignedUp = true;
          this.isSignUpFailed = false;
          this.router.navigateByUrl("/");
        },
        (error) => {
          console.log(error);
          this.errorMessage = error.error.message;
          this.isSignUpFailed = true;
          if(error.status == 400)
            this.userAlreadyExists = true;
        }
      );
    }
  }
}
