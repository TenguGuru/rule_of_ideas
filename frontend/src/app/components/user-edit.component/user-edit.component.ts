import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { User, Role } from "../../models";
import { UserService } from "src/app/services/user.service";
import { Router, ActivatedRoute } from "@angular/router";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: "app-user-edit",
  templateUrl: "./user-edit.component.html",
  styleUrls: ["./user-edit.component.css"],
})
export class UserEditComponent implements OnInit {
  newUserForm: FormGroup;
  user: User;
  userRoles = Object.keys(Role).filter((obj) => isNaN(Number(obj)));

  constructor(
    private formbuilder: FormBuilder,
    public userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    public dialogRef: MatDialogRef<UserEditComponent>,
    @Inject(MAT_DIALOG_DATA) public dialog_data: any
  ) {}

  ngOnInit() {
    this.newUserForm = this.formbuilder.group({
      id: [""],
      name: ["", Validators.required],
      email: ["", Validators.required],
      userRole: ["", Validators.required],
      points: [0, [Validators.required, Validators.min(0)]],
    });

    if (this.dialog_data && this.dialog_data["id"]) {
      this.userService
        .getUser(this.dialog_data["id"])
        .subscribe((data: User) => {
          this.newUserForm.get("id").setValue(data.id);
          this.newUserForm.get("name").setValue(data.name);
          this.newUserForm.get("userRole").setValue(data.userRole);
          this.newUserForm.get("email").setValue(data.email);
        });
    }
  }
  onScroll(event) {} /* for wheel support */

  saveUser(value) {
    let result = new User();
    result.id = value.id;
    result.name = value.name;
    result.email = value.email;
    result.userRole = value.userRole;
    this.userService.saveUser(result).subscribe((s) => {
      this.router.navigateByUrl("/");
      this.dialogRef.close();
      this.userService.loadUsers();
    });
  }
}
