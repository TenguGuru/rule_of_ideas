import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { TokenStorageService } from "../../security/token-storage.service";
import { AuthLoginInfo } from "../../security/login-info";
import { MessageService } from "../../services";

@Component({
  selector: "app-log-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.css"],
})
export class SignInComponent implements OnInit {
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = "";
  userRoles: string[] = [];
  return: string = "";
  mailControl:FormControl;
  hide: boolean = true;
  signInForm: FormGroup;
  private loginInfo: AuthLoginInfo;

  constructor(
    public fb: FormBuilder,
    public userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private tokenStorage: TokenStorageService,
    public messageService: MessageService
  ) {
    this.mailControl = new FormControl("",Validators.email);
    this.signInForm = this.fb.group({
      login: ["", [Validators.required]],
      password: [
        "",
        [
          /*Validators.pattern('regex'),
           Validators.maxLength(25),
           Validators.minLength(6),*/
        ],
      ],
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      (params) => (this.return = params["return"] || "/")
    );
  }

  get login() {
    return this.signInForm.get("login");
  }

  get password() {
    return this.signInForm.get("password");
  }

  onSuccess() {
    this.messageService.initializeWebSocketConnection();
    this.userService.loadCurrentUserData(this.login.value, () =>
      this.router.navigateByUrl(this.return)
    );
  }

  onSubmit() {
    this.mailControl.setValue(this.login.value);
    if(this.mailControl.invalid){
      this.loginInfo = new AuthLoginInfo(null ,this.login.value, this.password.value);
    }else{
      this.loginInfo = new AuthLoginInfo(this.login.value, null, this.password.value);
    }
    this.userService.attemptAuth(this.loginInfo).subscribe(
      (data) => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.userRoles = this.tokenStorage.getAuthorities();
        this.onSuccess();
      },
      (error) => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      }
    );
  }
}
