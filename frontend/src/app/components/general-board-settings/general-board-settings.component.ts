import { Component, OnInit, Input } from '@angular/core';
import { Board } from 'src/app/models';
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: 'app-general-board-settings',
  templateUrl: './general-board-settings.component.html',
  styleUrls: ['./general-board-settings.component.css']
})
export class GeneralBoardSettingsComponent implements OnInit {
  @Input() boardId: string;
  boardName : string;
  board : Board;

  constructor(
    public boardService : BoardService

  ) {

   }

  ngOnInit(): void {
    this.boardService.findById(this.boardId).subscribe((board) => {this.board = board;
      this.boardName = this.board.name;})
  }

  boardNameChange($event){
    this.board.name = this.boardName;
    this.boardService.save(this.board).subscribe();
  }
}
