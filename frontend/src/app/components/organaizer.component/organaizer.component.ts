import { Component, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material';
import { Board } from 'src/app/models';
import { BoardSettings } from "src/app/models/board-settings.model";
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';
import { CreateBoardComponent } from '../create-board.component/create-board.component';
import {BoardSettingsService} from "../../services/board.settings.service";
import { BidService } from "src/app/services/bid.service";
import { ConfirmationDialogComponent } from "../confirmation-dialog/confirmation-dialog.component";
import { Router } from "@angular/router";

@Component({
    selector: "app-organaizer",
    templateUrl: "./organaizer.component.html",
    styleUrls: ["./organaizer.component.css"],
  })
export class OrganaizerComponent implements OnInit{

  boardList:any[];
  boardBidding: boolean;
  displayedColumns: string[] = ['Имя', 'E-mail', 'Роль','Действия'];
    constructor(public service:BoardService,  public dialog: MatDialog,
      public userService:UserService, public boardSettingsService: BoardSettingsService,
      public bidService:BidService, public boardService:BoardService,
      private router: Router){
    }
    ngOnInit(){
      this.getData();
    }

    navigateToBoardEdit(boardId) {
      this.router.navigate(["/boardEdit"], {
        queryParams: {
          boardId: boardId,
        },
      })
    }

    getData(){
      this.service.orgSub.subscribe(sub=>{
        this.service.findByCreator().subscribe((boards:any[])=>{
          this.boardList=boards;
          this.boardList.forEach(board=>{
            board.creator = this.userService.currentUser;
            this.boardSettingsService.getBiddingStatus(board.id);
            this.service.getMembers(board.id).subscribe(
              mem=>{
                board.members = mem;
              }
            )
            this.service.getBoardQuota(board.id).subscribe(
              quota=>{
                board.quota = quota;
              }
            )
          })
        })
      });

      this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    }

    copyLink(board){
      let link =`${window.location.origin}/sign-in-board/${board.creator.id}/${board.name}`;
      const selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = link;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
    }

    copyCode(board){
      let codeValue = board.code;
      const selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = codeValue;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
    }

    updateBiddingStatus(board) {
      this.boardSettingsService.getBiddingStatus(board.id);
      let settings = {
        board: board,
        bidding: !this.boardSettingsService.bidding
      };
      this.boardSettingsService.updateBiddingStatus(settings);
    }

    updateBidding(boardId){
      this.bidService.updateBidding(boardId);
    }

    createConfirmationDialog(row, board:Board): void{
      let data = row ? row : {};
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data,
        width: "15%",
        height: "40%",
        maxWidth: "30%",
      });
      dialogRef.afterClosed().subscribe((result) =>{
        if(result === "confirmed")
          this.updateBidding(board.id);
      })
    }

    changeRole(member,role){
      let changer ={
        memberId:member.id,
        role:role
      }
      this.service.changeRole(changer).subscribe((cngMem:any)=>{
       let brd:Board = this.boardList.find(board=>board.id == member.board.id);
       brd.members.find(mem=>mem.id==member.id).role = cngMem.role;
      })
    }

    createEditBoardDialog(row): void {
      let data = row ? row : {};
      const dialogRef = this.dialog.open(CreateBoardComponent, { data });
      dialogRef.afterClosed().subscribe((result) => {
       this.getData();
      });
    }
}
