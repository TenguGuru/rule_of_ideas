import { Component, OnInit, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { UserService } from 'src/app/services';
import { MatDialog } from '@angular/material/dialog';
import { Idea, Initiative } from "src/app/models";

@Component({
  selector: "app-main",
  templateUrl: `main.component.html`,
  styleUrls: ["main.component.css"],
})
export class MainComponent implements OnInit {
  idea = Idea;
  initiative = Initiative;
  constructor(
    public userService: UserService,
    public dialog: MatDialog,
    @Inject(DOCUMENT) document
  ) {
  }
  ngOnInit(): void {
  }
  
 
}
