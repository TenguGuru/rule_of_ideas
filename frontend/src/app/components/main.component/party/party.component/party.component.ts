import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Board, Party } from "../../../../models";
import { UserService, PartyService } from "../../../../services";
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import { BoardService } from 'src/app/services/board.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';

@Component({
  selector: 'app-party',
  templateUrl: './party.component.html',
  styleUrls: ['./party.component.css']
})
export class PartyComponent implements OnInit {
  @Input() party: Party;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(public userService: UserService,
              public partyService: PartyService,
              public boardService: BoardService,
              public boardSettingsService: BoardSettingsService,
              public _snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.getData();
  }

  joinParty(){

    this.partyService.join(this.party, this.userService.currentUser).subscribe((s:any) => {
      if(s!=null){
        this._snackBar.open('Вы были забанены в этой группе по причине:'+s.description, '', {
          panelClass:"message",
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }else{
        this.userService.currentUser.party = this.party;
        this.partyService.getAll();
        this.partyService.refreshSub();
      }
    });
  }

  getData(){
    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
    this.partyService.getPartyInfo(this.party.id);
  }
}
