import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { User } from 'src/app/models';
import { PartyService } from 'src/app/services/party.service';
import { UserService } from 'src/app/services/user.service';
import {map, startWith} from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: 'app-party-blacklist',
  templateUrl: './party-blacklist.component.html',
  styleUrls: ['./party-blacklist.component.css']
})
export class PartyBlacklistComponent implements OnInit {
  bannedList:any[] = [];
  bannedCol: string[] = ['Имя', 'Причина','Действия'];
    myControl = new FormControl();
    options: User[] = [];
    filteredOptions: Observable<User[]>;
  constructor(public userService:UserService,public partyService: PartyService,
    public dialogRef: MatDialogRef<PartyBlacklistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
      this.getData();
  }

  getData(){
    this.userService.loadUsers().subscribe((users:User[])=>{
      this.options=users;
      this.partyService.getBanned(this.data.party.id).subscribe((banned:any[])=>{
        this.bannedList=banned;
        this.bannedList.forEach(banned=>{
          this.options.splice(this.options.findIndex(val=>val.id == banned.user.id),1);
        })
    })
  })
   
  }
  private _filter(value: string): User[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }
  ban(member){
    this.partyService.createBanDialog(member);
  }
  unban(member){
    this.partyService.unban(member.id,this.data.party.id).subscribe(ans=>{
      this.getData();
    })
  }
}
