import { Component, OnInit, AfterViewInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { BoardSettingsService } from 'src/app/services/board.settings.service';
import { PartyService, UserService } from "../../../../services";
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: 'app-party-list',
  templateUrl: './party-list.component.html',
  styleUrls: ['./party-list.component.css']
})
export class PartyListComponent implements OnInit, OnChanges {
  @Input() active:boolean;
  constructor(public partyService: PartyService,
              public userService:UserService,
              public boardSettingsService:BoardSettingsService,
              public boardService: BoardService) {}

  ngOnChanges(changes: SimpleChanges) {
    if(this.active){
      this.ngOnInit();
    }
  }
  exit(){
    this.partyService.createWarnDialog(this.userService.currentUser);
  }
  ngOnInit() {
    this.getData();
  }

  getData(){

    this.partyService.getPartyMember(this.userService.currentUser.id,
      this.userService.currentUser.currBoard.id).subscribe((pm:any)=>{
      if(pm!=null){
        this.userService.currentUser.party = pm.party;
      }else{
        this.userService.currentUser.party = null;
    }
    });
    this.partyService.getAll();




    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
  }
  
}
