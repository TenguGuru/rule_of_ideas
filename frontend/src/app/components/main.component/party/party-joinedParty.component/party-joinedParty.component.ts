import { Component,Input,OnChanges,OnInit, SimpleChanges } from "@angular/core";
import { Subscription } from 'rxjs/internal/Subscription';
import { UserService } from 'src/app/services';
import { BoardSettingsService } from "src/app/services/board.settings.service";
import { PartyService } from 'src/app/services/party.service';
import { BoardService } from "src/app/services/board.service";

@Component({
    selector: "app-joinedParty-component",
    templateUrl: './party-joinedParty.component.html',
    styleUrls: ['./party-joinedParty.component.css'],
  })
export class JoinedPartyComponent implements OnInit, OnChanges {
    memberCol: string[] = ['Имя', 'Статус','Действия'];
    electCol: string[] = ['Имя', 'Слоган','Действия'];
    party:any;
    currUser:any;
    electDate:string;
    createDate:string;
    chosenCand:any;
    bidding:boolean;
    private subscriptions: Subscription[] = [];
    mainList:any[] = [];
    @Input() active:boolean;
    electList:any[] = [];
    constructor(public partyService: PartyService,
                public userService:UserService,
                public boardSettingsService:BoardSettingsService,
                public boardService: BoardService){}

    ngOnInit(){
        this.partyService.getPartyMember(this.userService.currentUser.id,
            this.userService.currentUser.currBoard.id).subscribe((pm:any)=>{
                if(pm!=null){
                    this.userService.currentUser.party = pm.party;
                  }else{
                      this.userService.currentUser.party = null;
                  }
          });
        this.getData();
      }

    ngOnChanges(changes: SimpleChanges) {//Когда пользователь переключается на эту вкладу, снова грузит данные
        this.subscriptions
        .forEach(s => s.unsubscribe());
        if(this.active){
           this.ngOnInit();
          }
    }

    ngOnDestroy(){
        this.subscriptions
        .forEach(s => s.unsubscribe());
    }

    getData(){
        const subRefr = this.partyService.joinedPartySub.subscribe(res=>{
            this.partyService.get(this.userService.currentUser.party.id).subscribe(
                party=>{
                    this.party=party;
                    let date = new Date(this.party.electDate);
                    this.electDate = date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear();
                    date = new Date(this.party.createDate);
                    this.createDate = date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear();
                    this.partyService.getPartyMembers(this.party.id).subscribe((parMem:any[])=>{
                        this.mainList = [];
                        this.electList = [];
                        parMem.forEach(mem=>{
                            mem.member.party = this.party;
                            mem.member.party.board = this.userService.currentUser.currBoard;
                            if(mem.member.id == this.userService.currentUser.id){
                                this.currUser = mem;
                                this.partyService.getPartyElect(this.currUser.id).subscribe(elect=>{this.chosenCand = elect;});
                            }
                            this.mainList.push(mem);
                            if(mem.candidate==true){
                                    this.electList.push(mem);
                                }
                        });
                    });
                })
            });




        this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
        this.boardService.getCurrentUserRole();


        this.subscriptions.push(subRefr);
    }

    likeCandidate(candidate){
        this.partyService.likeCandidate(this.currUser.id, candidate.id).subscribe(ans=>{
            this.chosenCand = ans;
        });
    }

    addRequest(partyMember){
        let dia = this.partyService.createJoinElectDialog(partyMember);
        dia.afterClosed().subscribe(res=>{
            this.partyService.refreshSub();
            if(this.chosenCand){
                this.partyService.deleteElect(this.chosenCand.id);
            }
        });
    }

    delRequest(partyMember){
        partyMember.slogan = null;
        partyMember.candidate = false;
        this.partyService.savePartyMember(partyMember).subscribe(res=>{
            this.partyService.refreshSub();
        });
    }

    blackList(){
        let data = { party:this.party, user:this.currUser};
        this.partyService.createBlackListDialog(data);
    }

    ban(member){
    this.partyService.createBanDialog(member);
    }

    exit(){
     this.partyService.createWarnDialog(this.userService.currentUser);
    }
}
