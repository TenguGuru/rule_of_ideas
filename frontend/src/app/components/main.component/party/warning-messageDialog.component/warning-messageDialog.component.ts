import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Party, User } from 'src/app/models';
import { UserService } from 'src/app/services';
import { PartyService } from 'src/app/services/party.service';


@Component({
    selector: "app-party-warnDialog",
    templateUrl: './warning-messageDialog.component.html',
    styleUrls: ['./warning-messageDialog.component.css'],
  })

export class WarningMessageDialogComponent {
    user:User;
    constructor(
      public partyService: PartyService,
        public userService:UserService,
        public dialogRef: MatDialogRef<WarningMessageDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: User) {
           this.user = data;
        }
        exit(){
          this.partyService.exitParty(this.user.id);
          this.userService.currentUser.party=null;
          this.dialogRef.close();
        }
}