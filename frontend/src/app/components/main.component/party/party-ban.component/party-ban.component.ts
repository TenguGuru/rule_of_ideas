import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { User } from 'src/app/models';
import { UserService } from 'src/app/services';
import { PartyService } from 'src/app/services/party.service';


@Component({
    selector: "app-party-ban",
    templateUrl: './party-ban.component.html',
    styleUrls: ['./party-ban.component.css'],
  })
export class PartyBan {
    user:User;  
    search:String;
    description:string= "";
    constructor(
      public partyService:PartyService,
      public userSerivce: UserService,
        public dialogRef: MatDialogRef<PartyBan>,
        @Inject(MAT_DIALOG_DATA) public data: User) {
            this.user = data;
        }
  ban(user?:any){
    if(user!=null){
      this.partyService.get(this.userSerivce.currentUser.party.id).subscribe((party:any)=>{
        party.board = this.userSerivce.currentUser.currBoard;
        let ban={
          user:user,
          party:party,
          description:this.description,
          date:new Date()
        }
        this.partyService.ban(ban).subscribe(b=>{
          console.log(b);
          this.partyService.refreshSub();
          this.partyService.closeDialog();
        });
      });
    }
  }
}