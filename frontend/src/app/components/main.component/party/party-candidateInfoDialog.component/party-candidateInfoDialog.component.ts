import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { PartyService } from 'src/app/services/party.service';


@Component({
    selector: "app-party-InfoDialog",
    templateUrl: './party-candidateInfoDialog.component.html',
    styleUrls: ['./party-candidateInfoDialog.component.css'],
  })

export class CandidateInfoDialog {
    candidateInfo:any;
    constructor(
      public partyService: PartyService,
        public dialogRef: MatDialogRef<CandidateInfoDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
          this.candidateInfo = data;
        }
}