import { Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Party } from 'src/app/models';
import { UserService } from 'src/app/services';
import { PartyService } from 'src/app/services/party.service';


@Component({
    selector: "app-party-InfoDialog",
    templateUrl: './party-joinElect.component.html',
    styleUrls: ['./party-joinElect.component.css'],
  })

export class JoinElectComponent{
  partyForm: FormGroup;
    constructor(public partyService: PartyService,
      public fb: FormBuilder, public userService:UserService,
        public dialogRef: MatDialogRef<JoinElectComponent>,
        @Inject(MAT_DIALOG_DATA) public data:any) {
          this.partyForm = this.fb.group({
            slogan : ["",Validators.maxLength(140)],
          });
        }

      Submit(){
        this.data.slogan = this.partyForm.value.slogan;
        this.data.candidate = true;
        this.data.party = this.data.member.party;
        this.partyService.savePartyMember(this.data).subscribe(res=>{
          this.dialogRef.close(this.data);
        });
       
      }
}