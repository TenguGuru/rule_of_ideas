import { Component, Inject } from "@angular/core";
import { MatDialogRef, MatTabChangeEvent, MAT_DIALOG_DATA } from "@angular/material";
import { UserService } from "src/app/services";
import { Idea, Initiative, Party } from 'src/app/models';
import { PartyService } from 'src/app/services/party.service';


@Component({
    selector: "app-party-InfoDialog",
    templateUrl: './party-infoDialog.component.html',
    styleUrls: ['./party-infoDialog.component.css'],
  })

export class PartyInfoDialogComponent {
    partyInfo:any;
    ratesMatching:any;
    ideas: Idea[];
    initiatives: Initiative[];
    selectedIndex: number = 0;
    partyId: number;
    constructor(
        private userService: UserService,
        public partyService: PartyService,
        public dialogRef: MatDialogRef<PartyInfoDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Party) {
            this.partyService.getInfo(data.id).subscribe((info:any)=>{
                this.partyInfo = {title:info.title,leader:info.leader.name, creator:info.creator.name,count:info.membersCount};
            });
            this.partyService.getRatesMatching(this.userService.currentUser.id, data.id).subscribe(
                matching=>{
                    this.ratesMatching = matching;
                    console.log(matching);
                }
            )
            this.partyId = data.id;
            this.partyService.getPartyIdeas(this.partyId).subscribe((ideas: Idea[]) =>{
                this.ideas = ideas;
            });
            this.partyService.getPartyInitiatives(this.partyId).subscribe((initiatives: Initiative[]) =>{
                this.initiatives = initiatives;
            });
        }

        onTabChange(event: MatTabChangeEvent){
            this.selectedIndex = event.index;
            if(event.index == 0){
                this.partyService.getInfo(this.partyId).subscribe((info:any)=>{
                    this.partyInfo = {title:info.title,leader:info.leader.name, creator:info.creator.name,count:info.membersCount};
                });
                this.partyService.getRatesMatching(this.userService.currentUser.id,this.partyId).subscribe(
                    matching=>{
                        this.ratesMatching = matching;
                        console.log(matching);
                    }
                )
            }
            else if (event.index == 1){
                this.partyService.getPartyIdeas(this.partyId).subscribe((ideas: Idea[]) =>{
                    this.ideas = ideas;
                });
            }
            else{
                this.partyService.getPartyInitiatives(this.partyId).subscribe((initiatives: Initiative[]) =>{
                    this.initiatives = initiatives;
                });
            }
        }
}
