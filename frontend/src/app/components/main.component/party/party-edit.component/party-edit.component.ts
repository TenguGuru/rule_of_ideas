import { Component, OnInit, Inject, ViewChild, ViewChildren } from "@angular/core";
import { Party } from "../../../../models";
import { PartyService, UserService } from "../../../../services";
import { FormGroup, FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Color } from '@angular-material-components/color-picker';

@Component({
  selector: 'app-party-edit',
  templateUrl: './party-edit.component.html',
  styleUrls: ['./party-edit.component.css']
})
export class PartyEditComponent implements OnInit {
  public visible = false;

  partyForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    public userService: UserService,
    public partyService: PartyService,
    public dialogRef: MatDialogRef<PartyEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  onSubmit() {
    let party = this.partyService.parties.filter(party => party.title == this.partyForm.value.name)[0];
    if (party != null){
      this.visible = true;
    } else {
      if (this.partyForm.valid) {
        if (this.partyForm.controls.id.value.length != 0) {
          this.partyService.getInfo(this.partyForm.controls.id.value).subscribe((info:any)=>{
            let cur = {
              createDate : new Date(),
              leader : info.leader,
              creator : info.creator,
              id : this.partyForm.controls.id.value,
              board : info.board,
              color : "#" + this.partyForm.controls.color.value.hex,
              title : this.partyForm.controls.title.value,
              ideaLoyalty: 100,
              initiativeLoyalty: 100}
          this.partyService.save(cur).subscribe((s:any) => {
            this.dialogRef.close();
           });
          });
        } else {
          let cur = {
            createDate : new Date(),
            leader : this.userService.currentUser,
            creator : this.userService.currentUser,
            id : this.partyForm.controls.id.value,
            board : this.userService.currentUser.currBoard,
            color : "#" + this.partyForm.controls.color.value.hex,
            title : this.partyForm.controls.title.value,
            ideaLoyalty: 100,
            initiativeLoyalty: 100}
          this.partyService.save(cur).subscribe((s:any) => {
          if (this.userService.currentUser){
            this.userService.currentUser.party = s;
          }
          this.partyService.getAll();
          this.partyService.refreshSub();
          this.dialogRef.close();
          });
        }
      }
    }  
  }

  ngOnInit() {
    this.partyForm = this.fb.group({
      id : [""],
      title: [""],
      color: [""],
    });

    if (this.data && this.data["id"]) {
      this.partyService
        .get(this.data["id"])
        .subscribe((data: Party) => {
          const colorRgb = this.hexToRgb(data.color);

          this.partyForm.get("id").setValue(data.id);
          this.partyForm.get("title").setValue(data.title);
          this.partyForm.get("color").setValue(new Color(colorRgb.r, colorRgb.g, colorRgb.b));
        });
    }
  }

  hexToRgb(hex) {
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, (m, r, g, b) => {
      return r + r + g + g + b + b;
    });
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

}
