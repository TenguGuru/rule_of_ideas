import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MatTabChangeEvent, MAT_DIALOG_DATA } from '@angular/material';
import { Initiative, Idea } from 'src/app/models';
import { AcceptRate, InitiativeIdeasInfo, RateInfo } from 'src/app/models/req-info-interfaces';
import { UserService, PartyService } from 'src/app/services';
import { InitiativeIdeasService } from 'src/app/services/initiative-ideas.service';
import { InitiativeBidService } from 'src/app/services/initiative-bid.service';
import { InitiativeService } from 'src/app/services/initiative.service';
import { InitiativeBid } from 'src/app/models/initiative-bid.model';
import { BidUtilsService } from 'src/app/services/bid.utils.service';

@Component({
  selector: 'app-initiative-detail-info',
  templateUrl: './thougth-detail-info.component.html',
  styleUrls: ['./thougth-detail-info.component.css']
})
export class InitiativeDetailInfoComponent{
  acceptInfo:AcceptRate;
  thought:Initiative;
  info:RateInfo;
  party:number;
  suppList:InitiativeIdeasInfo[]=[];
  counterList:InitiativeIdeasInfo[]=[];
  isLeader: boolean;
  maxBidInfo: any[] = [];
  thoughtBid: InitiativeBid;
  lastPercent: number;
  loyaltyCount: number;
  maxLoyaltyPercent: number;
  membersCount: number;
  MAX_LOYALTY_ATTEMPTS: number = 3;
  tabIndex: number = 0;
  constructor(
    public bidUtilsService: BidUtilsService,
    public thoughtBidService: InitiativeBidService,
    public userService: UserService,
    public partyService: PartyService,
    public thoughtService: InitiativeService,
    private initiativeIdeaService:InitiativeIdeasService,
    public dialogRef: MatDialogRef<InitiativeDetailInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Initiative) {
        this.thought = data;
        this.partyService.isPartyLeader(this.userService.currentUser.id, this.userService.currentUser.currBoard.id).subscribe((memberInfo:boolean)=>{
          this.isLeader=memberInfo;
    })
  }

  ngOnInit(): void {
    this.getCommonData();
    this.getBidData();
  }

  onTabChange(event: MatTabChangeEvent) {
    this.tabIndex = event.index;
    if (event.index == 1) {
      this.getPartyData();
    }
    else if(event.index == 2){
      this.getPartyData();
      this.partyService.getInitiativeLoyaltyPercent(this.userService.currentUser.party.id);
    }
    else {
      this.getCommonData();
    }
  }

  calculateLoyalty(){
    if (this.thoughtBid.loyaltyPercent > this.maxLoyaltyPercent){
      this.thoughtBid.loyaltyPercent = this.maxLoyaltyPercent;
    }
    this.loyaltyCount = this.membersCount * this.thoughtBid.loyaltyPercent * this.info.rate;
  }

  sliderChange(event) {
    if (event.value > this.maxLoyaltyPercent) {
        setTimeout(() => this.thoughtBid.loyaltyPercent = event.source.value = this.maxLoyaltyPercent);
    }
    this.calculateLoyalty();
  }

  saveLoyalty(){
    if (this.thoughtBid.attempt !== 0){
      this.partyService.spendInitiativeLoyaltyPercent(this.userService.currentUser.party.id,
        this.bidUtilsService.getPercentageDifference(
          this.thoughtBid.loyaltyPercent,
          this.lastPercent)).subscribe(
        value => {
          this.partyService.getInitiativeLoyaltyPercent(this.userService.currentUser.party.id).subscribe(initiativeLoyaltyPercent => {
            this.maxLoyaltyPercent = initiativeLoyaltyPercent;
            this.thoughtBidService.getLoyaltyPercent(this.userService.currentUser.party.id, this.thought.id).subscribe(loyaltyPercent => {
              this.maxLoyaltyPercent += loyaltyPercent;
            })
          });
        }
      );
    }
    else{
      this.loyaltyCount = this.thoughtBid.loyaltyPercent * this.membersCount * this.info.rate;
    }

    let cur = new InitiativeBid();

    cur.id = this.thoughtBid.id;
    cur.loyaltyPercent = this.thoughtBid.loyaltyPercent;
    cur.party = this.userService.currentUser.party;
    cur.attempt = this.thoughtBid.attempt;
    cur.thought = this.thought;

    this.thoughtBidService.save(cur).subscribe(
      thoughtBid => {
        this.thoughtBid = thoughtBid;
        this.lastPercent = this.thoughtBid.loyaltyPercent;
      }
    );
  }

  getBidData(){
    if (this.userService.currentUser.party && this.userService.currentUser.id == this.userService.currentUser.party.leader.id){
      this.partyService.getInitiativeLoyaltyPercent(this.userService.currentUser.party.id).subscribe(loyalty => {
        this.maxLoyaltyPercent = loyalty;
        this.thoughtBidService.getLoyaltyPercent(this.userService.currentUser.party.id, this.thought.id).subscribe(loyaltyPercent => {
          this.maxLoyaltyPercent += loyaltyPercent;
        })
      })

      this.thoughtBidService.getByPartyAndThought(this.userService.currentUser.party.id, this.thought.id).subscribe(thoughtBid => {
        this.thoughtBid = thoughtBid;
        if (this.thoughtBid == null){
          this.thoughtBid = new InitiativeBid();
          this.thoughtBid.party = this.userService.currentUser.party;
          this.thoughtBid.thought = this.thought;
          this.thoughtBid.loyaltyPercent = 0;
          this.loyaltyCount = 0;
          this.thoughtBid.attempt = this.MAX_LOYALTY_ATTEMPTS;
        }
        else {
          this.lastPercent = this.thoughtBid.loyaltyPercent;
          if(this.thought.status==1){
            this.thoughtService.getAcceptingInfo(this.thought.id).subscribe((info:AcceptRate)=>{
              this.acceptInfo = info;
            })
          }else{
              this.thoughtService.getThoughtRateInfo(this.data.id).subscribe(initId=>{
                this.info = initId;
                this.calculateLoyalty();
            })
          }
        }
      });

      this.partyService.getPartyMembersCount(this.userService.currentUser.party.id).subscribe(membersCount => {
        this.membersCount = membersCount;
      })
    }
  }

  getCommonData(){
    if(this.thought.status==1){
      this.thoughtService.getAcceptingInfo(this.thought.id).subscribe((info:AcceptRate)=>{
        this.acceptInfo = info;
      })
    }else{
        this.thoughtService.getThoughtRateInfo(this.data.id).subscribe(initId=>{
          this.info = initId;
      })
    }
    this.initiativeIdeaService.getInitiativeIdeasList(this.data.id).subscribe((list:InitiativeIdeasInfo[])=>{
      list.forEach(item=>{
        switch(item.relationship){
          case 1:{
            this.suppList.push(item);
          }
            break;
          case 0:{
            this.counterList.push(item);
          }
            break;
        }
      })
    })
    this.suppList=[];
    this.counterList=[];
  }

  getPartyData(){
    this.party = this.userService.currentUser.party.id;
    if(this.thought.status==1){
      this.thoughtService.getAcceptingPartyInfo(this.thought.id, this.party).subscribe((info:AcceptRate)=>{
        this.acceptInfo = info;
      })
    }else{
        this.thoughtService.getThoughtsPartyRateInfo(this.data.id, this.party).subscribe(initId=>{
          this.info = initId;
      })
    }
    this.initiativeIdeaService.getInitiativeIdeasPartyList(this.data.id, this.party).subscribe((list:InitiativeIdeasInfo[])=>{
      list.forEach(item=>{
        switch(item.relationship){
          case 1:{
            this.suppList.push(item);
          }
            break;
          case 0:{
            this.counterList.push(item);
          }
            break;
        }
      })
    })
    this.thoughtService.getPartyMaxBidInfo(this.userService.currentUser.currBoard.id, this.thought.id, this.userService.currentUser.party.id).subscribe((data:any[]) => {
      this.maxBidInfo = data;
    })
    this.suppList=[];
    this.counterList=[];
  }
}
