import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ThoughtService } from 'src/app/services/thought.service';

@Component({
  selector: 'app-thought-estimate-dialog',
  templateUrl: './thought-estimate-dialog.component.html',
  styleUrls: ['./thought-estimate-dialog.component.css']
})
export class ThoughtEstimateDialogComponent{
  rate:Number;
  addRate: boolean=false;
  neg:boolean = false;
  constructor(
    public dialogRef: MatDialogRef<ThoughtEstimateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if(data.thoughtRate.rate == 0){
        this.neg = true;
      }
      if(data.addRate!=null){
        if (data.addRate > 10) {
          data.addRate = 10;
        }
        this.rate = data.addRate;
        this.addRate=true;
      }else{
        if (this.neg==true && data.thoughtRate.userRate >= 0) {
          this.rate = -5;
        } else if (this.neg==false && data.thoughtRate.userRate <= 0) {
          this.rate = 5;
        } else {
          this.rate = data.thoughtRate.userRate;
        }
      }
    }
    onSubmit(){
      this.dialogRef.close(true);
      this.data.thoughtRate.userRate = this.rate;
    }
    close(){
      this.dialogRef.close(false);
    }
}
