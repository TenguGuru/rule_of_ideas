import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Idea } from 'src/app/models/idea.model';
import { UserService } from 'src/app/services';
import { IdeaService } from 'src/app/services/idea.service';

@Component({
  selector: 'app-idea-edit',
  templateUrl: './idea-edit.component.html',
  styleUrls: ['./idea-edit.component.css']
})
export class IdeaEditComponent {
  ideaForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    public ideaService: IdeaService,
    public userService: UserService,
    public dialogRef: MatDialogRef<IdeaEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Idea,
  ) {
    this.ideaForm = this.fb.group({
      name: ["",Validators.maxLength(100)],
      text: ["",Validators.maxLength(256)],
    });
    if(this.data!=null){
      this.ideaForm.setValue({name:data.title, text: data.text})
      if(data.author.id!=this.userService.currentUser.id  && this.userService.currentUser.userRole!="ADMIN"){
        this.ideaForm.disable();
      }
    }
  }

  onSubmit() {
    if (this.ideaForm.valid) {
      if(this.data == null){
        let cur = new Idea();
        cur.title = this.ideaForm.value.name;
        cur.text = this.ideaForm.value.text;
        cur.author = this.userService.currentUser;
        cur.board = this.userService.currentUser.currBoard;
        this.ideaService.save(cur).subscribe((s) => {
          this.dialogRef.close();
        });
      }else{
        let cur = new Idea();
        cur.status = this.data.status;
        cur.author = this.data.author;
        cur.id = this.data.id;
        cur.board = this.data.board;
        cur.number = this.data.number;
        cur.text = this.ideaForm.value.text;
        cur.title = this.ideaForm.value.name;
        this.ideaService.save(cur).subscribe((s) => {
          this.dialogRef.close();
        });
      }
    }
  }
}
