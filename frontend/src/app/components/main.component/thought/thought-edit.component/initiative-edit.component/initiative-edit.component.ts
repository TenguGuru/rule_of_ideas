import { Component, Inject } from "@angular/core";
import { UserService} from "src/app/services";
import { Initiative} from "src/app/models";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { InitiativeIdeas } from 'src/app/models/initiativeIdeas.model';
import { InitiativeIdeasService } from "src/app/services/initiative-ideas.service";
import { IdeaService } from "src/app/services/idea.service";
import { InitiativeService } from "src/app/services/initiative.service";

@Component({
    selector: "app-initiative-edit",
    templateUrl: './initiative-edit.component.html',
    styleUrls: ['./initiative-edit.component.css'],
  })
  export class InitiativeEditComponent {

    initiativeForm: FormGroup;
    ideas:any[]=[];
    supp:any[]=[];
    counter:any[]=[];
    used_ideas:any[]=[];
    
    constructor(
      public fb: FormBuilder,
      public ideaService: IdeaService,
      public initiativeService: InitiativeService,
      public userService: UserService,
      public initiativeIdeasService: InitiativeIdeasService,
      public dialogRef: MatDialogRef<InitiativeEditComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
    ) { 
      let idArr:any[];
      this.initiativeForm = this.fb.group({
        name: ["",Validators.maxLength(100)],
        text: ["",Validators.maxLength(256)],
      });
      if(data!=null){//Чек на уже существующую инициативу и загрузка данных
        this.ideaService.getAllByBoard(this.data.board.id).subscribe(//Пока пусть будет так, если будут возникать ошибки с отсутствием этих данных, то заменим
          ideas=>{//кусок кода необходимый для загрузки идей в админке
            this.ideas=ideas.map((item):any=>{
              let newItem = new InitiativeIdeas();
              newItem.idea = item;
              return newItem;
            });;
            idArr = this.ideas.map((old)=>{return old.idea.id;});
          }
        )
        this.initiativeForm.setValue({name:data.title, text: data.text})
        this.initiativeIdeasService.getInitiativeIdeas(this.userService.currentUser.id,data.id).
        subscribe(array=>{
          array.forEach(elem=>{
            this.used_ideas.push(elem.id);
            if(idArr.includes(elem.idea.id)){
              this.ideas.splice(idArr.indexOf(elem.idea.id),1)
              idArr.splice(idArr.indexOf(elem.idea.id),1)//Удаление идей, которые уже использовались
            }
            if(elem.relationship == 0){
              this.counter.push(elem);//Запись данных в массивы по идеям
            }else if(elem.relationship == 1){
              this.supp.push(elem);
            }
          })
        })
        if(data.author.id!=this.userService.currentUser.id && this.userService.currentUser.userRole!="ADMIN"){
          this.initiativeForm.disable();
        }
      }else{//Кусок кода, чтобы идеи грузились всегда
        this.ideaService.getAllByBoard(this.userService.currentUser.currBoard.id).subscribe(ideas=>{
          this.ideas = ideas.map((item):any=>{
            let newItem = new InitiativeIdeas();
            newItem.idea = item;
            return newItem;
          });  
        })
      }
  }

    onSubmit() { 
      if ((this.initiativeForm.valid)){
        let cur = new Initiative();
        cur.id = this.data ? this.data.id:null;
        cur.number = this.data ? this.data.number:null;
        cur.status = this.data ? this.data.status:0;
        cur.title = this.initiativeForm.value.name;
        cur.text = this.initiativeForm.value.text;
        cur.author = this.userService.currentUser;
        cur.board = this.userService.currentUser.currBoard;
        this.initiativeService.save(cur).subscribe((s:Initiative) => {
          this.saveRelationshipChanges(s);
        });
      }
    }
    saveRelationshipChanges(data){
      this.saveInitiativeIdeas(this.supp,1,data);
      this.saveInitiativeIdeas(this.counter,0,data);
      this.dialogRef.close();
    }
         

    saveInitiativeIdeas(array:InitiativeIdeas[],relationship:number,initiative:Initiative){
      array.forEach((element:InitiativeIdeas )=> {
        if(!(this.used_ideas.includes(element.id))){
            element.id = element.id ? element.id : null;
            element.number = element.number ? element.number : null;
            element.initiative = initiative;
            element.author = this.userService.currentUser;
            element.relationship = relationship;
          this.initiativeIdeasService.saveInitiativeIdeas(element).subscribe(s=>{
          });
        }
      });
    }

    //Функции перемещения по массивам
    return(array:any[],initIdea:InitiativeIdeas){
      initIdea.relationship=null;
      this.ideas.push(initIdea);
      array.splice(array.indexOf(initIdea),1);
      (initIdea.id!=null || initIdea.relationship!=null) ? this.initiativeIdeasService.deleteInitiativeIdea(initIdea.id).subscribe(s=>console.log(s)):{};//удаление из бд
    }
    inSupp(array:any[],initIdea:InitiativeIdeas){
      this.supp.push(initIdea);
      array.splice(array.indexOf(initIdea),1);
      (initIdea.id!=null) ? this.initiativeIdeasService.deleteInitiativeIdea(initIdea.id).subscribe(s=>console.log(s)):{};//удаление из бд
    }
    inCounter(array:any[],initIdea:InitiativeIdeas){
      this.counter.push(initIdea);
      array.splice(array.indexOf(initIdea),1);
      (initIdea.id!=null) ? this.initiativeIdeasService.deleteInitiativeIdea(initIdea.id).subscribe(s=>console.log(s)):{};//удаление из бд
    }
  }
  