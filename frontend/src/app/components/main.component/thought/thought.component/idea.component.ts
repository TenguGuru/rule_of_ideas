import { Component, Input, OnInit } from '@angular/core';
import { Idea } from 'src/app/models';
import { IdeaService } from 'src/app/services/idea.service';
import { BoardService } from 'src/app/services/board.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';



@Component({
  selector: 'app-idea',
  templateUrl: './thought.component.html',
  styleUrls: ['./thought.component.css']
})
export class IdeaComponent implements OnInit {
  @Input("idea") thought: Idea;
  constructor(public service:IdeaService, public boardService:BoardService,
    public boardSettingsService:BoardSettingsService) { 
  }
  ngOnInit(): void {
    this.boardService.getCurrentUserRole();
  }

}
