import { Component, Input, OnInit } from '@angular/core';
import { Initiative } from 'src/app/models/initiative.model';
import { InitiativeService } from 'src/app/services/initiative.service';
import { BoardService } from 'src/app/services/board.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';



@Component({
  selector: 'app-initiative',
  templateUrl: './thought.component.html',
  styleUrls: ['./thought.component.css']
})
export class InitiativeComponent implements OnInit {
  @Input("initiative") thought: Initiative;
  constructor(public service:InitiativeService, public boardService:BoardService,
    public boardSettingsService:BoardSettingsService) { 
  }
  ngOnInit(): void {
    this.boardService.getCurrentUserRole();
  }

}
