import { Component, Input, OnInit } from '@angular/core';
import { InitiativeService } from 'src/app/services/initiative.service';
import { PartyService } from 'src/app/services/party.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: 'app-initiative-estimate-listrow',
  templateUrl: './thought-estimate-listrow.component.html',
  styleUrls: ['./thought-estimate-listrow.component.css']
})
export class InitiativeEstimateListrowComponent{
  @Input("thoughtRate") thoughtRate:any;
  @Input("estimated") estimated:boolean;
  party: any = null;

  constructor(public service:InitiativeService,
              public partyService: PartyService,
              public userService: UserService,
              public boardSettingsService:BoardSettingsService,
              public boardService:BoardService) {
  }

  ngOnInit(): void {
    this.partyService.getPartyByInitiative(this.thoughtRate.thought.id).subscribe(
      (party: any) =>{
        party ? this.party = {
          title: party.title,
          color: party.color,
        }:null;
      });
    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
  }
}
