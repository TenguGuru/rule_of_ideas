import { Component, Input, OnInit } from '@angular/core';
import { IdeaService } from 'src/app/services/idea.service';
import { PartyService } from 'src/app/services/party.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: 'app-idea-estimate-listrow',
  templateUrl: './thought-estimate-listrow.component.html',
  styleUrls: ['./thought-estimate-listrow.component.css']
})
export class IdeaEstimateListrowComponent{
  @Input("thoughtRate") thoughtRate:any;
  @Input("estimated") estimated:boolean;
  party: any = null;

  constructor(public service:IdeaService,
              public partyService:PartyService,
              public boardSettingsService:BoardSettingsService,
              public boardService:BoardService,
              public userService:UserService) {
  }

  ngOnInit(): void {
    this.partyService.getPartyByIdea(this.thoughtRate.thought.id).subscribe(
      (party: any) =>{
        party ? this.party = {
          title: party.title,
          color: party.color,
        }:null;
      });
    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
  }
}
