import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BoardSettingsService } from 'src/app/services/board.settings.service';
import { IdeaService } from 'src/app/services/idea.service';
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: 'app-idea-list',
  templateUrl: './idea-list.component.html',
  styleUrls: ['./idea-list.component.css']
})
export class IdeaListComponent implements OnInit, OnChanges {
  @Input() active:boolean;
  constructor(public ideaService:IdeaService,public boardSettingsService: BoardSettingsService, public userService: UserService,
    public boardService:BoardService) {
  }

  ngOnInit(){
    this.getData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.active) {
      this.ngOnInit();
    }
  }

  getData(){
    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
  }
}
