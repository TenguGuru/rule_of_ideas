import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Thought } from 'src/app/models/thought.interface';
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';
import { InitiativeService } from 'src/app/services/initiative.service';
@Component({
  selector: 'app-initiative-list',
  templateUrl: './initiative-list.component.html',
  styleUrls: ['./initiative-list.component.css']
})
export class InitiativeListComponent implements OnInit, OnChanges {
  @Input() active:boolean;
  thoughts:Thought[];
  constructor(public initiativeService:InitiativeService,public boardSettingsService: BoardSettingsService, 
    public userService : UserService, public boardService:BoardService) {
  }

  ngOnInit(){
    this.getData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.active) {
      this.ngOnInit();
    }
  }

  getData(){
    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
  }
}
