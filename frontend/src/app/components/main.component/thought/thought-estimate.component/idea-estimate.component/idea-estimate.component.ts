import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog, MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Thought } from 'src/app/models/thought.interface';
import { DataService, UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';
import { BoardSettingsService } from 'src/app/services/board.settings.service';
import { IdeaService } from 'src/app/services/idea.service';
import { ThoughtEstimateDialogComponent } from '../../thought-estimate-dialog.component/thought-estimate-dialog.component';
@Component({
  selector: 'app-idea-estimate',
  templateUrl: './idea-estimate.component.html',
  styleUrls: ['./idea-estimate.component.css']
})
export class IdeaEstimateComponent  implements OnInit, OnChanges {
  @Input() active:boolean;
  private subscriptions: Subscription[] = [];
  dialogRef: any;
  boardStatus = false;
  dataChangedStatus: any;

  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    public boardService:BoardService,
    public boardSettingsService:BoardSettingsService,
    public userService: UserService,
    public ideaService: IdeaService,
    public dialog: MatDialog,
    public dataService: DataService,
    @Inject(DOCUMENT) document
  ) {
    this.dataChangedStatus = this.dataService.getDataChangedAsObservable().subscribe((result) => {
      if (result) {
        this.ideaService.clearData();
        this.initComp();
      }
      console.log(result);
    })
    iconRegistry.addSvgIcon(
      "boring",
      sanitizer.bypassSecurityTrustResourceUrl("assets/boring.svg")
    );
    iconRegistry.addSvgIcon(
      "libra",
      sanitizer.bypassSecurityTrustResourceUrl("assets/libra.svg")
    );
  }
  ngOnChanges(changes: SimpleChanges) {
    if(!this.active){
      this.subscriptions
      .forEach(s => s.unsubscribe());
    }else{
     this.initComp();
    }
  }
  
  
  initComp(){
    this.ideaService.clearData(); 
    this.ideaService.updateThougthSubscribe();
    this.ideaService.getData();
    this.boardSettingsService.getBiddingStatus(this.userService.currentUser.currBoard.id);
    this.boardService.getCurrentUserRole();
    this.boardService.findMember(this.userService.currentUser.id, this.userService.currentUser.currBoard.id).subscribe( member =>{
      this.userService.currentUser.boardRole = member.role;
    });
    this.ideaService.subForSort();
    this.changeBoardClear();
  }
  ngOnInit() {
   this.initComp();
  }

  ngOnDestroy(){
    this.subscriptions
    .forEach(s => s.unsubscribe());
  }

  changeBoardClear(){//Очищение всех списков при смене доски
    const changeBoardSub = this.boardService.boardChangeSub.subscribe(value=>{
      if(value){
        this.ideaService.clearData();
        this.boardService.changeBoard(false);//Чтобы при переключении табов он не чистил списки и не портил весь перфоманс надо дать ему false 
      }
    })
    this.subscriptions.push(changeBoardSub);
  }

  /*
    1. все доступные для его оценки, но ещё не оценённые
    2. одобряемые участником идеи (упорядоченным списком)
    3. неодобряемые участником идеи (упорядоченным списком)
    4. неинтересные ему идеи (к оценке которых участник безразличен)
    5. предложенные им идеи(если есть).
    6. идеи на допуск к оценке(read only), там можно поставить +/- за то, чтобы идея попала на общее голосование.
  */



  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      let data = event.container.data;
      if(event.currentIndex==0){
       if(data[event.currentIndex+1] && (data[event.currentIndex+1].userRate!=data[event.currentIndex].userRate)){
         this.ideaService.createEstimateDialog(data[event.currentIndex],data[event.currentIndex+1].userRate+1);
       }
      }else if(event.currentIndex==(data.length-1)){
        if(data[event.currentIndex-1] && (data[event.currentIndex-1].userRate!=data[event.currentIndex].userRate)){
          this.ideaService.createEstimateDialog(data[event.currentIndex],data[event.currentIndex-1].userRate-1);
        }
      }else {
        if(event.currentIndex<event.previousIndex 
          && data[event.currentIndex+1] &&data[event.currentIndex+1].userRate>data[event.currentIndex].userRate){
            this.ideaService.createEstimateDialog(data[event.currentIndex],data[event.currentIndex+1].userRate+1);
        }else if(event.currentIndex>event.previousIndex
          && data[event.currentIndex-1] &&data[event.currentIndex-1].userRate<data[event.currentIndex].userRate){
            this.ideaService.createEstimateDialog(data[event.currentIndex],data[event.currentIndex-1].userRate-1);
        }
      }
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      let item = event.container.data[event.currentIndex];
      let id = event.container.id;
      if(!(id=="uninterestingThoughts" || id =="uninterestingThoughtsMobile")){
        switch(id){
          case "badThoughts":
          case "badThoughtsMobile":{
            item.rate = 0;
            break;
          }
          case "goodThoughts":
          case "goodThoughtsMobile":{
            item.rate =  2;
            break;
          }
        }
        this.createEstimateDialog(item);
        this.dialogRef.afterClosed().subscribe((result) => {
          if (!result) {
            transferArrayItem(
              event.container.data,
              event.previousContainer.data,
              event.currentIndex,
              event.previousIndex
              );
          } else {
            this.ideaService.rank(item).subscribe((ans:any)=>{
              this.ideaService.used_thoughts.push(item.thought.id);//т.к данные по уже оцененным мы грузим 1 раз, надо пополнять массив уже оцененых
              this.ideaService.sendToSort();
              if(item.id == null){
                item.id = ans.id;
              }});
          }
        });
      }else{
        item.rate = 1;
        item.userRate = 0;
        this.ideaService.rank(item).subscribe((ans:any)=>{
            this.ideaService.used_thoughts.push(item.thought.id);
            if(item.id == null){
              item.id = ans.id;
            }
            item.userRate = 0;
          })
        }
      }
  }
  acceptRate(thought:Thought,rate:number){
    thought.board = this.userService.currentUser.currBoard;
    let user = this.userService.currentUser;
    let accRate = {thought:thought,user:user,rate:rate}
    this.ideaService.saveAcceptRate(accRate).subscribe(res=>{
      this.initComp();
    });


  }
  accept(thought:Thought){
    this.ideaService.accept(thought.id).subscribe(res=>{
      this.ideaService.thoughts_for_judge.splice(this.ideaService.thoughts_for_judge.findIndex(value => value.thought.id ==thought.id),1);
      this.ideaService.updateThougthSubscribe();
    });
  }

  createEstimateDialog(row){//он нужен, т.к надо принимать значения оценки из него
    let data = {thoughtRate: row};
    this.dialogRef = this.dialog.open(ThoughtEstimateDialogComponent, {
      data,
      width: "100%",
      maxWidth: "none",
    });
  }

  mobileNavToBadThoughts() {
    document.getElementsByClassName("mat-tab-body-content")[0].scrollLeft =
      document.getElementsByClassName("mat-tab-body-content")[0].scrollWidth /
      3.325;

    this.updateVisibilityAndOrder({
      ifem: { display: false },
      bim: { display: true, order: 2 },
      gim: { display: true, order: 1 },
      ifjm: { display: false },
      uim: { display: true, order: 3 },
    });
  }

  mobileNavToThoughtsForJudge() {
    document.getElementsByClassName("mat-tab-body-content")[0].scrollLeft = 0;
    this.updateVisibilityAndOrder({
      ifem: { display: false },
      bim: { display: false },
      gim: { display: false },
      ifjm: { display: true },
      uim: { display: false },
    });
  }

  mobileNavToGoodThoughts() {
    document.getElementsByClassName("mat-tab-body-content")[0].scrollLeft =
      document.getElementsByClassName("mat-tab-body-content")[0].scrollWidth /
      3.325;
    this.updateVisibilityAndOrder({
      ifem: { display: false },
      bim: { display: true, order: 1 },
      gim: { display: true, order: 2 },
      ifjm: { display: false },
      uim: { display: true, order: 3 },
    });
  }

  mobileNavToUninteresingThoughts() {
    document.getElementsByClassName("mat-tab-body-content")[0].scrollLeft =
      document.getElementsByClassName("mat-tab-body-content")[0].scrollWidth /
      3.325;
    this.updateVisibilityAndOrder({
      ifem: { display: false },
      bim: { display: true, order: 1 },
      gim: { display: true, order: 3 },
      ifjm: { display: false },
      uim: { display: true, order: 2 },
    });
  }

  mobileNavToThoughtsForEstimation() {
    document.getElementsByClassName("mat-tab-body-content")[0].scrollLeft =
      document.getElementsByClassName("mat-tab-body-content")[0].scrollWidth /
      3.325;
    this.updateVisibilityAndOrder({
      ifem: { display: true, order: 2 },
      bim: { display: true, order: 1 },
      gim: { display: true, order: 3 },
      ifjm: { display: false },
      uim: { display: false },
    });
  }

  updateVisibilityAndOrder(config) {
    let ifem = document.getElementById("ifem");
    ifem.style.setProperty("display", config.ifem.display ? "flex" : "none");
    config.ifem.order && ifem.style.setProperty("order", config.ifem.order);
    (<HTMLElement>ifem.getElementsByClassName("mat-icon")[0]).style.setProperty(
      "align-self",
      this.alignByOrder(config.ifem.order)
    );

    let bim = document.getElementById("bim");
    bim.style.setProperty("display", config.bim.display ? "flex" : "none");
    config.bim.order && bim.style.setProperty("order", config.bim.order);
    (<HTMLElement>bim.getElementsByClassName("mat-icon")[0]).style.setProperty(
      "align-self",
      this.alignByOrder(config.bim.order)
    );

    let gim = document.getElementById("gim");
    gim.style.setProperty("display", config.gim.display ? "flex" : "none");
    config.gim.order && gim.style.setProperty("order", config.gim.order);
    (<HTMLElement>gim.getElementsByClassName("mat-icon")[0]).style.setProperty(
      "align-self",
      this.alignByOrder(config.gim.order)
    );

    let ifjm = document.getElementById("ifjm");
    ifjm.style.setProperty("display", config.ifjm.display ? "flex" : "none");
    config.ifjm.order && ifjm.style.setProperty("order", config.ifjm.order);
    (<HTMLElement>ifjm.getElementsByClassName("mat-icon")[0]).style.setProperty(
      "align-self",
      this.alignByOrder(config.ifjm.order)
    );

    let uim = document.getElementById("uim");
    uim.style.setProperty("display", config.uim.display ? "flex" : "none");
    config.uim.order && uim.style.setProperty("order", config.uim.order);
    (<HTMLElement>uim.getElementsByClassName("mat-icon")[0]).style.setProperty(
      "align-self",
      this.alignByOrder(config.uim.order)
    );
  }

  alignByOrder(order) {
    switch (order) {
      case 1:
        return "flex-end";
      case 2:
        return "center";
      case 3:
        return "flex-start";
      default:
        return "auto";
    }
  }
}
