import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { MatRadioChange, Sort } from '@angular/material';
import { MapService, ThoughtService, UserService, PartyService } from 'src/app/services';;
import { MapListObj, PartyMapInfo } from "src/app/models/req-info-interfaces";
import { IdeaService } from "src/app/services/idea.service";
import { InitiativeService } from "src/app/services/initiative.service";
import { IdeaBidService } from "src/app/services/idea-bid.service";
import { InitiativeBidService } from "src/app/services/initiative-bid.service";
import { Thought } from "src/app/models/thought.interface";
import { Subscription } from "rxjs";

@Component({
  selector: "app-map",
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
})
export class MapComponent implements OnInit, OnChanges {
  matrix:String[];
  radio = "3";
  //objList:MapListObj[];
  //partyList:PartyMapInfo[];
  private subscriptions:Subscription[] = [];
  search:string;
  sortedData:MapListObj[];
  sortedPartyData:PartyMapInfo[];
  maxBidInfo: any[] = [];
  @Input() active:boolean;
  constructor(private service:MapService,
   private ideaService:IdeaService, private initiativeService:InitiativeService, public userService:UserService,
   public partyService:PartyService, private ideaBidService: IdeaBidService, private initiativeBidService: InitiativeBidService)
     {

     }
  ngOnInit() {
    const mapSubRef = this.service.mapSub.subscribe((res)=> {
      this.service.getPartyMatrixColorData().subscribe(
        res=>{
         this.matrix =res;
        }
      );
      this.partyService.getAllPartiesValidatedForMap().subscribe((all:any[])=>{
        this.sortedPartyData = all.slice();
      })
    })
    this.subscriptions.push(mapSubRef);
  }
  ngOnChanges(changes:SimpleChanges){
    switch(this.radio){
      case "1":{
        this.matrix = [];
        this.ideaService.getAllValidatedForMap().subscribe(all=>{
          this.sortedData = all.slice();
        })
        break;
      }
      case "2":{
        this.matrix = [];
        this.initiativeService.getAllValidatedForMap().subscribe(all=>{
          this.sortedData = all.slice();
        })
       break;
       }
       case "3":{
        this.sortedData = [];
        this.matrix = [];
        this.service.getPartyMatrixColorData().subscribe(
          res=>{
           this.matrix =res;
          }
        );
        this.partyService.getAllPartiesValidatedForMap().subscribe((all:any[])=>{
          this.sortedPartyData = all.slice();
        })
        break;
       }
    }
    this.getList();
  }

  ngOnDestroy(){
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  getList(){
   switch(this.radio){
    case "1":{
      this.matrix = [];
      this.ideaService.searchForContaining(this.search).subscribe(
        res=>{
          this.sortedData = res.slice();
        });
      break;
    }
    case "2":{
      this.matrix = [];
      this.initiativeService.searchForContaining(this.search).subscribe(
        res=>{
          this.sortedData = res.slice();
        });
      break;
    }
   }
  }
  getMatrixData(id){
    switch(this.radio){
      case "1":{
        this.matrix = [];
        this.service.getIdeaMatrixColorData(id).subscribe(
          res=>{
            this.matrix = res;
          }
        );
        break;
      }
      case "2":{
        this.matrix = [];
        this.service.getInitiativeMatrixColorData(id).subscribe(
          res=>{
            this.matrix = res;
          }
        );
        break;
      }
    }
  }


  getResults(id){
    switch(this.radio){
      case "1":{
        this.service.setVotingIdeaResult(id).subscribe(res=>{})
        break;
      }
      case "2":{

        break;
      }
    }
  }


  getData(event:MatRadioChange){
    const mapSubRef = this.service.mapSub.subscribe((res) => {
      switch(event.value){
        case "1":{
          this.matrix = [];
          this.ideaService.getAllValidatedForMap().subscribe(all=>{
            this.sortedData = all.slice();
          })
          this.ideaBidService.getAllThoughtBidsForMap(this.userService.currentUser.currBoard.id).subscribe(
            (data: any[]) =>{
              this.maxBidInfo = data;
            }
          )
          break;
        }
        case "2":{
          this.matrix = [];
          this.initiativeService.getAllValidatedForMap().subscribe(all=>{
            this.sortedData = all.slice();
          })
          this.initiativeBidService.getAllThoughtBidsForMap(this.userService.currentUser.currBoard.id).subscribe(
            (data: any[]) =>{
              this.maxBidInfo = data
            }
          )
         break;
         }
         case "3":{
          this.sortedData = [];
          this.matrix = [];
          this.service.getPartyMatrixColorData().subscribe(
            res=>{
             this.matrix =res;
            }
          );
          this.partyService.getAllPartiesValidatedForMap().subscribe((all:any[])=>{
            this.sortedPartyData = all.slice();
          })
          break;
         }
      }
    })
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions.push(mapSubRef);
  }
  sortData(sort: Sort) {
    const data = this.sortedData.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'title': return compare(a.title, b.title, isAsc);
        case 'number': return compare(a.number, b.number, isAsc);
        case 'rate': return compare(a.rate, b.rate, isAsc);
        case 'absRate': return compare(a.absRate, b.absRate, isAsc);
        default: return 0;
      }
    });
  }
  sortPartyData(sort:Sort){
    const data = this.sortedPartyData.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedPartyData = data;
      return;
    }

    this.sortedPartyData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'title': return compare(a.title, b.title, isAsc);
        case 'membersCount': return compare(a.membersCount, b.membersCount, isAsc);
        default: return 0;
      }
    });
  }
  concludeVoting(thoughtId: any){
    console.log("I'm here")
    if (this.radio === "1"){
      return this.ideaService.concludeVoting(thoughtId).subscribe();
    }
    if (this.radio === "2"){
      return this.initiativeService.concludeVoting(thoughtId).subscribe();
    }
  }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
