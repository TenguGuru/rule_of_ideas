import { Component, OnInit } from '@angular/core';
import { PartyEditComponent, InitiativeEditComponent, UserEditComponent, DeleteDialogComponent, CrudTableComponent, IdeaEditComponent } from '..';
import { MatDialog } from '@angular/material';
import { UserService, PartyService, ThoughtService } from 'src/app/services';
import { User, Party, Idea, Initiative } from 'src/app/models';
import { IdeaService } from 'src/app/services/idea.service';
import { InitiativeService } from 'src/app/services/initiative.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  userDataModel = User;
  partyDataModel = Party;
  ideaDataModel = Idea;
  initiativeDataModel= Initiative;
  loadUserData: Function = () => this.userService.loadUsers();
  loadPartyData: Function = () => this.partyService.loadParties();
  loadIdeaData: Function = () => this.ideaService.loadThoughts();
  loadInitiativeData: Function = () => this.initiativeService.loadThoughts();

//TODO пока что после удаления таблицы не обновляются, надо будет - пофиксим
  constructor(
    public dialog: MatDialog, 
    public userService: UserService,
    public partyService: PartyService,
    public ideaService: IdeaService,
    public initiativeService:InitiativeService) {}
  deleteUserDialog(row): void {
    let data = row ? row.id : {};
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.componentInstance.type = "user";
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }

  deletePartyDialog(row): void {
    let data = row ? row.id : {};
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.componentInstance.type = "party";
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }
  deleteIdeaDialog(row): void {
    let data = row ? row.id : {};
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.componentInstance.type = "idea";
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }
  deleteInitiativeDialog(row): void {
    let data = row ? row.id : {};
    const dialogRef = this.dialog.open(DeleteDialogComponent, { data });
    dialogRef.componentInstance.type = "initiative";
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }

  createEditUserDialog(row): void {
    let data = row ? row : {};
    const dialogRef = this.dialog.open(UserEditComponent, { data });
  }

  createEditInitiativeDialog(row): void {
    let data = row ? row : {};
    const dialogRef = this.dialog.open(InitiativeEditComponent, { data });
  }

  createEditIdeaDialog(row): void {
    let data = row ? row : {};
    const dialogRef = this.dialog.open(IdeaEditComponent, { data });
  }

  createEditPartyDialog(row): void {
    let data = row ? row : {};
    const dialogRef = this.dialog.open(PartyEditComponent, { data });
  }

  ngOnInit() {
  }

}
