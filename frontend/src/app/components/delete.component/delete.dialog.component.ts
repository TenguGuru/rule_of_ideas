import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject, OnInit } from "@angular/core";
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: "delete.dialog",
  templateUrl: "./delete.dialog.html",
  styleUrls: ["./delete.dialog.css"],
})
export class DeleteDialogComponent implements OnInit{
  public type : string

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    public dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(){
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteIssue(this.data, this.type);
  }
}
