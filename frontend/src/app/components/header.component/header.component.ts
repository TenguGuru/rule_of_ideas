import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { UserService } from 'src/app/services';
import { TokenStorageService } from "../../security/token-storage.service";
import { AboutProjComponentComponent } from "../about-proj.component/about-proj.component";

@Component({
  selector: "app-header",
  templateUrl: 'header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,
    private token: TokenStorageService, public userService:UserService, public dialog: MatDialog) {}

  ngOnInit() {
  }
  org(){
    this.router.navigate(["/organaizer"]);
  }
  home() {
    this.router.navigate(["/"]);
  }
  admin() {
    this.router.navigate(["/admin"]);
  }
  myProfile() {
    this.router.navigate(["/my-profile"]);
  }
  logout() {
    this.token.signOut();
    window.location.reload();
  }
  logoutFromBoard() {
    this.token.boardLogout();
    this.userService.currentUser.currBoard = null;
    this.userService.currentUser.party = null;
    this.router.navigate(["/sign-in-board"]);
  }
  aboutProj(){
    this.dialog.open(AboutProjComponentComponent, {
      width: "100%",
      maxWidth: "none",
    });
  }
}
