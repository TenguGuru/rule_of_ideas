import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { BoardService } from 'src/app/services/board.service';
import { TokenStorageService } from "src/app/security/token-storage.service"
import { Board } from 'src/app/models/board.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User } from 'src/app/models';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { PartyService } from 'src/app/services';
import { ThoughtService } from "src/app/services/thought.service";
import { IdeaService } from "src/app/services/idea.service";
import { InitiativeService } from "src/app/services/initiative.service";
import { BoardSettingsService } from "src/app/services/board.settings.service";

@Component({
  selector: "app-sign-in-board",
  templateUrl: "./sign-in-board.component.html",
  styleUrls: ["./sign-in-board.component.css"],
})
export class SignInBoardComponent implements OnInit{
  boardForm: FormGroup;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  //boards:Board[] = [];
  board:Board = new Board();
    constructor(public route: ActivatedRoute,public fb: FormBuilder, public userService: UserService,public router: Router,
      public service:BoardService, public token:TokenStorageService,  public _snackBar: MatSnackBar, private ideaService:IdeaService, 
      public partyService:PartyService, private initiativeService:InitiativeService, private boardSettingsSerivce:BoardSettingsService){
        this.boardForm = this.fb.group({
          code: [""],
        });
    }
  ngOnInit(): void {
  this.route.params.subscribe(params=>{
    if(params.boardName!=null && params.orgId!=null){
      this.service.findByName(params.boardName).subscribe(board=>{
        this.addBoard(board);
        this.router.navigate(["/sign-in-board"]);
      })}
  });
  this.getData();
  }
  getData(){
    this.service.getBoards(this.userService.currentUser.id);
  }
  addBoard(board?){
      if(board){
        let saveElem = {boardCode:board.code,
          userId:this.userService.currentUser.id}
        this.service.addBoard(saveElem).subscribe((res:Board)=>{
          if(res!=null){
           this.service.boardList.push(res);
          }
        })
      }else{
      let saveElem = {boardCode:this.boardForm.value.code,
        userId:this.userService.currentUser.id}
      this.service.addBoard(saveElem).subscribe((res:Board)=>{
        if(res==null){
          this._snackBar.open('Вы уже есть в данной доске или данной доски не существует', '', {
            panelClass:"message",
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }else{
          this.service.boardList.push(res);
        }
        this.boardForm.reset();
      })
    }
    }
    joinBoard(board:Board){
      this.token.signInBoard();
      this.service.findMember(this.userService.currentUser.id,board.id).subscribe(
        member=>{
          this.partyService.getPartyMember(this.userService.currentUser.id,board.id).subscribe(
            (pm:any)=>{
              this.userService.currentUser.boardRole=member.role;
              this.userService.currentUser.currBoard=board;
              if(pm!=null){
                this.userService.currentUser.party = pm.party;
              }else{
                this.userService.currentUser.party = null;
              }
              this.ideaService.clearData();
              this.ideaService.getData();
              this.ideaService.subForSort();
              this.boardSettingsSerivce.getBiddingStatus(board.id);
              this.ideaService.getAllUserThoughts();
              this.initiativeService.getAllUserThoughts();
              this.router.navigate(["/"]);
            }
          )
        }
      );
    }
}
