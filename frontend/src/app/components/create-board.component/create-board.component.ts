import { Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Board } from 'src/app/models';
import { UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';

@Component({
  selector: "app-create-board",
  templateUrl: "./create-board.component.html",
  styleUrls: ["./create-board.component.css"],
})
export class CreateBoardComponent {
  boardForm: FormGroup;
  board:Board = new Board();
 constructor(
   public dialogRef: MatDialogRef<CreateBoardComponent>,  
   public fb: FormBuilder,public userService:UserService,
   public service:BoardService,
   @Inject(MAT_DIALOG_DATA) public data: Board){
  this.boardForm = this.fb.group({
    name: [""],
  });
  if(data!=null){
    this.boardForm.setValue({name:data.name})
  }
 }
 onSubmit(){
   this.board.name=this.boardForm.value.name;
   this.board.code=this.boardForm.value.code;
   this.board.creator = this.userService.currentUser;
   if(this.data==null){
   this.board.createDate = new Date();
   }
   this.service.save(this.board).subscribe((brd:Board)=>{
     let saveElem = {boardName:brd.name,
      boardCode:brd.code,
      userId:this.userService.currentUser.id,
    role:"creator"};
     this.service.addBoard(saveElem).subscribe(res=>{
     });
     this.service.closeDialog();
   })
 }
}