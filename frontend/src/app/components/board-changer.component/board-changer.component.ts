import { Component, OnInit } from '@angular/core';
import { MatOptionSelectionChange } from '@angular/material';
import { Router } from '@angular/router';
import { Board } from 'src/app/models';
import { TokenStorageService } from 'src/app/security/token-storage.service';
import { MapService, PartyService, UserService } from 'src/app/services';
import { BoardService } from 'src/app/services/board.service';
import { IdeaService } from 'src/app/services/idea.service';
import { InitiativeService } from 'src/app/services/initiative.service';

@Component({
  selector: 'app-board-changer',
  templateUrl: './board-changer.component.html',
  styleUrls: ['./board-changer.component.css']
})
export class BoardChangerComponent implements OnInit {
  boardList: Board[]=[];
  selectedBoard:Board;
  constructor(private router: Router, private token: TokenStorageService, public userService:UserService,
    public boardService:BoardService,private initiativeService:InitiativeService, private ideaService:IdeaService,
    private partyService:PartyService, private mapService:MapService) { }

  ngOnInit() {
    this.selectedBoard = this.userService.currentUser.currBoard;
    this.boardService.getBoards(this.userService.currentUser.id);
  }
  joinBoard(board){
    this.token.signInBoard();
    this.boardService.findMember(this.userService.currentUser.id,board.id).subscribe(
      member=>{
        this.partyService.getPartyMember(this.userService.currentUser.id,board.id).subscribe(
          (pm:any)=>{
            this.userService.currentUser.boardRole=member.role;
            this.userService.currentUser.currBoard=board;
            if(pm!=null){
              this.userService.currentUser.party = pm.party;
            }else{
              this.userService.currentUser.party =null;
            }
            this.boardService.changeBoard(true);
            this.ideaService.updateThougthSubscribe();
            this.ideaService.clearData();
            this.ideaService.getData();
            this.ideaService.subForSort();
            this.ideaService.getAllUserThoughts();
            this.initiativeService.updateThougthSubscribe();
            this.initiativeService.clearData();
            this.initiativeService.getData();
            this.initiativeService.subForSort();
            this.initiativeService.getAllUserThoughts();
            this.partyService.getAll();
            this.partyService.refreshSub();
            this.mapService.refreshSub();
            this.ngOnInit();
          }
        )
      }
    )
  }
}
