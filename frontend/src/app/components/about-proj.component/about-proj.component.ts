import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-about-proj.component',
  templateUrl: './about-proj.component.html',
  styleUrls: ['./about-proj.component.css']
})
export class AboutProjComponentComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AboutProjComponentComponent>) { }

  ngOnInit(): void {
  }

}
