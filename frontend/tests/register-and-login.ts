import { ClientFunction, Selector } from 'testcafe';
const scrollIntoView = ClientFunction( (selector: Selector) => {
const element = selector() as unknown as HTMLElement;

element.scrollIntoView();
});
fixture`HTMLElement`
    .page('http://localhost:4200/sign-up');

test('Register new user', async t => {
    const userNameInput = Selector('input').withAttribute('placeholder','Name');
    const userEmailInput = Selector('input').withAttribute('placeholder','Email');
    const userPasswordInput = Selector('input').withAttribute('placeholder','Password');
    const registerButton = Selector('button').withText("Sign-up");

    await t
        .typeText(userNameInput, 'tau3')
        .expect(userNameInput.value).eql('tau3');

    await t
        .typeText(userEmailInput, 'tau3@gmail.com');

    await t
        .typeText(userPasswordInput, '123');


    await t.click(registerButton);
});

test('Register already existed user', async t => {
    const userNameInput = Selector('input').withAttribute('placeholder','Name');
    const userEmailInput = Selector('input').withAttribute('placeholder','Email');
    const userPasswordInput = Selector('input').withAttribute('placeholder','Password');
    const registerButton = Selector('button').withText("Sign-up");

    const errorWarning = Selector('mat-error').withAttribute('role', 'alert');

    await t
        .typeText(userNameInput, 'tau3')
        .expect(userNameInput.value).eql('tau3');

    await t
        .typeText(userEmailInput, 'tau3@gmail.com')
        .expect(userEmailInput.value).eql('tau3@gmail.com');

    await t
        .typeText(userPasswordInput, '123')
        .expect(userPasswordInput.value).ok();


    
    await t.click(registerButton).wait(50);
    await t
        .expect(errorWarning.textContent).eql('User with such name/email already exists');
});

