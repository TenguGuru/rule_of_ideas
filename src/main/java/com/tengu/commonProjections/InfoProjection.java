package com.tengu.commonProjections;

public interface InfoProjection {
    Double getRate();
    Double getAveragePlusRate();
    Double getAverageNegRate();
}
