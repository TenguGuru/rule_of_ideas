package com.tengu.commonProjections;

import java.util.UUID;

public interface AverageAcceptRateProjection {
    UUID getId();
    Double getRate();
}
