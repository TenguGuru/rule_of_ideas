package com.tengu.commonProjections;

public interface AcceptRateProjection {
    Integer getPlus();
    Integer getMinus();
}
