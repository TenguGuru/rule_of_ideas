package com.tengu.commonProjections;

import java.util.UUID;

public interface MapListSortInfoProjection {
    UUID getId();
    Long getNumber();
    String getTitle();
    Double getRate();
    Double getAbsRate();
}
