package com.tengu.parties;

import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardService;
import com.tengu.parties.entities.*;
import com.tengu.parties.projections.MatchingOfRates;
import com.tengu.parties.projections.PartyInfoProjection;
import com.tengu.parties.projections.PartyListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.parties.services.PartyService;
import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.entities.Initiative;
import com.tengu.thoughts.services.IdeaBidService;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import com.tengu.users.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/parties")
public class PartyController {
    private PartyService service;
    private UserRepository userRepository;
    private BoardService boardService;
    public PartyController(PartyService service, UserRepository userRepository, BoardService boardService){
        this.service = service;
        this.userRepository = userRepository;
        this.boardService = boardService;
    }

    @GetMapping(value = {"", "/"})
    public Iterable<Party> findAll(){
        return this.service.findAll();
    }

    @GetMapping("/board/{boardId}")
    public Iterable<Party> boardParties(@PathVariable UUID boardId){
        return this.service.boardParties(boardId);
    }

    @GetMapping(value = {"/{id}"})
    public Optional<Party> findById(@PathVariable UUID id){
        return this.service.findById(id);
    }


    @PostMapping("/save")
    public Party save(@RequestBody Party party, Principal principal){
        User user = this.userRepository.findByEmail(principal.getName()).get();
        Party result;
        if(user.getUserRole() == User.UserRole.ADMIN) {
            party.setBoard(this.service.findById(party.getId()).get().getBoard());
            result = this.service.update(party);
        }
        else {
            UUID board_id = party.getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
            Party old_party = null;
            PartyMembers pm = this.service.getPartyMember(user.getId(),party.getBoard().getId());
            if(pm!=null){
                old_party = pm.getParty();
            }

            if (old_party == null || party.getId() == null || !party.getId().equals(old_party.getId())) {
                party.setElectDate(party.getCreateDate().plusMonths(1));
                result = this.service.save(party);
                this.service.join(party.getId(), user.getId());
            } else {
                result = this.service.update(party);
            }
        }

        return result;
    }

    @GetMapping("/join/{party_id}/{user_id}")
    public PartyBan join(@PathVariable UUID party_id, @PathVariable UUID user_id, Principal principal){
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = this.service.findById(party_id).stream().findAny().get().getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user_id, board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return this.service.join(party_id, user_id);
    }

    @GetMapping("/getPartyByIdea/{ideaId}")
    public Party getPartyByIdea(@PathVariable UUID ideaId){
        return this.service.getPartyByIdea(ideaId);
    }

    @GetMapping("/getPartyByInitiative/{initiativeId}")
    public Party getPartyByInitiative(@PathVariable UUID initiativeId){
        return this.service.getPartyByInitiative(initiativeId);
    }

    @PostMapping("/savePartyIdea/")
    public PartyIdeas savePartyIdea(@RequestBody PartyIdeas partyIdeas){
        return this.service.savePartyIdea(partyIdeas);
    }

    @PostMapping("/savePartyInitiative/")
    public PartyInitiatives savePartyInitiative(@RequestBody PartyInitiatives partyInitiatives){
        return this.service.savePartyInitiative(partyInitiatives);
    }

    @GetMapping("/getPartyIdeas/{partyId}")
    public Iterable<Idea> getPartyIdeas(@PathVariable UUID partyId){
        return this.service.getPartyIdeas(partyId);
    }

    @GetMapping("/getPartyInitiatives/{partyId}")
    public Iterable<Initiative> getPartyInitiatives(@PathVariable UUID partyId){
        return this.service.getPartyInitiatives(partyId);
    }

    @GetMapping("/getInfo/{partyId}")
    public PartyInfoProjection getPartyInfo(@PathVariable UUID partyId){
        return this.service.getPartyInfo(partyId);
    }

    @GetMapping("/getMembers/{partyId}")
    public Iterable<PartyMembers> getPartyMembers(@PathVariable UUID partyId){
        return  this.service.getPartyMembers(partyId);
    }

    @GetMapping("getAllPartiesValidatedForMap/{boardId}")
    public Iterable<PartyListSortInfoProjection> getAllPartiesValidatedForMap(@PathVariable UUID boardId) {
        return this.service.getAllPartiesValidatedForMap(boardId);
    }

    @PostMapping("/savePartyMember/")
    public PartyMembers savePartyMember(@RequestBody PartyMembers partyMembers){
        return  this.service.savePartyMember(partyMembers);
    }

    @GetMapping("/savePartyElect/{voterId}/{candidateId}")
    public PartyElect savePartyElect(@PathVariable(value = "voterId") UUID voterId, @PathVariable(value = "candidateId") UUID candidateId){
        return  this.service.savePartyElect(voterId,candidateId);
    }

    @GetMapping("/getPartyElect/{voterId}")
    public PartyElect getPartyElect(@PathVariable(value = "voterId") UUID voterId){
        return  this.service.getPartyElect(voterId);
    }
    @DeleteMapping("/deletePartyElect/{electId}")
    public ResponseEntity<?> deletePartyElect(@PathVariable(value = "electId") UUID electId){
        this.service.deletePartyElect(electId);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("Deleted successfully!"), HttpStatus.OK);
    }
    @GetMapping("/getMatrixMap/{boardId}")
    public List<List<String>> getMatrixMap(@PathVariable UUID boardId){
        return  this.service.getMatrixMap(this.boardService.findById(boardId).get());
    }

    @GetMapping("/exitFromParty/{boardId}/{userId}")
    public ResponseEntity<?> exitFromParty(@PathVariable(value = "userId") UUID userId,
                                           @PathVariable(value = "boardId") UUID boardId, Principal principal){
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            BoardMember.MemberRole memberRole = boardService.findMember(userId, boardId).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        this.service.exitFromParty(userId,boardId);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("You exit the party!"),HttpStatus.OK);
    }

    @PostMapping("/ban/")
    public ResponseEntity<?> partyBan(@RequestBody PartyBan partyBan, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = partyBan.getParty().getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        this.service.ban(partyBan);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage(partyBan.getUser().getName()+" was banned!"),HttpStatus.OK);
    }
    @GetMapping("/unban/{userId}/{partyId}")
    public ResponseEntity<?> partyBan(@PathVariable("userId") UUID userId,
                                      @PathVariable("partyId") UUID partyId) {
        this.service.deleteBan(userId, partyId);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("User unbanned!"),HttpStatus.OK);
    }
    @GetMapping("/banned/{partyId}")
    public Iterable<PartyBan> getBannedId(@PathVariable UUID partyId){
        return this.service.getBanned(partyId);
    }
    @GetMapping("/getPM/{userId}/{boardId}")
    public PartyMembers getPartyMember(@PathVariable("userId") UUID userId,
                                       @PathVariable("boardId") UUID boardId){
        return this.service.getPartyMember(userId, boardId);
    }

    @GetMapping("/isPartyLeader/{userId}/{boardId}")
    public boolean isPartyLeader(@PathVariable("userId") UUID userId,
                            @PathVariable("boardId") UUID boardId){
        return this.service.isPartyLeader(userId, boardId);
    }


    @GetMapping("/getRatesMatching/{userId}/{partyId}")
    public MatchingOfRates getPartyRatesMatching(@PathVariable("userId") UUID userId, @PathVariable("partyId") UUID partyId){
        return this.service.getPartyRatesMatching(userId, partyId);
    }

    @GetMapping("/getPartyMembersCount/{partyId}")
    public Integer getPartyMembersCount(@PathVariable("partyId") UUID partyId){
        return this.service.getPartyMembersCount(partyId);
    }

    @GetMapping("/getInitiativeLoyaltyPercent/{partyId}")
    public Integer getInitiativeLoyaltyPercent(@PathVariable("partyId") UUID partyId){
        return this.service.getInitiativeLoyaltyPercent(partyId);
    }

    @GetMapping("/getIdeaLoyaltyPercent/{partyId}")
    public Integer getIdeaLoyaltyPercent(@PathVariable("partyId") UUID partyId){
        return this.service.getIdeaLoyaltyPercent(partyId);
    }

    @PostMapping("/spendIdeaLoyaltyPercent/{partyId}")
    public Integer spendIdeaLoyaltyPercent(@PathVariable("partyId") UUID partyId, @RequestBody Integer ideaLoyalty){
        return this.service.spendIdeaLoyaltyPercent(partyId, ideaLoyalty);
    }

    @PostMapping("/spendInitiativeLoyaltyPercent/{partyId}")
    public Integer spendInitiativeLoyaltyPercent(@PathVariable("partyId") UUID partyId, @RequestBody Integer initiativeLoyalty){
        return this.service.spendInitiativeLoyaltyPercent(partyId, initiativeLoyalty);
    }
}
