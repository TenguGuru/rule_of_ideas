package com.tengu.parties.repositories;

import com.tengu.parties.projections.ElectCalculateProjection;
import com.tengu.parties.entities.PartyElect;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
@org.springframework.stereotype.Repository
public interface PartyElectRepository extends PagingAndSortingRepository<PartyElect, UUID> {

    @Query("SELECT pe from PartyElect pe where pe.voter.id = :voterId")
    PartyElect getByVoter(@Param("voterId") UUID voterId);

    @Query("SELECT " +
            "count(pe) as count," +
            "pe.candidate.id as candidateId" +
            " from PartyElect pe where pe.voter.party.id = :partyId " +
            "GROUP BY pe.candidate.id")
    Iterable<ElectCalculateProjection> electCalculate(@Param("partyId") UUID partyId);

    @Transactional
    @Modifying
    @Query("DELETE from PartyElect where id in (SELECT pe from PartyElect pe where pe.voter.party.id = :partyId)")
    void deleteByParty(@Param("partyId") UUID partyId);

    @Transactional
    @Modifying
    @Query("DELETE from PartyElect " +
            "where id in " +
            "(SELECT pe from PartyElect pe " +
                "where pe.voter.id = :partyMemId or " +
                "pe.candidate.id = :partyMemId)")
    void deleteByPartyMember(@Param("partyMemId") UUID partyMemId);
}