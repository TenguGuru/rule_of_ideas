package com.tengu.parties.repositories;

import com.tengu.parties.entities.PartyMembers;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
public interface PartyMembersRepository extends PagingAndSortingRepository<PartyMembers, UUID> {

    @Query("SELECT p from PartyMembers p where p.party.id = :partyId")
    Iterable<PartyMembers> getPartyMembers(@Param("partyId") UUID partyId);

    @Query("SELECT p from PartyMembers p where p.member.id = :userId and p.party.board.id = :boardId")
    PartyMembers getPartyMemberByUser(@Param("userId") UUID userId,@Param("boardId") UUID boardId);

    @Query("select pm from PartyMembers pm where pm.party.id = :partyId and pm.member.id <> :userId")
    List<PartyMembers> getPmForDelete(@Param("partyId")UUID partyId, @Param("userId")UUID userId);

    @Transactional
    @Modifying
    @Query("UPDATE PartyMembers pm set pm.candidate = false, pm.slogan = null where pm.party.id = :partyId and pm.candidate = true")
    void deleteAllCandidates(@Param("partyId") UUID partyId);

    @Query("SELECT count(*) FROM PartyMembers pm WHERE pm.party.id = :partyId")
    Integer getPartyMembersCount(@Param("partyId") UUID partyId);
}
