package com.tengu.parties.repositories;

import com.tengu.parties.entities.PartyBan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PartyBanRepository extends CrudRepository<PartyBan, UUID> {

    @Query("SELECT b from PartyBan b where b.user.id =:userId and b.party.id=:partyId")
    PartyBan findByUserAndParty(@Param("userId") UUID userId, @Param("partyId") UUID partyId);

    @Query("SELECT b from PartyBan b where b.party.id =:partyId")
    Iterable<PartyBan> getBanned(@Param("partyId") UUID partyId);
}
