package com.tengu.parties.repositories;

import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyThought;
import com.tengu.thoughts.entities.Thought;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@NoRepositoryBean
public interface PartyThoughtRepository<E extends PartyThought, T extends Thought> extends CrudRepository<E, UUID> {
    Iterable<T> getPartyThoughts(UUID partyId);

    Party getPartyByThought(UUID thoughtId);

    void deleteByThought(UUID thoughtId);

    void deleteByParty(UUID partyId);

    void update(UUID partyId, UUID thoughtId);

    E existsByThoughtId(UUID thoughtId);
}
