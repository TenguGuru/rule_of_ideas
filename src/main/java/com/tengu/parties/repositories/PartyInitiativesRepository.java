package com.tengu.parties.repositories;

import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyInitiatives;
import com.tengu.thoughts.entities.Initiative;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
public interface PartyInitiativesRepository extends PartyThoughtRepository<PartyInitiatives, Initiative> {

    @Query("SELECT pi.initiative from PartyInitiatives pi where pi.party.id = :partyId")
    Iterable<Initiative> getPartyThoughts(@Param("partyId") UUID partyId);
    @Query("SELECT pi.party from PartyInitiatives pi where pi.initiative.id = :initiativeId")
    Party getPartyByThought(@Param("initiativeId") UUID initiativeId);

    @Transactional
    @Modifying
    @Query("DELETE FROM PartyInitiatives pi where pi.initiative.id = :initiativeId")
    void deleteByThought(@Param("initiativeId") UUID initiativeId);

    @Transactional
    @Modifying
    @Query("DELETE FROM PartyInitiatives pi where pi.party.id = :partyId")
    void deleteByParty(@Param("partyId") UUID partyId);

    @Transactional
    @Modifying
    @Query("UPDATE PartyInitiatives pi set pi.party.id = :partyId where pi.initiative.id = :thoughtId")
    void update(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Query("SELECT pi FROM PartyInitiatives pi WHERE pi.initiative.id = :thoughtId")
    PartyInitiatives existsByThoughtId(@Param("thoughtId") UUID thoughtId);

}
