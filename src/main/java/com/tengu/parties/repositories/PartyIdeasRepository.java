package com.tengu.parties.repositories;

import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyIdeas;
import com.tengu.thoughts.entities.Idea;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
public interface PartyIdeasRepository extends PartyThoughtRepository<PartyIdeas, Idea> {

    @Query("SELECT pi.idea from PartyIdeas pi where pi.party.id = :partyId")
    Iterable<Idea> getPartyThoughts(@Param("partyId") UUID partyId);
    @Query("SELECT pi.party from PartyIdeas pi where pi.idea.id = :ideaId")
    Party getPartyByThought(@Param("ideaId") UUID ideaId);

    @Transactional
    @Modifying
    @Query("DELETE FROM PartyIdeas pi where pi.idea.id = :ideaId")
    void deleteByThought(@Param("ideaId") UUID ideaId);

    @Transactional
    @Modifying
    @Query("DELETE FROM PartyIdeas pi where pi.party.id = :partyId")
    void deleteByParty(@Param("partyId") UUID partyId);

    @Transactional
    @Modifying
    @Query("UPDATE PartyIdeas pi set pi.party.id = :partyId where pi.idea.id = :thoughtId")
    void update(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Query("SELECT pi FROM PartyIdeas pi WHERE pi.idea.id = :thoughtId")
    PartyIdeas existsByThoughtId(@Param("thoughtId") UUID thoughtId);
}
