package com.tengu.parties.repositories;

import com.tengu.parties.projections.PartyColorProjection;
import com.tengu.parties.projections.PartyInfoProjection;
import com.tengu.parties.entities.Party;
import com.tengu.parties.projections.PartyListSortInfoProjection;
import org.springframework.data.jpa.repository.Modifying;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
public interface PartyRepository extends PagingAndSortingRepository<Party, UUID> {

    @Query("SELECT " +
            "p.title as title," +
            "p.creator as creator," +
            "p.leader as leader," +
            "p.board as board," +
            "(SELECT count(pm) from PartyMembers pm where pm.party.id = :partyId ) as membersCount " +
            "from Party p " +
            "where p.id = :partyId")
    PartyInfoProjection getPartyInfo(@Param("partyId") UUID partyId);

    @Query("SELECT p from Party p where p.board.id =:boardId")
    Iterable<Party> boardParty(@Param("boardId") UUID boardId);

    @Query("SELECT u.party.color as color from PartyMembers u where u.party.board.id =:boardId")
    List<PartyColorProjection> getAllPartyColors(@Param("boardId") UUID boardId);

    @Query("SELECT p.title as title, p.color as color, " +
            "(SELECT count(pm) from PartyMembers pm where pm.party.id = p.id ) as membersCount " +
            "from Party p where p.board.id = :boardId")
    Iterable<PartyListSortInfoProjection> getAllPartiesValidatedForMap(@Param("boardId") UUID boardId);

    @Transactional
    @Modifying
    @Query("UPDATE Party p set p.ideaLoyalty = p.ideaLoyalty + :ideaLoyalty where p.id =:partyId")
    void restoreIdeaLoyalty(@Param("partyId") UUID partyId, @Param("ideaLoyalty") Integer ideaLoyalty);

    @Transactional
    @Modifying
    @Query("UPDATE Party p set p.initiativeLoyalty = p.initiativeLoyalty + :initiativeLoyalty where p.id =:partyId")
    void restoreInitiativeLoyalty(@Param("partyId") UUID partyId, @Param("initiativeLoyalty") Integer initiativeLoyalty);

    @Transactional
    @Modifying
    @Query("UPDATE Party p set p.ideaLoyalty = 100, p.initiativeLoyalty = 100 where p.id = :partyId")
    void restoreAllPartyLoyaltyForBidding(@Param("partyId") UUID partyId);

    @Transactional
    @Modifying
    @Query("UPDATE Party p set p.ideaLoyalty = p.ideaLoyalty - :ideaLoyalty where p.id =:partyId")
    Integer spendIdeaLoyaltyPercent(@Param("partyId") UUID partyId, @Param("ideaLoyalty") Integer ideaLoyalty);

    @Transactional
    @Modifying
    @Query("UPDATE Party p set p.initiativeLoyalty = p.initiativeLoyalty - :initiativeLoyalty where p.id =:partyId")
    Integer spendInitiativeLoyaltyPercent(@Param("partyId") UUID partyId, @Param("initiativeLoyalty") Integer initiativeLoyalty);

    @Query("SELECT p.initiativeLoyalty from Party p where p.id = :partyId")
    Integer getInitiativeLoyaltyPercent(@Param("partyId") UUID partyId);

    @Query("SELECT p.ideaLoyalty from Party p where p.id = :partyId")
    Integer getIdeaLoyaltyPercent(@Param("partyId") UUID partyId);
}

