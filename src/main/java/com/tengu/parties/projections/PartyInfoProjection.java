package com.tengu.parties.projections;

import com.tengu.boards.Board;
import com.tengu.users.User;

public interface PartyInfoProjection {
    String getTitle();
    User getCreator();
    User getLeader();
    Board getBoard();
    Integer getMembersCount();
}
