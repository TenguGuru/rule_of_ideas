package com.tengu.parties.projections;

public interface PartyColorProjection {
    String getColor();
}
