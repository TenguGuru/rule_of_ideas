package com.tengu.parties.projections;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MatchingOfRates {
    //% коэффициент совпадения оценок пользователя и средних по партии
    private Double ideaMatch;
    private Double initiativeMatch;

    public void setIdeaMatch(Double ideaMatch) {
        this.ideaMatch = ideaMatch;
    }

    public void setInitiativeMatch(Double initiativeMatch) {
        this.initiativeMatch = initiativeMatch;
    }

}
