package com.tengu.parties.projections;

import java.util.UUID;

public interface ElectCalculateProjection {
    Integer getCount();
    UUID getCandidateId();
}

