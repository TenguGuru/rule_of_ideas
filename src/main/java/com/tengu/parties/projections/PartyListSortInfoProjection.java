package com.tengu.parties.projections;

import com.tengu.users.User;

public interface PartyListSortInfoProjection {
    String getTitle();
    String getColor();
    Integer getMembersCount();
}
