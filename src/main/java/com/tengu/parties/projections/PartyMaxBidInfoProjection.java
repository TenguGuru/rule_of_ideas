package com.tengu.parties.projections;

public interface PartyMaxBidInfoProjection {
    String getTitle();
    Integer getMaxBid();
}
