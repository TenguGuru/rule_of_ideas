package com.tengu.parties.entities;

import com.tengu.boards.BoardSettings;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class PartyElect {
    @GeneratedValue
    @Id
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "candidate_id", columnDefinition="uuid not null")
    private PartyMembers candidate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "voter_id", columnDefinition="uuid not null")
    private PartyMembers voter;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PartyMembers getCandidate() {
        return candidate;
    }

    public void setCandidate(PartyMembers candidate) {
        this.candidate = candidate;
    }

    public PartyMembers getVoter() {
        return voter;
    }

    public void setVoter(PartyMembers voter) {
        this.voter = voter;
    }
}
