package com.tengu.parties.entities;

import com.tengu.thoughts.entities.Initiative;
import com.tengu.thoughts.entities.Thought;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PartyInitiatives implements PartyThought {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "initiative_id", nullable = false)
    private Initiative initiative;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "party_id", columnDefinition="uuid not null")
    private Party party;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) { this.id = id; }

    public Thought getThought() { return initiative; }

    public void setThought(Thought thought) { this.initiative = (Initiative) thought; }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

}
