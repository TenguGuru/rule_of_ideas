package com.tengu.parties.entities;

import com.fasterxml.jackson.annotation.*;
import com.tengu.boards.Board;
import com.tengu.users.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
public class Party {
    @Id
    @GeneratedValue
    private UUID id;
    @Column(unique = true)
    String title;
    String color;
    private LocalDate createDate;
    private LocalDate electDate;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "leader_id", columnDefinition="uuid not null")
    private User leader;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id", columnDefinition="uuid not null")
    private Board board;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "creator_id", columnDefinition="uuid not null")
    private User creator;
    private Integer ideaLoyalty = 100;
    private Integer initiativeLoyalty = 100;

    public Party(){

    }
    public Party(UUID id, String title, String color, LocalDate createDate, LocalDate electDate, User leader, Board board, User creator){
        this.id = id;
        this.title = title;
        this.color = color;
        this.createDate = createDate;
        this.electDate = electDate;
        this.leader = leader;
        this.board = board;
        this.creator = creator;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public User getLeader() {
        return leader;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    public LocalDate getElectDate() {
        return electDate;
    }

    public void setElectDate(LocalDate electDate) {
        this.electDate = electDate;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
    /*
    public Set<User> getUsers() { return users;}

    public void addUser(User user) { this.users.add(user); }

    public void removeUser(User user) { this.users.remove(user); }*/

    @Override
    public String toString() {
        return "Party{" + "id=" + id + ", title='" + title + '\'' + ", color='" + color + '\'' + '}';
    }

    public int getIdeaLoyalty() {
        return ideaLoyalty;
    }

    public void setIdeaLoyalty(int ideaLoyalty) {
        this.ideaLoyalty = ideaLoyalty;
    }

    public int getInitiativeLoyalty() {
        return initiativeLoyalty;
    }

    public void setInitiativeLoyalty(int initiativeLoyalty) {
        this.initiativeLoyalty = initiativeLoyalty;
    }
}
