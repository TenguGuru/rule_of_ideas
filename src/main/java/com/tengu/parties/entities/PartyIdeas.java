package com.tengu.parties.entities;


import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.entities.Thought;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PartyIdeas implements PartyThought {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idea_id", nullable = false)
    private Idea idea;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "party_id", columnDefinition="uuid not null")
    private Party party;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) { this.id = id; }

    public Thought getThought() { return idea; }

    public void setThought(Thought thought) { this.idea = (Idea) thought; }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

}
