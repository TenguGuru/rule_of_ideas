package com.tengu.parties.entities;

import com.tengu.thoughts.entities.Thought;

import javax.persistence.Entity;
import java.util.UUID;


public interface PartyThought {
    UUID getId();
    void setId(UUID id);
    Thought getThought();
    void setThought(Thought thought);
    Party getParty();
    void setParty(Party party);
}
