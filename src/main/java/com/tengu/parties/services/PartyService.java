package com.tengu.parties.services;

import com.tengu.boards.Board;
import com.tengu.parties.entities.*;
import com.tengu.parties.projections.MatchingOfRates;
import com.tengu.parties.projections.PartyInfoProjection;
import com.tengu.parties.projections.PartyListSortInfoProjection;
import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.entities.Initiative;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PartyService {
        Iterable<Party> findAll();
        Iterable<Party> boardParties(UUID boardId);
        Optional<Party> findById(UUID id);
        PartyBan join(UUID party_id, UUID user_id);
        Party save(Party party);
        void delete(Party party);
        Party update(Party party);
        PartyInfoProjection getPartyInfo(UUID partyId);
        Iterable<PartyMembers> getPartyMembers(UUID partyId);
        PartyMembers savePartyMember(PartyMembers partyMembers);
        PartyElect savePartyElect(UUID voterId, UUID candidateId);
        PartyElect getPartyElect(UUID voterId);
        void deletePartyElect(UUID electId);
        void calculateElection();
        List<List<String>> getMatrixMap(Board board);
        void exitFromParty(UUID userId, UUID boardId);
        void ban(PartyBan partyBan);
        void deleteBan(UUID userId, UUID partyId);
        Iterable<PartyBan> getBanned(UUID partyId);
        PartyMembers getPartyMember(UUID userId, UUID boardId);
        MatchingOfRates getPartyRatesMatching(UUID userId, UUID partyId);
        PartyIdeas savePartyIdea(PartyIdeas partyIdea);
        PartyInitiatives savePartyInitiative(PartyInitiatives partyInitiative);
        Iterable <Idea> getPartyIdeas(UUID partyId);
        Iterable <Initiative> getPartyInitiatives(UUID partyId);
        Party getPartyByIdea(UUID ideaId);
        Party getPartyByInitiative(UUID initiativeId);
        Iterable<PartyListSortInfoProjection> getAllPartiesValidatedForMap(UUID boardId);
        boolean isPartyLeader(UUID userId, UUID boardId);
        Integer getPartyMembersCount(UUID partyId);
        Integer getInitiativeLoyaltyPercent(UUID partyId);
        Integer getIdeaLoyaltyPercent(UUID partyId);
        Integer spendIdeaLoyaltyPercent(UUID partyId, Integer ideaLoyalty);
        Integer spendInitiativeLoyaltyPercent(UUID partyId, Integer initiativeLoyalty);
}
