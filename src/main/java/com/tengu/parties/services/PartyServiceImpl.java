package com.tengu.parties.services;

import com.tengu.boards.*;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.parties.entities.*;
import com.tengu.parties.projections.*;

import com.tengu.parties.repositories.*;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.services.IdeaService;
import com.tengu.thoughts.services.InitiativeService;
import com.tengu.thoughts.services.ThoughtService;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;
import java.util.*;

@org.springframework.stereotype.Service
public class PartyServiceImpl implements PartyService {
    @Autowired
    private PartyRepository repository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PartyMembersRepository partyMembersRepository;
    @Autowired
    private PartyElectRepository partyElectRepository;
    @Autowired
    private BoardMemberRepository boardMemberRepository;
    @Autowired
    private PartyBanRepository banRepository;
    @Autowired
    private IdeaService ideaService;
    @Autowired
    private InitiativeService initiativeService;
    @Autowired
    private PartyIdeasRepository partyIdeasRepository;
    @Autowired
    private PartyInitiativesRepository partyInitiativesRepository;
    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;

    @Autowired
    private BoardSettingsService boardSettingsService;

    public PartyServiceImpl(PartyRepository repository, PartyMembersRepository partyMembersRepository, PartyElectRepository partyElectRepository,
        UserRepository userRepository, PartyBanRepository banRepository, PartyIdeasRepository partyIdeasRepository, PartyInitiativesRepository partyInitiativesRepository,
                            BoardMemberRepository boardMemberRepository, BoardSettingsService boardSettingsService, IdeaService ideaService, InitiativeService initiativeService){
        this.repository = repository;
        this.partyMembersRepository = partyMembersRepository;
        this.partyElectRepository = partyElectRepository;
        this.userRepository = userRepository;
        this.banRepository = banRepository;
        this.partyIdeasRepository = partyIdeasRepository;
        this.partyInitiativesRepository = partyInitiativesRepository;
        this.boardMemberRepository = boardMemberRepository;
        this.boardSettingsService = boardSettingsService;
        this.ideaService = ideaService;
        this.initiativeService = initiativeService;
    }

    public PartyServiceImpl(){

    }

    @Override
    public Iterable<Party> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Iterable<Party> boardParties(UUID boardId) {
        return this.repository.boardParty(boardId);
    }

    @Override
    public Optional<Party> findById(UUID id) {
        return this.repository.findById(id);
    }

    @Override
    public Party save(Party party) {
        Party saved = this.repository.save(party);
        //brokerMessagingTemplate.convertAndSend("/message", party.toString());
        return saved;
    }

    @Override
    public PartyBan join(UUID party_id, UUID user_id) {
        Party p = this.repository.findById(party_id).get();
        User u = this.userRepository.findById(user_id).get();
        PartyBan pb = this.banRepository.findByUserAndParty(user_id,party_id);
        if (pb == null) {
            Party old_p = null;
            PartyMembers pm = this.partyMembersRepository.getPartyMemberByUser(user_id,p.getBoard().getId());
            if(pm == null){
                pm = new PartyMembers();
                pm.setMember(u);
            }else{
                old_p = pm.getParty();
            }
            pm.setCandidate(false);
            pm.setParty(p);
            this.partyMembersRepository.save(pm);
            this.userRepository.save(u);
            if (old_p != null) {//TODO Воткнуть это в выход из партии
                List<PartyMembers> pmPartyToDelete = this.partyMembersRepository.getPmForDelete(old_p.getId(), user_id);
                if (pmPartyToDelete.size() < 1) {
                    this.repository.delete(old_p);
                }
            }
            return null;
        }
        /*old_p.removeUser(u);
        p.addUser(u);*/
        return pb;

    }

    public void delete(Party party){
        this.banRepository.deleteAll(this.banRepository.getBanned(party.getId()));
        this.partyElectRepository.deleteByParty(party.getId());
        this.partyMembersRepository.getPartyMembers(party.getId()).forEach(partyMembers ->
        {
            this.partyMembersRepository.delete(partyMembers);
        });
        this.partyIdeasRepository.deleteByParty(party.getId());
        this.partyInitiativesRepository.deleteByParty(party.getId());
        this.repository.delete(party);
    }

    public Party update(Party party){
        Party newParty = this.repository.findById(party.getId()).get();
        newParty.setColor(party.getColor());
        newParty.setTitle(party.getTitle());
        return this.repository.save(newParty);
    }

    @Override
    public PartyInfoProjection getPartyInfo(UUID partyId) {
        return this.repository.getPartyInfo(partyId);
    }

    @Override
    public Iterable<PartyMembers> getPartyMembers(UUID partyId) {
        return partyMembersRepository.getPartyMembers(partyId);
    }

    @Override
    public PartyMembers savePartyMember(PartyMembers partyMembers) {
        return this.partyMembersRepository.save(partyMembers);
    }

    @Override
    public PartyElect savePartyElect(UUID voterId, UUID candidateId) {
        PartyElect partyElect = this.partyElectRepository.getByVoter(voterId);
        PartyMembers candidate = this.partyMembersRepository.findById(candidateId).get();
        if (partyElect == null) {
            partyElect = new PartyElect();
            partyElect.setVoter(this.partyMembersRepository.findById(voterId).get());
        }
        partyElect.setCandidate(candidate);
        return this.partyElectRepository.save(partyElect);
    }

    @Override
    public PartyElect getPartyElect(UUID voterId) {
        return this.partyElectRepository.getByVoter(voterId);
    }

    @Override
    public void deletePartyElect(UUID electId) {
        this.partyElectRepository.deleteById(electId);
    }

    @Override
    @Scheduled(cron = "0 0 0 */1 * *")
    public void calculateElection() {
        Iterable<ElectCalculateProjection> electCalculates ;
        Iterable<Party> parties = this.repository.findAll();
        List<ElectCalculateProjection> win = new ArrayList<>();

        PartyMembers winner;
        Integer max = null;
        Integer compared;
        for (Party party :parties) {

            if(party.getElectDate().isBefore(LocalDate.now().plusDays(1)) && !boardSettingsService.isBidding(party.getBoard().getId())){
                electCalculates =this.partyElectRepository.electCalculate(party.getId());

                for(ElectCalculateProjection electCalculateProjection : electCalculates){
                    compared= electCalculateProjection.getCount();
                    if(max!=null){
                        max = Integer.max(compared,max);
                    }else{
                        max=compared;
                    }
                }
                for(ElectCalculateProjection electCalculateProjection :electCalculates){
                    if(electCalculateProjection.getCount().equals(max)){
                            win.add(electCalculateProjection);
                    }
                }
                if(win.size()==1){
                    winner=this.partyMembersRepository.findById(win.get(0).getCandidateId()).get();
                    party.setLeader(winner.getMember());
                }
                win.clear();
                party.setElectDate(LocalDate.now().plusMonths(1));
                this.partyMembersRepository.deleteAllCandidates(party.getId());
                this.partyElectRepository.deleteByParty(party.getId());
                this.repository.save(party);
            }
        }
    }

    @Override
    public List<List<String>> getMatrixMap(Board board) {
        int usersCount = this.boardMemberRepository.getMembers(board.getId()).size();
        int objCount = (int) Math.ceil(Math.sqrt(usersCount));
        List<PartyColorProjection> colors = this.repository.getAllPartyColors(board.getId());
        List<List<String>> result = new ArrayList<>();
        List<String> tempArr = new ArrayList<>();
        List<String> convArr;
        for (int i = objCount; i > 0; i--) {
            for (int  j = objCount; j > 0; j--) {
                if(usersCount!=0) {
                    if (colors.size() != 0) {
                        int ind = (int) (Math.random() * colors.size());
                        tempArr.add(colors.get(ind).getColor());
                        colors.remove(ind);
                    } else {
                        tempArr.add("#999999");
                    }
                    usersCount--;
                }else{
                    tempArr.add(null);
                }
            }
            convArr = new ArrayList<>(tempArr.subList(0, objCount));
            result.add(convArr);
            tempArr.clear();
        }
        return result;
    }

    @Override
    public void exitFromParty(UUID userId, UUID boardId) {
        PartyMembers pm = this.partyMembersRepository.getPartyMemberByUser(userId,boardId);
        Party old_p = pm.getParty();
        this.partyElectRepository.deleteByPartyMember(pm.getId());
        this.partyMembersRepository.delete(pm);
        this.userRepository.save(pm.getMember());
        if ((this.partyMembersRepository.getPartyMembersCount(old_p.getId())) < 1) {
            this.repository.delete(old_p);
        }
    }

    @Override
    public void ban(PartyBan partyBan) {
        PartyMembers pm = this.partyMembersRepository.getPartyMemberByUser(partyBan.getUser().getId(),
                partyBan.getParty().getBoard().getId());
        if(pm!=null) {
            if (pm.getParty().getId().equals(partyBan.getParty().getId())){
                this.userRepository.save(partyBan.getUser());
                this.partyMembersRepository.delete(pm);
            }
        }
        this.banRepository.save(partyBan);
    }

    @Override
    public void deleteBan(UUID userId, UUID partyId) {
        this.banRepository.delete(this.banRepository.findByUserAndParty(userId, partyId));
    }

    @Override
    public Iterable<PartyBan> getBanned(UUID partyId) {
        return this.banRepository.getBanned(partyId);
    }

    @Override
    public PartyMembers getPartyMember(UUID userId, UUID boardId){
        return this.partyMembersRepository.getPartyMemberByUser(userId, boardId);
    }

    @Override
    public MatchingOfRates getPartyRatesMatching(UUID userId, UUID partyId) {
        MatchingOfRates result = new MatchingOfRates();
        UUID boardId = this.repository.findById(partyId).get().getBoard().getId();
        Iterable<IdeaRate> userIdeaRates = this.ideaService.getRatedByUser(userId,boardId);
        Iterable<InitiativeRate> userInitiativesRate = this.initiativeService.getRatedByUser(userId,boardId);
        try{
            result.setIdeaMatch(getMatch(userIdeaRates,partyId,this.ideaService));
        }catch(ArithmeticException e){
            result.setIdeaMatch((double) 0);
        }
        try{
            result.setInitiativeMatch(getMatch(userInitiativesRate,partyId,this.initiativeService));
        }catch(ArithmeticException e){
            result.setInitiativeMatch((double) 0);
        }
        return result;
    }

    private double getMatch(Iterable<? extends ThoughtRate> userRates,UUID partyId,ThoughtService service){
        List<Double> matchList = new ArrayList<>();
        for(ThoughtRate userRate:userRates) {
            InfoProjection partyRate = service.getThoughtPartyRateInfo(userRate.getThought().getId(), partyId);
            Double rate = (double) userRate.getUserRate();
            if (partyRate.getRate() > rate) {
                matchList.add(partyRate.getRate() - rate);
            } else {
                matchList.add(rate - partyRate.getRate());
            }
        }
        return getAverageMatch(matchList);
    }

    private double getAverageMatch(List<Double> list){
        double result = list.stream().mapToDouble(d -> d)
                .average()
                .orElse(0.0);
        return  result>0 ? result:-result;
    }
    @Override
    public PartyIdeas savePartyIdea(PartyIdeas partyIdeas){
        return this.partyIdeasRepository.save(partyIdeas);
    }
    @Override
    public PartyInitiatives savePartyInitiative(PartyInitiatives partyInitiatives){
        return this.partyInitiativesRepository.save(partyInitiatives);
    }
    @Override
    public Iterable <Idea> getPartyIdeas(UUID partyId){
        return this.partyIdeasRepository.getPartyThoughts(partyId);
    }
    @Override
    public Iterable <Initiative> getPartyInitiatives(UUID partyId){
        return this.partyInitiativesRepository.getPartyThoughts(partyId);
    }
    @Override
    public Party getPartyByIdea(UUID ideaId){ return this.partyIdeasRepository.getPartyByThought(ideaId); }

    @Override
    public Party getPartyByInitiative(UUID initiativeId) { return this.partyInitiativesRepository.getPartyByThought(initiativeId); }

    @Override
    public Iterable<PartyListSortInfoProjection> getAllPartiesValidatedForMap(UUID boardId) {
        return this.repository.getAllPartiesValidatedForMap(boardId);
    }

    @Override
    public boolean isPartyLeader(UUID userId, UUID boardId){
        if(this.getPartyMember(userId, boardId)!=null) {
            return userId.equals(this.getPartyMember(userId, boardId).getParty().getLeader().getId());
        }
        return false;
    }

    @Override
    public Integer getPartyMembersCount(UUID partyId) {
        return this.partyMembersRepository.getPartyMembersCount(partyId);
    }

    @Override
    public Integer getInitiativeLoyaltyPercent(UUID partyId) {
        return this.repository.getInitiativeLoyaltyPercent(partyId);
    }

    @Override
    public Integer getIdeaLoyaltyPercent(UUID partyId) {
        return this.repository.getIdeaLoyaltyPercent(partyId);
    }

    @Override
    public Integer spendIdeaLoyaltyPercent(UUID partyId, Integer ideaLoyalty) {
        return this.repository.spendIdeaLoyaltyPercent(partyId, ideaLoyalty);
    }

    @Override
    public Integer spendInitiativeLoyaltyPercent(UUID partyId, Integer initiativeLoyalty) {
        return this.repository.spendInitiativeLoyaltyPercent(partyId, initiativeLoyalty);
    }
}