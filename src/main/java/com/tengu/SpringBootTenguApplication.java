package com.tengu;


import com.tengu.boards.AddBoard;
import com.tengu.boards.Board;
import com.tengu.boards.BoardService;
import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyMembers;
import com.tengu.parties.services.PartyService;
import com.tengu.users.User;
import com.tengu.users.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.UUID;

@EnableScheduling
@SpringBootApplication
public class SpringBootTenguApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTenguApplication.class, args);

	}

	//создание админки
	@Bean
	CommandLineRunner runner(UserService userService, PasswordEncoder encoder, BoardService boardService, PartyService partyService) {
		return args -> {
			if(userService.canRegister("admin","admin@gmail.com")) {
				User user1 = userService.save(
						new User(
								UUID.randomUUID(),
								"admin",
								"admin@gmail.com",
								encoder.encode("jhp521yuk09bd"),
								User.UserRole.ADMIN,
								false,
								true
						)
				);
				/*User organizer = userService.save(
						new User(
								UUID.randomUUID(),
								"org",
								"org@gmail.com",
								encoder.encode("123"),
								User.UserRole.ORGANIZER,
								false,
								true
						)
				);
				Board board = new Board();
				board.setCreator(organizer);
				board.setName("Test");
				board = boardService.save(board);
				AddBoard toAddOrg = new AddBoard();
				toAddOrg.setBoardCode(board.getCode());
				toAddOrg.setUserId(organizer.getId());
				boardService.addBoard(toAddOrg);
				Party party = new Party();
				party.setElectDate(LocalDate.now());

				party.setColor("#FF0000");
				party.setTitle("ho");
				party.setCreator(organizer);
				party.setLeader(organizer);
				party.setBoard(board);
				party = partyService.save(party);
				PartyMembers pm = new PartyMembers();
				pm.setMember(organizer);
				pm.setParty(party);
				partyService.savePartyMember(pm);
				for (int i = 0; i < 100; ++i) {
					User temp = userService.save(
							new User(
									UUID.randomUUID(),
									"user" + Integer.toString(i),
									"user" + Integer.toString(i) + "@gmail.com",
									encoder.encode("123"),
									User.UserRole.USER,
									false,
									true
							)
					);
					AddBoard toAdd = new AddBoard();
					toAdd.setBoardCode(board.getCode());
					toAdd.setUserId(temp.getId());
					boardService.addBoard(toAdd);
					PartyMembers partyMembers = new PartyMembers();
					partyMembers.setMember(temp);
					partyMembers.setParty(party);
					partyService.savePartyMember(partyMembers);
				}*/
			}
		};
	}
}

