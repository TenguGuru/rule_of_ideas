package com.tengu.users;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Users")
@Table(name="tengu_user", uniqueConstraints = {
		@UniqueConstraint(columnNames = {
				"name"
		}),
		@UniqueConstraint(columnNames = {
				"email"
		})
})
public class User {
	public static enum UserRole {
		ADMIN, USER, ORGANIZER;
        public static UserRole getById(String id){
            for(UserRole e : values()) {
                if(e.name().equalsIgnoreCase(id)) return e;
            }
            return ORGANIZER;
        }

	}

	@Id
	@GeneratedValue()
	private UUID id;

	private String name;

	private String email;

	private String password;

	public UUID getId() {return id;}

	@Builder.Default
	private UserRole userRole = UserRole.ORGANIZER;

	@Builder.Default
	private Boolean locked = false;

	@Builder.Default
	private Boolean enabled = false;
}
