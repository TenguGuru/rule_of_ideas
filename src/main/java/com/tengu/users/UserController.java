package com.tengu.users;

import com.tengu.security.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/users")
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	private UserService userService;

	private ConfirmationTokenService confirmationTokenService;

	public UserController(UserService userService, ConfirmationTokenService confirmationTokenService){}

	public ConfirmationTokenService getConfirmationTokenService() {
		return confirmationTokenService;
	}

	@GetMapping(value = {"", "/"})
	public Iterable<User> findAll(){
		return this.userService.findAll();
	}

	@GetMapping("/{login}")
	public ResponseEntity<User> getUser(@PathVariable String login) {
		var userByName = this.userService.getUserByName(login);
		return ResponseEntity.ok().body(userByName.isEmpty() ? this.userService.getUserByEmail(login):userByName.get());
	}

	@PostMapping("/auth/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody com.tengu.controllers.message.request.LoginForm loginRequest){
		User userEntity;
		if(loginRequest.getEmail() == null){
			userEntity = this.userService.getUserByName(loginRequest.getUsername()).get();
		}else{
			userEntity = this.userService.getUserByEmail(loginRequest.getEmail());
		}
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(userEntity.getEmail(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		this.userService.save(userEntity);
		return ResponseEntity.ok(new com.tengu.controllers.message.response.JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
	}

	@PostMapping("/auth/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody com.tengu.controllers.message.request.SignUpForm signUpRequest) {
		if(!this.userService.canRegister(signUpRequest.getNickName(), signUpRequest.getEmail()))
			return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("Email / Nickname is already in use"),
					HttpStatus.BAD_REQUEST);

		/* id, name, email, password, userRole, locked, enabled */
		User user = new User(
				UUID.randomUUID(),
				signUpRequest.getNickName(),
				signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()),
				User.UserRole.ORGANIZER,
				false,
				true
		);
		this.userService.save(user);
		return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}

	@GetMapping("/auth/sign-up/confirm")
	String confirmMail(@RequestParam("token") String token) {

		Optional<ConfirmationToken> optionalConfirmationToken = confirmationTokenService.findConfirmationTokenByToken(token);

		optionalConfirmationToken.ifPresent(userService::confirmUser);

		return "redirect:/sign-in";
	}

	@GetMapping("get/{id}")
	User getUser(@PathVariable("id") UUID id){
		return this.userService.getUserById(id);
	}

	@PostMapping("/save")
	User saveUser(@RequestBody User user){
		User fullUser = this.userService.getUserById(user.getId());
		user.setPassword(fullUser.getPassword());
		return this.userService.save(user);
	}
}
