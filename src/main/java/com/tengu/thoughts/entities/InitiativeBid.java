package com.tengu.thoughts.entities;

import com.tengu.parties.entities.Party;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class InitiativeBid implements ThoughtBid{
    @Id
    @GeneratedValue
    private UUID id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "initiative_id", columnDefinition = "uuid not null")
    private Initiative thought;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "party_id", columnDefinition = "uuid not null")
    private Party party;
    private int loyaltyPercent;
    private int attempt;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    public Initiative getThought() {
        return thought;
    }

    public void setThought(Thought thought) {
        this.thought = new Initiative(thought);
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public int getLoyaltyPercent() {
        return loyaltyPercent;
    }

    public void setLoyaltyPercent(int loyaltyPercent) {
        this.loyaltyPercent = loyaltyPercent;
    }

    @Override
    public int getAttempt() {
        return attempt;
    }

    @Override
    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    @Override
    public String toString() {
        return "InitiativeBid{" +
                "id=" + id +
                ", thought=" + thought +
                ", party=" + party +
                ", loyaltyPercent=" + loyaltyPercent +
                ", attempt=" + attempt +
                '}';
    }
}
