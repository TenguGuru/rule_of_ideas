package com.tengu.thoughts.entities;

import com.tengu.users.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class InitiativeIdeas {
    @Id
    @GeneratedValue
    private UUID id;
    @GeneratedValue
    private Long number;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id", columnDefinition="uuid not null")
    private User author;
    @ManyToOne(optional = false)
    @JoinColumn(name = "idea_id", nullable = false)
    private Idea idea;
    @ManyToOne(optional = false)
    @JoinColumn(name = "initiative_id", nullable = false)
    private Initiative initiative;
    private int relationship;//0-против,1-поддерживает

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public Long getNumber() {
        return number;
    }

    public Idea getIdea() {
        return idea;
    }

    public void setIdea(Thought idea) {
        this.idea = new Idea(idea);
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Initiative getInitiative() {
        return initiative;
    }

    public void setInitiative(Thought initiative) {
        this.initiative = new Initiative(initiative);
    }

    public int getRelationship() {
        return relationship;
    }

    public void setRelationship(int relationship) {
        this.relationship = relationship;
    }
}
