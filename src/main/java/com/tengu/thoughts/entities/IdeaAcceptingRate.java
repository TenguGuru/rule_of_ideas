package com.tengu.thoughts.entities;

import com.tengu.users.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class IdeaAcceptingRate implements ThoughtAcceptingRate {

    @Id
    @GeneratedValue
    private UUID id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idea_id", columnDefinition="uuid not null")
    private Idea thought;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", columnDefinition="uuid not null")
    private User user;
    private int rate;//-1/1

    public IdeaAcceptingRate(ThoughtAcceptingRate thoughtAcceptingRate){
        this.id = thoughtAcceptingRate.getId();
        this.thought = new Idea(thoughtAcceptingRate.getThought());
        this.user = thoughtAcceptingRate.getUser();
        this.rate = thoughtAcceptingRate.getRate();
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Idea getThought() {
        return thought;
    }

    public void setThought(Thought thought) {
        this.thought = new Idea(thought);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public void setRate(int rate) {
        this.rate = rate;
    }
}
