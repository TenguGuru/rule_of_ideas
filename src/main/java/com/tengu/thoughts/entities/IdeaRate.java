package com.tengu.thoughts.entities;

import com.tengu.users.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class IdeaRate implements ThoughtRate {
    @Id
    @GeneratedValue
    private UUID id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idea_id", nullable = false)
    Idea thought;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", columnDefinition="uuid not null")
    User user;
    int rate;//2-fun, 1-boring, 0-notfun.
    int userRate;
    public IdeaRate(ThoughtRate thoughtRate){
        this.id = thoughtRate.getId();
        this.thought = new Idea(thoughtRate.getThought());
        this.user = thoughtRate.getUser();
        this.rate = thoughtRate.getRate();
        this.userRate = thoughtRate.getUserRate();
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Idea getThought() {
        return thought;
    }
    @Override
    public void setThought(Thought thought) {
        this.thought = new Idea(thought);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public int getUserRate() {
        return userRate;
    }

    @Override
    public void setUserRate(int userRate) {
        this.userRate = userRate;
    }
}
