package com.tengu.thoughts.entities;


import com.tengu.parties.entities.Party;

import java.util.UUID;

public interface ThoughtBid {
    UUID getId();
    void setId(UUID id);
    Thought getThought();
    void setThought(Thought thought);
    Party getParty();
    void setParty(Party party);
    int getLoyaltyPercent();
    void setLoyaltyPercent(int loyaltyPercent);
    int getAttempt();
    void setAttempt(int attempt);
}
