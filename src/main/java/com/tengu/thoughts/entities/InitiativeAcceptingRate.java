package com.tengu.thoughts.entities;

import com.tengu.thoughts.entities.Initiative;
import com.tengu.thoughts.entities.Thought;
import com.tengu.thoughts.entities.ThoughtAcceptingRate;
import com.tengu.users.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class InitiativeAcceptingRate implements ThoughtAcceptingRate {
    @Id
    @GeneratedValue
    private UUID id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "initiative_id", columnDefinition="uuid not null")
    private Initiative thought;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", columnDefinition="uuid not null")
    private User user;
    private int rate;//-1/1
    public InitiativeAcceptingRate(ThoughtAcceptingRate thoughtAcceptingRate){
        this.id = thoughtAcceptingRate.getId();
        this.thought = new Initiative(thoughtAcceptingRate.getThought());
        this.user = thoughtAcceptingRate.getUser();
        this.rate = thoughtAcceptingRate.getRate();
    }

    /**
     * For tests only
     * */
    public InitiativeAcceptingRate(UUID id, Initiative initiative, User user, int rate){
        this.id = id;
        this.thought = initiative;
        this.user = user;
        this.rate = rate;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Initiative getThought() {
        return thought;
    }
    @Override
    public void setThought(Thought thought) {
        this.thought = new Initiative(thought);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public void setRate(int rate) {
        this.rate = rate;
    }
}
