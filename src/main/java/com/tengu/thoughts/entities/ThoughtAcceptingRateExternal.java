package com.tengu.thoughts.entities;

import com.tengu.users.User;
import java.util.UUID;

public class ThoughtAcceptingRateExternal implements ThoughtAcceptingRate {//Нужен для получения данных
    private UUID id;
    private ThoughtExternal thought;
    private User user;
    private int rate;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Thought getThought() {
        return thought;
    }
    @Override
    public void setThought(Thought thought) {
        this.thought = new ThoughtExternal(thought);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public void setRate(int rate) {
        this.rate = rate;
    }
}
