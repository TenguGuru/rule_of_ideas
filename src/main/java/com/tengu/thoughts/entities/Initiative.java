package com.tengu.thoughts.entities;

import com.tengu.boards.Board;
import com.tengu.users.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"title", "board_id"})
})
public class Initiative implements Thought {
    @Id
    @GeneratedValue
    private UUID id;
    Long number;
    int status;//0 - превалидация.1 - на допуске к оценке.2 - на оценке.3 - снятые с оценки
    String title;
    String text;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id", columnDefinition="uuid not null")
    private Board board;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id", columnDefinition="uuid not null")
    private User author;

    public Initiative(Thought thought){
        this.id = thought.getId();
        this.number = thought.getNumber();
        this.status = thought.getStatus();
        this.title = thought.getTitle();
        this.text = thought.getText();
        this.board = thought.getBoard();
        this.author = thought.getAuthor();
    }

    /**
     * For tests only
    * */
    public Initiative(UUID id, Long number, int status, String title, String text, Board board, User author){
        this.id = id;
        this.number = number;
        this.status = status;
        this.title = title;
        this.text = text;
        this.board = board;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Initiative [" + title + ":" + text + "]";
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Long getNumber() {
        return number;
    }

    @Override
    public void setNumber(Long number) {
        this.number = number;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public void setBoard(Board board) {
        this.board = board;
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(User author) {
        this.author = author;
    }
}
