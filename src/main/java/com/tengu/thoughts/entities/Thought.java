package com.tengu.thoughts.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tengu.boards.Board;
import com.tengu.users.User;

import javax.persistence.*;
import java.util.UUID;
@JsonDeserialize(as=ThoughtExternal.class)
public interface Thought {

    UUID getId();
    void setId(UUID id);
    Long getNumber();
    void setNumber(Long number);
    String getTitle();
    void setTitle(String title);
    String getText();
    void setText(String text);
    Board getBoard();
    void setBoard(Board board);
    User getAuthor();
    void setAuthor(User author);
    int getStatus();
    void setStatus(int status);
}
