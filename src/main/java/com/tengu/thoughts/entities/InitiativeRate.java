package com.tengu.thoughts.entities;

import com.tengu.thoughts.entities.Initiative;
import com.tengu.thoughts.entities.Thought;
import com.tengu.thoughts.entities.ThoughtRate;
import com.tengu.users.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class InitiativeRate implements ThoughtRate{
    @Id
    @GeneratedValue
    private UUID id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "initiative_id", nullable = false)
    Initiative thought;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", columnDefinition="uuid not null")
    User user;
    int rate;//2-fun, 1-boring, 0-notfun
    int userRate;
    public InitiativeRate(ThoughtRate thoughtRate){
        this.id = thoughtRate.getId();
        this.thought = new Initiative(thoughtRate.getThought());
        this.user = thoughtRate.getUser();
        this.rate = thoughtRate.getRate();
        this.userRate = thoughtRate.getUserRate();
    }
    /**
     * For tests only
     * */
    public InitiativeRate(UUID id, Initiative initiative, User user, int rate, int userRate ){
        this.id = id;
        this.thought = initiative;
        this.user = user;
        this.rate = rate;
        this.userRate = userRate;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Initiative getThought() {
        return thought;
    }

    @Override
    public void setThought(Thought thought) {
        this.thought = new Initiative(thought);
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public int getUserRate() {
        return userRate;
    }

    @Override
    public void setUserRate(int userRate) {
        this.userRate = userRate;
    }
}
