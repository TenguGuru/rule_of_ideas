package com.tengu.thoughts.entities;

import com.tengu.users.User;

import java.util.UUID;

public interface ThoughtAcceptingRate {
     UUID getId();
     void setId(UUID id);
     Thought getThought();
     void setThought(Thought thought);
     User getUser();
     void setUser(User user);
     int getRate();
     void setRate(int rate);
}
