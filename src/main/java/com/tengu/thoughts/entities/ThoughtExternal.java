package com.tengu.thoughts.entities;

import com.tengu.boards.Board;
import com.tengu.users.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
public class ThoughtExternal implements Thought{//Нужен для получения данных

    private UUID id;
    private Long number;
    private String title;
    private String text;
    private Board board;
    private User author;
    private int status;

    public ThoughtExternal(Thought thought){
        this.id = thought.getId();
        this.number = thought.getNumber();
        this.status = thought.getStatus();
        this.title = thought.getTitle();
        this.text = thought.getText();
        this.board = thought.getBoard();
        this.author = thought.getAuthor();
    }
    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Long getNumber() {
        return number;
    }

    @Override
    public void setNumber(Long number) {
        this.number = number;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public void setBoard(Board board) {
        this.board = board;
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }
}
