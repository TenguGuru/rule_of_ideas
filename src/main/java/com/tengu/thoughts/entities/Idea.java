package com.tengu.thoughts.entities;

import com.tengu.boards.Board;
import com.tengu.thoughts.entities.Thought;
import com.tengu.users.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"title", "board_id"})
})
public class Idea implements Thought {
    @Id
    @GeneratedValue
    private UUID id;
    Long number;
    String title;
    String text;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id", columnDefinition="uuid not null")
    private Board board;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id", columnDefinition="uuid not null")
    private User author;
    private int status;//0 - превалидация.1 - на допуске к оценке.2 - на оценке.3 - снятые с оценки

    public Idea(Thought thought){
        this.id = thought.getId();
        this.number = thought.getNumber();
        this.status = thought.getStatus();
        this.title = thought.getTitle();
        this.text = thought.getText();
        this.board = thought.getBoard();
        this.author = thought.getAuthor();
    }
    public String toString() {
        return "Idea [" + title + ":" + text + "]";
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Long getNumber() {
        return number;
    }

    @Override
    public void setNumber(Long number) {
        this.number = number;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public void setBoard(Board board) {
        this.board = board;
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }
}
