package com.tengu.thoughts.dto;

import com.tengu.parties.entities.Party;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MaxBidInfoDTO {
    Party party;
    double loyalty;
}
