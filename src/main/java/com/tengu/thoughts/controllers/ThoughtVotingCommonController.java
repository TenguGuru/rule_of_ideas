package com.tengu.thoughts.controllers;

import com.tengu.parties.entities.Party;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

public interface ThoughtVotingCommonController   {

    @GetMapping("/get/concludeVoting/{thoughtId}")
    Party concludeVoting(@PathVariable UUID thoughtId);
}
