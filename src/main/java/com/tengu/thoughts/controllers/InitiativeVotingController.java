package com.tengu.thoughts.controllers;

import com.tengu.thoughts.services.InitiativeVotingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/thoughtVoting/initiativeVoting")
public class InitiativeVotingController extends ThoughtVotingAbstractController<InitiativeVotingService> {
    public InitiativeVotingController(InitiativeVotingService service) {
        super(service);
    }
}
