package com.tengu.thoughts.controllers;

import com.tengu.thoughts.services.IdeaVotingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/thoughtVoting/ideaVoting")
public class IdeaVotingController extends ThoughtVotingAbstractController<IdeaVotingService> {
    public IdeaVotingController(IdeaVotingService service) {
        super(service);
    }
}
