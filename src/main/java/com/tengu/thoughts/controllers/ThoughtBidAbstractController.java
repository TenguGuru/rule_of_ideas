package com.tengu.thoughts.controllers;

import com.tengu.thoughts.entities.ThoughtBid;
import com.tengu.thoughts.dto.MaxBidInfoDTO;
import com.tengu.thoughts.services.ThoughtBidCommonService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.UUID;

public abstract class ThoughtBidAbstractController<E extends ThoughtBid, S extends ThoughtBidCommonService<E>> implements ThoughtBidCommonController<E> {

    private final S service;

    @Autowired
    protected ThoughtBidAbstractController(S service) {
        this.service = service;
    }

    @Override
    public Integer getLoyaltyPercent(UUID partyId, UUID thoughtId){
        return this.service.getLoyaltyPercent(partyId, thoughtId);
    }

    @Override
    public void updateLoyaltyPercent(UUID partyId, UUID thoughtId, Integer loyaltyPercent){
        this.service.updateLoyaltyPercent(partyId, thoughtId, loyaltyPercent);
    }

    @Override
    public Integer getAttempts(UUID partyId, UUID thoughtId){
        return this.service.getAttempts(partyId, thoughtId);
    }

    @Override
    public E save(E thoughtBid) {
        return this.service.save(thoughtBid);
    }


    @Override
    public E getByPartyAndThought(UUID partyId, UUID thoughtId){
        return this.service.getByPartyAndThought(partyId, thoughtId);
    }

    @Override
    public Map<UUID, MaxBidInfoDTO> getAllThoughtBidsForMap(UUID boardId){
        return this.service.getAllThoughtBidsForMap(boardId);
    }
}
