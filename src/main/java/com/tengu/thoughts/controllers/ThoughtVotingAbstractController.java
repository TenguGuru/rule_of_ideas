package com.tengu.thoughts.controllers;

import com.tengu.parties.entities.Party;
import com.tengu.thoughts.services.VotingResultAbstractService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public abstract class ThoughtVotingAbstractController<S extends VotingResultAbstractService> implements ThoughtVotingCommonController {

    private final S service;

    @Autowired
    public ThoughtVotingAbstractController(S service) { this.service = service; }

    @Override
    public Party concludeVoting(UUID thoughtId) {
        return this.service.concludeVoting(thoughtId);
    }

}
