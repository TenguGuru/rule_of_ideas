package com.tengu.thoughts.controllers;

import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardService;
import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyIdeas;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.services.*;
import com.tengu.thoughts.services.IdeaService;
import com.tengu.thoughts.services.InitiativeIdeasService;
import com.tengu.thoughts.services.ThoughtService;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("api/thoughts/ideas")
public class IdeaController implements ThoughtController {

    @Autowired
    private InitiativeIdeasService initiativeIdeasService;
    private ThoughtService ideaService;
    private IdeaVotingService ideaVotingService;
    private UserRepository userRepository;
    private BoardService boardService;

    public IdeaController(IdeaService ideaService, UserRepository userRepository,
                          BoardService boardService){
        this.ideaService = ideaService;
        this.userRepository = userRepository;
        this.boardService = boardService;
    }
    @GetMapping(value = {""})
    public Iterable<? extends Thought> findAll() {
        return ideaService.findAll();
    }


    @GetMapping("/board/{boardId}")
    public Iterable<? extends Thought> boardIdeas(@PathVariable UUID boardId) {
        return ideaService.boardThoughts(boardId);
    }


    @GetMapping(value = {"/{id}"})
    public Optional<? extends Thought> findById(@PathVariable UUID id) {
        return ideaService.findById(id);
    }
    @GetMapping("/getByUser/{boardId}/{userId}")
    public Iterable<? extends Thought> findByAuthor(@PathVariable(value = "userId") UUID userId,
                                                    @PathVariable(value = "boardId") UUID boardId) {
        return ideaService.findByAuthor(userId,boardId);
    }
    @PostMapping("/save")
    public Thought save(@RequestBody ThoughtExternal thought, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = thought.getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return ideaService.save(thought);
    }
    @PostMapping("/update")
    public void update(@RequestBody ThoughtExternal thought) {
        ideaService.update(thought);
    }

    @GetMapping("/getByStatus/{boardId}/{status}")
    public Iterable<? extends Thought> getByStatus(@PathVariable(value = "status") int status,
                                                   @PathVariable(value = "boardId") UUID boardId) {
        return ideaService.getByStatus(status,boardId);
    }

    @PostMapping("/save/rate")
    public ThoughtRate saveRate(@RequestBody ThoughtRateExternal thoughtRate, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = thoughtRate.getThought().getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return ideaService.saveRated(thoughtRate);
    }
    @PostMapping("/update/rate")
    public void updateRate(@RequestBody ThoughtRateExternal thoughtRate) {
        ideaService.updateRate(thoughtRate);
    }

    @GetMapping("/get/rate")
    public Iterable<? extends ThoughtRate> getAllRates() {
        return ideaService.findAllRated();
    }

    @GetMapping("/get/rate/{boardId}/{userId}")
    public Iterable<? extends ThoughtRate> getRateByUser(@PathVariable(value = "userId") UUID userId,
                                                         @PathVariable(value = "boardId") UUID boardId) {
        return ideaService.getRatedByUser(userId,boardId);
    }
    @GetMapping("/getThoughtRateInfo/{thoughtId}")
    public InfoProjection getThoughtRateInfo(@PathVariable UUID thoughtId) {
        return ideaService.getThoughtRateInfo(thoughtId);
    }
    @GetMapping("/getMatrixMap/{thoughtId}")
    public List<List<String>> getMatrixMap(@PathVariable UUID thoughtId) {
        return ideaService.getMatrixMap(thoughtId);
    }
    @GetMapping("/search/{boardId}/{req}")
    public Iterable<MapListSortInfoProjection> searchByContaining(@PathVariable(value = "req") String req,
                                                                  @PathVariable(value = "boardId") UUID boardId) {
        long number = 0;
        try {
            number = Integer.parseInt(req);
            return ideaService.getByNumberForMap(number,boardId);
        } catch (NumberFormatException ex) {
            return ideaService.findByNameContainingForMap(req,boardId);
        }

    }

    @GetMapping("/forValidatedMap/{boardId}")
    public Iterable<MapListSortInfoProjection> getAllValidatedForMap(@PathVariable UUID boardId) {
        return ideaService.getAllValidatedForMap(boardId);
    }

    @PostMapping("/save/acceptingRate")
    public ThoughtAcceptingRate saveAcceptingRate(@RequestBody ThoughtAcceptingRateExternal thoughtAcceptingRate, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = thoughtAcceptingRate.getThought().getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return ideaService.saveAcceptingRate(thoughtAcceptingRate);
    }
    @GetMapping("/get/acceptingRate/{thoughtId}")
    public AcceptRateProjection getAcceptingRateInfo(@PathVariable UUID thoughtId){
        return ideaService.getAcceptingInfo(thoughtId);
    }
    @GetMapping("/get/acceptingRateByUser/{userId}/{boardId}")
    public Map<UUID, Integer> getAcceptingInfoByUser(@PathVariable UUID userId, @PathVariable UUID boardId) {
        return ideaService.getAcceptingInfoByUser(userId, boardId);
    }
    @GetMapping("/acceptThought/{thoughtId}")
    public ResponseEntity<?> acceptThought(@PathVariable UUID thoughtId){
        ideaService.acceptThought(thoughtId);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("Accepted to rate!"), HttpStatus.OK);
    }
    @GetMapping("/get/AvgAcceptRate/{boardId}")
    public Iterable<AverageAcceptRateProjection> getAvgAcceptRate(@PathVariable UUID boardId) {
        return ideaService.getAvgAcceptRate(boardId);
    }
    @GetMapping("/getThoughtPartyRateInfo/{thoughtId}/{partyId}")
    public InfoProjection getThoughtPartyRateInfo(@PathVariable UUID thoughtId, @PathVariable UUID partyId){
        return ideaService.getThoughtPartyRateInfo(thoughtId, partyId);
    }
    @GetMapping("/get/acceptingPartyRate/{thoughtId}/{partyId}")
    public AcceptRateProjection getAcceptingPartyRateInfo(UUID thoughtId, UUID partyId) {
        return ideaService.getAcceptingPartyInfo(thoughtId, partyId);
    }
    @GetMapping("/getPartyMaxBidInfo/{boardId}/{ideaId}/{partyId}")
    public Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(@PathVariable("boardId") UUID boardId,
                                                                  @PathVariable("ideaId") UUID ideaId,
                                                                  @PathVariable("partyId") UUID partyId){
        return ideaService.getPartyMaxBidInfo(boardId, ideaId, partyId);
    }

    @Override
    @PostMapping("/delete")
    public void deleteThought(@RequestBody Thought thought) {
        ideaService.deleteThought(thought);
    }
}
