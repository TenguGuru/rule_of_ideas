package com.tengu.thoughts.controllers;

import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardService;
import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.services.InitiativeIdeasService;
import com.tengu.thoughts.services.InitiativeService;
import com.tengu.thoughts.services.ThoughtService;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("api/thoughts/initiatives")
public class InitiativeController implements ThoughtController {

    @Autowired
    private InitiativeIdeasService initiativeIdeasService;
    private UserRepository userRepository;
    private ThoughtService initiativeService;
    private BoardService boardService;

    public InitiativeController(InitiativeService initiativeService,
                                UserRepository userRepository,
                                BoardService boardService){
        this.initiativeService = initiativeService;
        this.userRepository = userRepository;
        this.boardService = boardService;
    }
    @GetMapping(value = {""})
    public Iterable<? extends Thought> findAll() {
        return initiativeService.findAll();
    }

    @GetMapping("/board/{boardId}")
    public Iterable<? extends Thought> boardIdeas(@PathVariable UUID boardId) {
        return initiativeService.boardThoughts(boardId);
    }

    @GetMapping(value = {"/{id}"})
    public Optional<? extends Thought> findById(@PathVariable UUID id) {
        return initiativeService.findById(id);
    }
    @GetMapping("/getByUser/{boardId}/{userId}")
    public Iterable<? extends Thought> findByAuthor(@PathVariable(value = "userId") UUID userId,
                                                    @PathVariable(value = "boardId") UUID boardId) {
        return initiativeService.findByAuthor(userId,boardId);
    }
    @PostMapping("/save")
    public Thought save(@RequestBody ThoughtExternal thought, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = thought.getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return initiativeService.save(thought);
    }
    @PostMapping("/update")
    public void update(@RequestBody ThoughtExternal thought) {
        initiativeService.update(thought);
    }

    @GetMapping("/getByStatus/{boardId}/{status}")
    public Iterable<? extends Thought> getByStatus(@PathVariable(value = "status") int status,
                                                   @PathVariable(value = "boardId") UUID boardId) {
        return initiativeService.getByStatus(status,boardId);
    }

    @PostMapping("/save/rate")
    public ThoughtRate saveRate(@RequestBody ThoughtRateExternal thoughtRate, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = thoughtRate.getThought().getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return initiativeService.saveRated(thoughtRate);
    }
    @PostMapping("/update/rate")
    public void updateRate(@RequestBody ThoughtRateExternal thoughtRate) {
        initiativeService.updateRate(thoughtRate);
    }

    @GetMapping("/get/rate")
    public Iterable<? extends ThoughtRate> getAllRates() {
        return initiativeService.findAllRated();
    }

    @GetMapping("/get/rate/{boardId}/{userId}")
    public Iterable<? extends ThoughtRate> getRateByUser(@PathVariable(value = "userId") UUID userId,
                                                         @PathVariable(value = "boardId") UUID boardId) {
        return initiativeService.getRatedByUser(userId,boardId);
    }
    @GetMapping("/getThoughtRateInfo/{thoughtId}")
    public InfoProjection getThoughtRateInfo(@PathVariable UUID thoughtId) {
        return initiativeService.getThoughtRateInfo(thoughtId);
    }
    @GetMapping("/getMatrixMap/{thoughtId}")
    public List<List<String>> getMatrixMap(@PathVariable UUID thoughtId) {
        return initiativeService.getMatrixMap(thoughtId);
    }
    @GetMapping("/search/{boardId}/{req}")
    public Iterable<MapListSortInfoProjection> searchByContaining(@PathVariable(value = "req") String req,
                                                                  @PathVariable(value = "boardId") UUID boardId) {
        long number = 0;
        try {
            number = Integer.parseInt(req);
            return initiativeService.getByNumberForMap(number,boardId);
        } catch (NumberFormatException ex) {
            return initiativeService.findByNameContainingForMap(req,boardId);
        }

    }

    @GetMapping("/forValidatedMap/{boardId}")
    public Iterable<MapListSortInfoProjection> getAllValidatedForMap(@PathVariable UUID boardId) {
        return initiativeService.getAllValidatedForMap(boardId);
    }

    @PostMapping("/save/acceptingRate")
    public ThoughtAcceptingRate saveAcceptingRate(@RequestBody ThoughtAcceptingRateExternal thoughtAcceptingRate, Principal principal){
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            UUID board_id = thoughtAcceptingRate.getThought().getBoard().getId();
            BoardMember.MemberRole memberRole = boardService.findMember(user.getId(), board_id).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return initiativeService.saveAcceptingRate(thoughtAcceptingRate);
    }
    @GetMapping("/get/acceptingRate/{thoughtId}")
    public AcceptRateProjection getAcceptingRateInfo(@PathVariable UUID thoughtId) {
        return initiativeService.getAcceptingInfo(thoughtId);
    }
    @GetMapping("/get/acceptingRateByUser/{userId}/{boardId}")
    public Map<UUID, Integer> getAcceptingInfoByUser(@PathVariable UUID userId, @PathVariable UUID boardId) {
        return initiativeService.getAcceptingInfoByUser(userId, boardId);
    }
    @GetMapping("/acceptThought/{thoughtId}")
    public ResponseEntity<?> acceptThought(@PathVariable UUID thoughtId) {
        initiativeService.acceptThought(thoughtId);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("Accepted to rate!"), HttpStatus.OK);
    }
    @GetMapping("/get/AvgAcceptRate/{boardId}")
    public Iterable<AverageAcceptRateProjection> getAvgAcceptRate(@PathVariable UUID boardId) {
        return initiativeService.getAvgAcceptRate(boardId);
    }
    @GetMapping("/getThoughtPartyRateInfo/{thoughtId}/{partyId}")
    public InfoProjection getThoughtPartyRateInfo(@PathVariable UUID thoughtId, @PathVariable UUID partyId){
        return initiativeService.getThoughtPartyRateInfo(thoughtId, partyId);
    }
    @GetMapping("/get/acceptingPartyRate/{thoughtId}/{partyId}")
    public AcceptRateProjection getAcceptingPartyRateInfo(UUID thoughtId, UUID partyId) {
        return initiativeService.getAcceptingPartyInfo(thoughtId, partyId);
    }

    @GetMapping("/getPartyMaxBidInfo/{boardId}/{initiativeId}/{partyId}")
    public Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(@PathVariable("boardId") UUID boardId,
                                                                  @PathVariable("initiativeId") UUID initiativeId,
                                                                  @PathVariable("partyId") UUID partyId){
        return initiativeService.getPartyMaxBidInfo(boardId, initiativeId, partyId);
    }

    @Override
    @PostMapping("/delete")
    public void deleteThought(@RequestBody Thought thought) {
        initiativeService.deleteThought(thought);
    }
}
