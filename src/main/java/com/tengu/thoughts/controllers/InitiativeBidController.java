package com.tengu.thoughts.controllers;

import com.tengu.thoughts.entities.InitiativeBid;
import com.tengu.thoughts.services.InitiativeBidService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/thoughtBids/initiativeBids")
public class InitiativeBidController extends ThoughtBidAbstractController<InitiativeBid, InitiativeBidService> {

    protected InitiativeBidController(InitiativeBidService service) {
        super(service);
    }
}
