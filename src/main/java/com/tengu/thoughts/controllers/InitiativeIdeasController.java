package com.tengu.thoughts.controllers;

import com.tengu.thoughts.entities.InitiativeIdeas;
import com.tengu.thoughts.projections.InitiativeIdeasInfoProjection;
import com.tengu.thoughts.services.InitiativeIdeasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("api/initiativeIdeas")
public class InitiativeIdeasController {
    @Autowired
    private InitiativeIdeasService service;

    @GetMapping("/getInitiativeIdeasPartyList/{initiativeId}/{partyId}")
    public Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasPartyList(@PathVariable UUID initiativeId, @PathVariable UUID partyId) {
        return this.service.getInitiativeIdeasPartyList(initiativeId, partyId);
    }
    @GetMapping("/getIdeaInitiativesPartyList/{ideaId}/{partyId}")
    public Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesPartyInfo(@PathVariable UUID ideaId, @PathVariable UUID partyId){
        return this.service.getIdeaInitiativesPartyList(ideaId, partyId);
    }
    @GetMapping("/getIdeaInitiativesList/{ideaId}")
    public Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesList(@PathVariable UUID ideaId){
        return this.service.getIdeaInitiativesList(ideaId);
    }
    @GetMapping("/getInitiativeIdeasList/{initiativeId}")
    public Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasList(@PathVariable UUID initiativeId){
        return this.service.getInitiativeIdeasList(initiativeId);
    }
    @GetMapping(value = {"get/{userId}/{initiativeId}"})
    public Iterable<InitiativeIdeas> getInitiativeIdeasByUserAndInitiative(@PathVariable UUID userId, @PathVariable UUID initiativeId){
        return this.service.findByUserAndInitiative(userId,initiativeId);
    }
    @PostMapping("/save")
    public InitiativeIdeas saveInitiativeIdeas(@RequestBody InitiativeIdeas initiativeIdeas) throws IOException {
       return this.service.saveInitiativeIdeas(initiativeIdeas);
    }
    @DeleteMapping("/delete/{initiativeIdeaId}")
    public ResponseEntity<?> deleteInitiativeIdea(@PathVariable UUID initiativeIdeaId){
        this.service.deleteInitiativeIdea(initiativeIdeaId);
        return new ResponseEntity<>(new com.tengu.controllers.message.response.ResponseMessage("Sucesfully Deleted!"), HttpStatus.OK);
    }

}
