package com.tengu.thoughts.controllers;

import com.tengu.thoughts.entities.IdeaBid;
import com.tengu.thoughts.services.IdeaBidService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/thoughtBids/ideaBids")
public class IdeaBidController extends ThoughtBidAbstractController<IdeaBid, IdeaBidService> {

    protected IdeaBidController(IdeaBidService service) {
        super(service);
    }
}
