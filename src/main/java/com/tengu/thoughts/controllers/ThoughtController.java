package com.tengu.thoughts.controllers;

import com.tengu.boards.BoardService;
import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.services.IdeaService;
import com.tengu.thoughts.services.InitiativeIdeasService;
import com.tengu.thoughts.services.InitiativeService;
import com.tengu.thoughts.services.ThoughtService;
import com.tengu.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;


interface ThoughtController{
     Iterable<? extends Thought> findAll();
     Iterable<? extends Thought> boardIdeas(UUID boardId);
     Optional<? extends Thought> findById(UUID id);
     Iterable<? extends Thought> findByAuthor(UUID userId, UUID boardId);
     Thought save(ThoughtExternal thought, Principal principal);
     void update(ThoughtExternal thought);
     Iterable<? extends Thought> getByStatus(int status, UUID boardId);
     ThoughtRate saveRate(ThoughtRateExternal thoughtRate, Principal principal);
     void updateRate( ThoughtRateExternal thoughtRate);
     Iterable<? extends ThoughtRate> getAllRates();
     Iterable<? extends ThoughtRate> getRateByUser(UUID userId,UUID boardId);
     InfoProjection getThoughtRateInfo( UUID thoughtId);
     List<List<String>> getMatrixMap(UUID thoughtId);
     Iterable<MapListSortInfoProjection> searchByContaining(String req,UUID boardId);
     Iterable<MapListSortInfoProjection> getAllValidatedForMap(UUID boardId);
     ThoughtAcceptingRate saveAcceptingRate(ThoughtAcceptingRateExternal thoughtAcceptingRate, Principal principal);
     AcceptRateProjection getAcceptingRateInfo(UUID thoughtId);
     Map<UUID, Integer> getAcceptingInfoByUser(UUID userId, UUID boardId);
     ResponseEntity<?> acceptThought(UUID thoughtId);
     Iterable<AverageAcceptRateProjection> getAvgAcceptRate(UUID boardId);
     InfoProjection getThoughtPartyRateInfo(UUID thoughtId, UUID partyId);
     AcceptRateProjection getAcceptingPartyRateInfo(UUID thoughtId, UUID partyId);
     Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(UUID boardId, UUID thoughtId, UUID partyId);
     void deleteThought(Thought thought);
}
