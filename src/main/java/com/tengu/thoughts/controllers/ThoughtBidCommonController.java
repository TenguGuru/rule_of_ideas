package com.tengu.thoughts.controllers;

import com.tengu.thoughts.entities.ThoughtBid;
import com.tengu.thoughts.dto.MaxBidInfoDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;
import java.util.UUID;

public interface ThoughtBidCommonController<E extends ThoughtBid> {

    @GetMapping("/get/loyaltyPercent/{partyId}/{thoughtId}")
    Integer getLoyaltyPercent(@PathVariable UUID partyId, @PathVariable UUID thoughtId);

    @PostMapping("/update/loyaltyPercent/{partyId}/{thoughtId}")
    void updateLoyaltyPercent(@PathVariable UUID partyId, @PathVariable UUID thoughtId, @RequestBody Integer loyaltyPercent);

    @GetMapping("/get/attempts/{partyId}/{thoughtId}")
    Integer getAttempts(@PathVariable UUID partyId, @PathVariable UUID thoughtId);

    @PostMapping("/save")
    E save(@RequestBody E thoughtBid);

    @GetMapping("/get/thoughtBid/byPartyAndThought/{partyId}/{thoughtId}")
    E getByPartyAndThought(@PathVariable UUID partyId, @PathVariable UUID thoughtId);

    @GetMapping("/get/thoughtBid/allThoughtBidsForMap/{boardId}")
    Map<UUID, MaxBidInfoDTO> getAllThoughtBidsForMap(@PathVariable UUID boardId);
}
