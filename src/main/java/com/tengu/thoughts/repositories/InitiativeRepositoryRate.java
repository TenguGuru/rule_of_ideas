package com.tengu.thoughts.repositories;

import com.tengu.commonProjections.InfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.thoughts.entities.InitiativeRate;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
public interface InitiativeRepositoryRate extends PagingAndSortingRepository<InitiativeRate, UUID> {

    @Query("SELECT r FROM InitiativeRate r where r.user.id = :userId and r.thought.board.id = :boardId")
    Iterable<InitiativeRate> getRatedByUser(@Param("userId") UUID userId,@Param("boardId") UUID boardId);

    @Query("SELECT i.userRate from InitiativeRate i where i.thought.id = :initiativeId")
    List<Integer> getRatesByInitiative(@Param("initiativeId") UUID initiativeId);
    @Query("SELECT i.userRate from InitiativeRate i where i.thought.id = :initiativeId and i.thought.status!=1")
    List<Integer> getRatesForMap(@Param("initiativeId") UUID initiativeId);
    @Query("SELECT "+
            "avg(i.userRate) as rate,"+
            "(SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.rate = 2 ) as averagePlusRate," +
            "(SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.rate = 0 ) as averageNegRate " +
            "FROM InitiativeRate i "+
            "where i.thought.id = :initiativeId")
    InfoProjection getInitiativeRateInfo(@Param("initiativeId") UUID initiativeId);

    @Query("SELECT "+
            "avg(i.userRate) as rate,"+
            "(SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and ir.rate = 2 ) as averagePlusRate," +
            "(SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and ir.rate = 0 ) as averageNegRate " +
            "FROM InitiativeRate i "+
            "where i.thought.id = :initiativeId and i.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId)")
    InfoProjection getThoughtPartyRateInfo(@Param("initiativeId") UUID initiativeId, @Param("partyId") UUID partyId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = "UPDATE public.initiative_rate set rate = :rate, user_rate = :userRate " +
                    "where id = :id")//Попробовать нативку, авось прокатит
    void update(@Param("rate") Integer rate,@Param("userRate") Integer userRate,
                @Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("DELETE FROM InitiativeRate ir where ir.thought.id = :initiativeId")
    void deleteByInitiative(@Param("initiativeId") UUID initiativeId);

    @Query("SELECT p.title as title, " +
            "(SELECT count(pm) from PartyMembers pm where pm.party.id = p.id ) * (SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = p.id)) * 100 as maxBid " +
            "from Party p where p.board.id = :boardId and p.id != :partyId and ((SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = p.id))) > 0")
    Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(@Param("boardId") UUID boardId, @Param("initiativeId") UUID initiativeId, @Param("partyId") UUID partyId);
}
