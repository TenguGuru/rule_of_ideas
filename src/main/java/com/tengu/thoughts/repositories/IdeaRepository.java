package com.tengu.thoughts.repositories;

import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.entities.Initiative;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.UUID;

@org.springframework.stereotype.Repository
public interface IdeaRepository extends PagingAndSortingRepository<Idea, UUID> {

    @Query("SELECT i from Idea i where i.author.id = :userId and i.board.id = :boardId")
    Iterable<Idea> getByUser(@Param("userId") UUID userId,@Param("boardId") UUID boardId);

    @Query("SELECT i from Idea i where i.status = :status and i.board.id = :boardId")
    Iterable<Idea> getByStatus(@Param("status") int status,@Param("boardId") UUID boardId);

    @Query("SELECT i from Idea i where i.board.id = :boardId")
    Iterable<Idea> boardIdeas(@Param("boardId") UUID boardId);

    @Query("SELECT count(i.number) from Idea i where i.board.id = :boardId")
    Long getLastNumber(@Param("boardId") UUID boardId);

    @Query("SELECT " +
            "i.title as title," +
            "i.number as number," +
            "i.id as id," +
            "(SELECT (CASE WHEN AVG(r.userRate) IS NULL THEN 0 ELSE AVG(r.userRate) END) from IdeaRate r where r.thought.id = i.id) as rate," +
            "(SELECT (CASE WHEN SUM(r.userRate) IS NULL THEN 0 ELSE SUM(r.userRate) END) from IdeaRate r where r.thought.id = i.id) as absRate " +
            " From Idea i where i.board.id = :boardId and i.status = 2")
    Iterable<MapListSortInfoProjection> getAllValidatedForMap(@Param("boardId") UUID boardId);

    @Query("SELECT " +
            "i.title as title," +
            "i.number as number," +
            "i.id as id," +
            "(SELECT AVG(r.userRate) from IdeaRate r where r.thought.id = i.id) as rate " +
            " From Idea i where i.title LIKE CONCAT('%',:title,'%') " +
            "and i.board.id = :boardId")
    Iterable<MapListSortInfoProjection> findByNameContainigForMap(@Param("title") String title,@Param("boardId") UUID boardId);

    @Query("SELECT " +
            "i.title as title," +
            "i.number as number," +
            "i.id as id," +
            "(SELECT AVG(r.userRate) from IdeaRate r where r.thought.id = i.id) as rate " +
            "from Idea i where i.number = :number and i.board.id = :boardId")
    Iterable<MapListSortInfoProjection> getByNumberForMap(@Param("number") long number, @Param("boardId") UUID boardId);

    @Transactional
    @Modifying
    @Query("UPDATE Idea i set i.title = :title, i.text = :text, i.status = :status " +
            "where i.id = :id")
    void update(@Param("id") UUID id, @Param("title") String title, @Param("text") String text,
                      @Param("status") int status);
}

