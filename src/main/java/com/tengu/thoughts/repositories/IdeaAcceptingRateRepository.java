package com.tengu.thoughts.repositories;

import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.thoughts.entities.IdeaAcceptingRate;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface IdeaAcceptingRateRepository extends CrudRepository<IdeaAcceptingRate, UUID> {

    @Query("SELECT " +
            "(SELECT count(r) " +
            "FROM IdeaAcceptingRate r where r.thought.id =:ideaId and r.rate > 0) as plus, "  +
            "(SELECT count(r) " +
            "FROM IdeaAcceptingRate r where r.thought.id =:ideaId and r.rate < 0) as minus "+
            "FROM IdeaAcceptingRate r where r.thought.id = :ideaId")
    List<AcceptRateProjection> getAcceptingInfo(@Param("ideaId") UUID ideaId);

    @Query("SELECT DISTINCT " +
            "r.thought.id as id, " +
            "(SELECT avg(ir.rate) from IdeaAcceptingRate ir where ir.thought.id = r.thought.id) as rate" +
            " FROM IdeaAcceptingRate r " +
            "Where r.thought.board.id = :boardId")
    Iterable<AverageAcceptRateProjection> getAvgAcceptRate(@Param("boardId") UUID boardId);

    @Query("SELECT r FROM IdeaAcceptingRate r where r.thought.id = :ideaId")
    List<IdeaAcceptingRate> getByIdea(@Param("ideaId") UUID ideaId);

    @Transactional
    @Modifying
    @Query("UPDATE IdeaAcceptingRate r set r.rate = :rate where (r.thought.id = :ideaId and r.user.id = :userId)")
    void updateIdeaRate(@Param("rate") Integer rate, @Param("ideaId") UUID ideaId, @Param("userId") UUID userId);

    @Transactional
    @Modifying
    @Query("DELETE FROM IdeaAcceptingRate where id in " +
            "(SELECT r.id FROM IdeaAcceptingRate r where r.thought.id = :ideaId)")
    void deleteByIdea(@Param("ideaId") UUID ideaId);

    @Query("SELECT " +
            "(SELECT count(r) " +
            "FROM IdeaAcceptingRate r where r.thought.id =:ideaId and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate > 0) as plus, "  +
            "(SELECT count(r) " +
            "FROM IdeaAcceptingRate r where r.thought.id =:ideaId and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate < 0) as minus "+
            "FROM IdeaAcceptingRate r where r.thought.id = :ideaId")
    List<AcceptRateProjection> getAcceptingPartyInfo(@Param("ideaId") UUID ideaId, @Param("partyId") UUID partyId);

    @Query("SELECT r FROM IdeaAcceptingRate r where r.user.id = :userId and r.thought.board.id = :boardId")
    List<IdeaAcceptingRate> getAcceptingInfoByUser(@Param("userId") UUID userId, @Param("boardId") UUID boardId);
}
