package com.tengu.thoughts.repositories;

import com.tengu.parties.entities.Party;
import com.tengu.thoughts.entities.InitiativeBid;
import com.tengu.thoughts.entities.ThoughtBid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.Iterator;
import java.util.UUID;

@NoRepositoryBean
public interface ThoughtBidRepository<E extends ThoughtBid> extends CrudRepository<E, UUID> {

    Integer getLoyaltyPercent(UUID partyId, UUID thoughtId);

    void updateLoyaltyPercent(UUID partyId, UUID thoughtId, Integer loyaltyPercent);

    Iterable<Party> getPartiesByThought(UUID thoughtId);

    Integer getAttempts(UUID partyId, UUID thoughtId);

    E getByPartyAndThought(UUID partyId, UUID thoughtId);

    Iterable<E> getAllThoughtBidsForMap(UUID boardId);

    Iterable<E> getAllBidsForThought(UUID thoughtId);
}
