package com.tengu.thoughts.repositories;


import com.tengu.parties.entities.Party;
import com.tengu.thoughts.entities.IdeaBid;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.UUID;

@Repository
public interface IdeaBidRepository extends ThoughtBidRepository<IdeaBid> {

    @Query("SELECT i.loyaltyPercent FROM IdeaBid i where i.party.id = :partyId AND i.thought.id = :thoughtId")
    Integer getLoyaltyPercent(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Transactional
    @Modifying
    @Query("UPDATE IdeaBid i set i.loyaltyPercent = i.loyaltyPercent + :loyaltyPercent WHERE i.party.id =:partyId AND i.thought.id=:thoughtId")
    void updateLoyaltyPercent(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId, @Param("loyaltyPercent") Integer loyaltyPercent);

    @Query("SELECT i.attempt FROM IdeaBid i where i.party.id = :partyId AND i.thought.id = :thoughtId")
    Integer getAttempts(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Query("SELECT i FROM IdeaBid i where i.party.id = :partyId AND i.thought.id = :thoughtId")
    IdeaBid getByPartyAndThought(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Query("SELECT i.party FROM IdeaBid i where i.thought.id = :thoughtId")
    Iterable<Party> getPartiesByThought(@Param("thoughtId") UUID thoughtId);

    @Query("SELECT i  FROM IdeaBid i where i.thought.id = :thoughtId")
    Iterable<IdeaBid> getAllBidsForThought(@Param("thoughtId") UUID thoughtId);

    @Query("SELECT i FROM IdeaBid i " +
            "where i.party.board.id = :boardId")
    Iterable<IdeaBid> getAllThoughtBidsForMap(@Param("boardId") UUID boardId);
}
