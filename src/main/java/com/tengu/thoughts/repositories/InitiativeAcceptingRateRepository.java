package com.tengu.thoughts.repositories;

import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.thoughts.entities.IdeaAcceptingRate;
import com.tengu.thoughts.entities.InitiativeAcceptingRate;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface InitiativeAcceptingRateRepository extends CrudRepository<InitiativeAcceptingRate, UUID> {

    @Query("SELECT " +
            "(SELECT count(r) " +
            "FROM InitiativeAcceptingRate r where r.thought.id =:initiativeId and r.rate > 0) as plus, "  +
            "(SELECT count(r) " +
            "FROM InitiativeAcceptingRate r where r.thought.id =:initiativeId and r.rate < 0) as minus "+
            "FROM InitiativeAcceptingRate r where r.thought.id = :initiativeId")
    List<AcceptRateProjection> getAcceptingInfo(@Param("initiativeId") UUID initiativeId);

    @Query("SELECT r FROM InitiativeAcceptingRate r where r.thought.id = :initiativeId")
    List<InitiativeAcceptingRate> getByInitiative(@Param("initiativeId") UUID initiativeId);

    @Transactional
    @Modifying
    @Query("UPDATE InitiativeAcceptingRate r set r.rate = :rate where (r.thought.id = :initiativeId and r.user.id = :userId)")
    void updateInitiativeRate(@Param("rate") Integer rate, @Param("initiativeId") UUID initiativeId, @Param("userId") UUID userId);

    @Query("SELECT DISTINCT " +
            "r.thought.id as id, " +
            "(SELECT avg(ir.rate) from InitiativeAcceptingRate ir" +
            " where ir.thought.id = r.thought.id) as rate" +
            " FROM InitiativeAcceptingRate r " +
            "Where r.thought.board.id = :boardId")
    Iterable<AverageAcceptRateProjection> getAvgAcceptRate(@Param("boardId") UUID boardId);

    @Transactional
    @Modifying
    @Query("DELETE FROM InitiativeAcceptingRate where id in " +
            "(SELECT r.id FROM InitiativeAcceptingRate r where r.thought.id = :initiativeId)")
    void deleteByInitiative(@Param("initiativeId") UUID initiativeId);

    @Query("SELECT " +
            "(SELECT count(r) " +
            "FROM InitiativeAcceptingRate r where r.thought.id =:initiativeId and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate > 0) as plus, "  +
            "(SELECT count(r) " +
            "FROM InitiativeAcceptingRate r where r.thought.id =:initiativeId and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate < 0) as minus "+
            "FROM InitiativeAcceptingRate r where r.thought.id = :initiativeId")
    List<AcceptRateProjection> getAcceptingPartyInfo(@Param("initiativeId") UUID initiativeId, @Param("partyId") UUID partyId);

    @Query("SELECT r FROM InitiativeAcceptingRate r where r.user.id = :userId and r.thought.board.id = :boardId")
    List<InitiativeAcceptingRate> getAcceptingInfoByUser(@Param("userId") UUID userId, @Param("boardId") UUID boardId);
}
