package com.tengu.thoughts.repositories;

import com.tengu.commonProjections.InfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.thoughts.entities.IdeaRate;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
public interface IdeaRepositoryRate extends PagingAndSortingRepository<IdeaRate, UUID> {

    @Query("SELECT r FROM IdeaRate r where r.user.id = :userId and r.thought.board.id = :boardId")
    Iterable<IdeaRate> getRatedByUser(@Param("userId") UUID userId,@Param("boardId") UUID boardId);

    @Query("SELECT r.userRate from IdeaRate r where r.thought.id = :ideaId ")
    List<Integer> getRatesByIdea(@Param("ideaId") UUID ideaId);

    @Query("SELECT "+
            "avg(i.userRate) as rate,"+
            "(SELECT avg(ir.userRate) from IdeaRate ir " +
            "where ir.thought.id = :ideaId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and ir.rate = 2 ) as averagePlusRate," +
            "(SELECT avg(ir.userRate) from IdeaRate ir " +
            "where ir.thought.id = :ideaId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and ir.rate = 0 ) as averageNegRate " +
            "FROM IdeaRate i "+
            "where i.thought.id = :ideaId and i.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId)")
    InfoProjection getThoughtPartyRateInfo(@Param("ideaId") UUID ideaId, @Param("partyId") UUID partyId);

    @Query("SELECT "+
            "avg(i.userRate) as rate,"+
            "(SELECT avg(ir.userRate) from IdeaRate ir " +
            "where ir.thought.id = :ideaId and ir.rate = 2 ) as averagePlusRate," +
            "(SELECT avg(ir.userRate) from IdeaRate ir " +
            "where ir.thought.id = :ideaId and ir.rate = 0 ) as averageNegRate " +
            "FROM IdeaRate i "+
            "where i.thought.id = :ideaId")
    InfoProjection getIdeaRateInfo(@Param("ideaId") UUID ideaId);

    @Query("SELECT p.title as title, " +
            "(SELECT count(pm) from PartyMembers pm where pm.party.id = p.id ) * (SELECT avg(ir.userRate) from IdeaRate ir " +
            "where ir.thought.id = :ideaId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = p.id)) * 100 as maxBid " +
            "from Party p where p.board.id = :boardId and p.id != :partyId and ((SELECT avg(ir.userRate) from IdeaRate ir " +
            "where ir.thought.id = :ideaId and ir.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = p.id))) > 0")
    Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(@Param("boardId") UUID boardId, @Param("ideaId") UUID ideaId, @Param("partyId") UUID partyId);

    @Query("SELECT i.userRate from IdeaRate i where i.thought.id = :ideaId and i.thought.status!=1")
    List<Integer> getRatesForMap(@Param("ideaId") UUID ideaId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = "UPDATE public.idea_rate set rate = :rate, user_rate = :userRate " +
            "where id = :id")//Попробовать нативку, авось прокатит
    void update(@Param("rate") Integer rate,@Param("userRate") Integer userRate,
                @Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("DELETE FROM IdeaRate where id in " +
            "(SELECT r.id FROM IdeaRate r where r.thought.id = :ideaId)")
    void deleteByIdea(@Param("ideaId") UUID ideaId);

}
