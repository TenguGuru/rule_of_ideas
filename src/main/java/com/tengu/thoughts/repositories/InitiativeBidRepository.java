package com.tengu.thoughts.repositories;

import com.tengu.parties.entities.Party;
import com.tengu.thoughts.entities.IdeaBid;
import com.tengu.thoughts.entities.InitiativeBid;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
public interface InitiativeBidRepository extends ThoughtBidRepository<InitiativeBid> {

    @Query("SELECT i.loyaltyPercent FROM InitiativeBid i where i.party.id = :partyId AND i.thought.id = :thoughtId")
    Integer getLoyaltyPercent(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Transactional
    @Modifying
    @Query("UPDATE InitiativeBid i set i.loyaltyPercent = i.loyaltyPercent + :loyaltyPercent where i.party.id =:partyId AND i.thought.id=:thoughtId")
    void updateLoyaltyPercent(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId, @Param("loyaltyPercent") Integer loyaltyPercent);

    @Query("SELECT i.attempt FROM InitiativeBid i where i.party.id = :partyId AND i.thought.id = :thoughtId")
    Integer getAttempts(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Query("SELECT i FROM InitiativeBid i where i.party.id = :partyId AND i.thought.id = :thoughtId")
    InitiativeBid getByPartyAndThought(@Param("partyId") UUID partyId, @Param("thoughtId") UUID thoughtId);

    @Query("SELECT i.party FROM InitiativeBid i where i.thought.id = :thoughtId")
    Iterable<Party> getPartiesByThought(@Param("thoughtId") UUID thoughtId);


    @Query("SELECT i FROM InitiativeBid i " +
        "where i.party.board.id = :boardId")
    Iterable<InitiativeBid> getAllThoughtBidsForMap(@Param("boardId") UUID boardId);

    @Query("SELECT i  FROM InitiativeBid i where i.thought.id = :thoughtId")
    Iterable<InitiativeBid> getAllBidsForThought(@Param("thoughtId") UUID thoughtId);
}
