package com.tengu.thoughts.repositories;

import com.tengu.commonProjections.InfoProjection;
import com.tengu.thoughts.entities.Initiative;
import com.tengu.thoughts.entities.InitiativeIdeas;
import com.tengu.thoughts.projections.InitiativeIdeasInfoProjection;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface InitiativeIdeasRepository extends CrudRepository<InitiativeIdeas, UUID> {

    @Query("SELECT i from InitiativeIdeas i where i.author.id = :userId and i.initiative.board.id = :boardId")
    Optional<InitiativeIdeas> findByUser(@Param("userId") UUID userId,@Param("boardId") UUID boardId);

    @Query("SELECT i from InitiativeIdeas i where i.author.id = :userId and i.initiative.id = :initiativeId")
    Iterable<InitiativeIdeas> findByUserAndInitiative(@Param("userId") UUID userId,@Param("initiativeId") UUID initiativeId);

    @Query("SELECT i from InitiativeIdeas i where i.initiative.id = :initiativeId")
    Iterable<InitiativeIdeas> findByInitiative(@Param("initiativeId") UUID initiativeId);
    @Query("SELECT i from InitiativeIdeas i where i.idea.id = :ideaId")
    Iterable<InitiativeIdeas> findByIdea(@Param("ideaId") UUID ideaId);

    @Transactional
    @Modifying
    @Query("DELETE FROM InitiativeIdeas where id in " +
            "(SELECT r.id FROM InitiativeIdeas r where r.initiative.id = :initiativeId)")
    void deleteAllInitiativeIdeasByInitiative(@Param("initiativeId") UUID initiativeId);

    @Transactional
    @Modifying
    @Query("DELETE FROM InitiativeIdeas where id in " +
            "(SELECT r.id FROM InitiativeIdeas r where r.idea.id = :ideaId)")
    void deleteAllInitiativeIdeasByIdea(@Param("ideaId") UUID ideaId);

    @Query("SELECT "+
            "avg(i.userRate) as rate,"+
            "(SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.rate = 2 ) as averagePlusRate," +
            "(SELECT avg(ir.userRate) from InitiativeRate ir " +
            "where ir.thought.id = :initiativeId and ir.rate = 0 ) as averageNegRate " +
            "FROM InitiativeRate i "+
            "where i.thought.id = :initiativeId")
    InfoProjection getInitiativesRateInfo(@Param("initiativeId") UUID initiativeId);

    @Query("SELECT " +
            "i.idea as thought," +
            "i.relationship as relationship," +
            "(SELECT avg(r.userRate) from IdeaRate r " +
            "where r.thought.id = i.idea.id) as averageRate,  " +
            "(SELECT avg(r.userRate) from IdeaRate r " +
            "where r.thought.id = i.idea.id and r.rate = 2) as averagePlusRate,  " +
            "(SELECT avg(r.userRate) from IdeaRate r " +
            "where r.thought.id = i.idea.id and r.rate = 0) as averageNegRate  " +
            "from InitiativeIdeas i " +
            "where i.initiative.id = :initiativeId")
    Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasList(@Param("initiativeId") UUID initiativeId);

    @Query("SELECT " +
            "i.idea as thought," +
            "i.relationship as relationship," +
            "(SELECT avg(r.userRate) from IdeaRate r " +
            "where r.thought.id = i.idea.id and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId)) as averageRate,  " +
            "(SELECT avg(r.userRate) from IdeaRate r " +
            "where r.thought.id = i.idea.id and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate = 2) as averagePlusRate,  " +
            "(SELECT avg(r.userRate) from IdeaRate r " +
            "where r.thought.id = i.idea.id and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate = 0) as averageNegRate  " +
            "from InitiativeIdeas i " +
            "where i.initiative.id = :initiativeId")
    Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasPartyList(@Param("initiativeId") UUID initiativeId, @Param("partyId") UUID partyId);
    @Query("SELECT " +
            "i.initiative as thought," +
            "i.relationship as relationship," +
            "(SELECT avg(r.userRate) from InitiativeRate r " +
            "where r.thought.id = i.initiative.id and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId)) as averageRate,  " +
            "(SELECT avg(r.userRate) from InitiativeRate r " +
            "where r.thought.id = i.initiative.id and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate = 2) as averagePlusRate,  " +
            "(SELECT avg(r.userRate) from InitiativeRate r " +
            "where r.thought.id = i.initiative.id and r.user.id in (SELECT pm.member.id FROM PartyMembers pm WHERE pm.party.id = :partyId) and r.rate = 0) as averageNegRate  " +
            "from InitiativeIdeas i " +
            "where i.idea.id = :ideaId")
    Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesPartyList(@Param("ideaId") UUID ideaId, @Param("partyId") UUID partyId);
    @Query("SELECT " +
            "i.initiative as thought," +
            "i.relationship as relationship," +
            "(SELECT avg(r.userRate) from InitiativeRate r " +
            "where r.thought.id = i.initiative.id) as averageRate,  " +
            "(SELECT avg(r.userRate) from InitiativeRate r " +
            "where r.thought.id = i.initiative.id and r.rate = 2) as averagePlusRate,  " +
            "(SELECT avg(r.userRate) from InitiativeRate r " +
            "where r.thought.id = i.initiative.id and r.rate = 0) as averageNegRate  " +
            "from InitiativeIdeas i " +
            "where i.idea.id = :ideaId")
    Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesList(@Param("ideaId") UUID ideaId);
}
