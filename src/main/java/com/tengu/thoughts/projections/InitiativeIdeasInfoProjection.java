package com.tengu.thoughts.projections;


import com.tengu.thoughts.entities.Thought;

public interface InitiativeIdeasInfoProjection {//Проекция для отношения между идееями и инициативами
    Thought getThought();
    Double getAverageRate();
    Integer getRelationship();
    Double getAveragePlusRate();
    Double getAverageNegRate();
}
