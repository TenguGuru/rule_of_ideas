package com.tengu.thoughts.services;

import com.tengu.parties.entities.Party;
import com.tengu.thoughts.entities.ThoughtBid;
import com.tengu.thoughts.dto.MaxBidInfoDTO;

import java.util.Map;
import java.util.UUID;

public interface ThoughtBidCommonService<E extends ThoughtBid> {

    Integer getLoyaltyPercent(UUID partyId, UUID thoughtId);

    void updateLoyaltyPercent(UUID partyId, UUID thoughtId, Integer loyaltyPercent);

    Integer getAttempts(UUID partyId, UUID thoughtId);

    E save(E thoughtBid);

    E getByPartyAndThought(UUID partyId, UUID thoughtId);

    Iterable<Party> getPartiesByThought(UUID thoughtId);

    Map<UUID, MaxBidInfoDTO> getAllThoughtBidsForMap(UUID boardId);

    double calculateLoyalty(E i);
}
