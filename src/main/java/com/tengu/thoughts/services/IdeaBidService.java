package com.tengu.thoughts.services;

import com.tengu.parties.services.PartyService;
import com.tengu.thoughts.entities.IdeaBid;
import com.tengu.thoughts.repositories.IdeaBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdeaBidService extends ThoughtBidAbstractService<IdeaBid, IdeaBidRepository> {

    @Autowired
    private IdeaBidRepository ideaBidRepository;
    private PartyService partyService;
    private IdeaService ideaService;

    public IdeaBidService(IdeaBidRepository repository, PartyService partyService,
                          ThoughtService ideaService) {
        super(repository, partyService, ideaService);
    }


}
