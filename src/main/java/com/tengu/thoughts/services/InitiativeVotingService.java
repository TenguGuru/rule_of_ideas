package com.tengu.thoughts.services;

import com.tengu.parties.entities.PartyInitiatives;
import com.tengu.parties.repositories.PartyInitiativesRepository;
import com.tengu.thoughts.entities.Initiative;
import com.tengu.thoughts.entities.InitiativeBid;
import com.tengu.thoughts.repositories.InitiativeBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitiativeVotingService extends VotingResultAbstractService<InitiativeBid,
        InitiativeBidRepository, InitiativeBidService, PartyInitiatives,
        Initiative, PartyInitiativesRepository> {

    @Autowired
    private InitiativeBidRepository initiativeBidRepository;
    @Autowired
    private PartyInitiativesRepository partyThoughtRepository;

    public InitiativeVotingService(InitiativeBidRepository repository, InitiativeBidService thoughtBidService, PartyInitiativesRepository partyThoughtRepository) {
        super(repository, thoughtBidService, partyThoughtRepository);
    }

    @Override
    PartyInitiatives createPartyThought(InitiativeBid bid) {
        PartyInitiatives Initiatives = new PartyInitiatives();
        Initiatives.setThought(bid.getThought());
        Initiatives.setParty(bid.getParty());
        return Initiatives;
    }
}
