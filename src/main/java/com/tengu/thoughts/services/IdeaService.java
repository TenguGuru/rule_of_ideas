package com.tengu.thoughts.services;

import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardMemberRepository;
import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.parties.entities.Party;
import com.tengu.parties.repositories.PartyIdeasRepository;
import com.tengu.parties.repositories.PartyRepository;
import com.tengu.service.ColorConverter;

import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.entities.IdeaRate;
import com.tengu.thoughts.entities.Thought;
import com.tengu.thoughts.entities.ThoughtRate;
import com.tengu.thoughts.entities.ThoughtAcceptingRate;
import com.tengu.thoughts.entities.IdeaAcceptingRate;
import com.tengu.thoughts.repositories.IdeaAcceptingRateRepository;
import com.tengu.thoughts.repositories.IdeaBidRepository;
import com.tengu.thoughts.repositories.IdeaRepository;
import com.tengu.thoughts.repositories.IdeaRepositoryRate;
import com.tengu.thoughts.repositories.InitiativeIdeasRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class IdeaService implements ThoughtService {

    @Autowired
    private IdeaRepository repository;
    @Autowired
    private IdeaAcceptingRateRepository acceptingRateRepository;
    @Autowired
    private IdeaRepositoryRate rateRepository;
    @Autowired
    private IdeaBidRepository ideaBidRepository;
    @Autowired
    private BoardMemberRepository boardMemberRepository;
    @Autowired
    private PartyRepository partyRepository;
    @Autowired
    private PartyIdeasRepository partyIdeasRepository;
    @Autowired
    private InitiativeIdeasRepository initiativeIdeasRepository;


    @Override
    public Iterable<Idea> findAll() {
       return this.repository.findAll();
    }

    @Override
    public Iterable<IdeaRate> findAllRated() {
        return this.rateRepository.findAll();
    }

    @Override
    public Optional<Idea> findById(UUID id) {
        return this.repository.findById(id);
    }

    @Override
    public Iterable<Idea> getByStatus(int status, UUID boardId) {
        return this.repository.getByStatus(status, boardId);
    }

    @Override
    public Iterable<Idea> findByAuthor(UUID userId, UUID boardId) {
        return this.repository.getByUser(userId, boardId);
    }

    @Override
    public Iterable<Idea> boardThoughts(UUID boardId) {
        return this.repository.boardIdeas(boardId);
    }

    @Override
    public Long getThoughtLastNumber(UUID boardId) {
        return this.repository.getLastNumber(boardId);
    }

    @Override
    public void acceptThought(UUID thoughtId) {
        Idea idea = this.repository.findById(thoughtId).get();
        idea.setStatus(2);
        this.repository.save(idea);
        this.acceptingRateRepository.deleteByIdea(thoughtId);
    }

    @Override
    public Thought save(Thought thought) {
        Idea id = new Idea(thought);
        id.setStatus(1);
        id.setNumber(getThoughtLastNumber(id.getBoard().getId())+1);
        return this.repository.save(id);
    }

    @Override
    public void update(Thought thought) {
        this.repository.update(thought.getId(),thought.getTitle(), thought.getText(),thought.getStatus());
    }

    @Override
    public void deleteThought(Thought thought) {
        Iterable<Party> parties = this.ideaBidRepository.getPartiesByThought(thought.getId());
        for (Party party: parties){
            this.partyRepository.restoreIdeaLoyalty(party.getId(), this.ideaBidRepository.getLoyaltyPercent(party.getId(), thought.getId()));
            this.ideaBidRepository.delete(this.ideaBidRepository.getByPartyAndThought(party.getId(), thought.getId()));
        }
        Idea idea = new Idea(thought);
        this.initiativeIdeasRepository.deleteAllInitiativeIdeasByIdea(idea.getId());
        if (thought.getStatus() == 1) {
            System.out.println("accepting");
            this.acceptingRateRepository.deleteByIdea(thought.getId());
        }
        else if (thought.getStatus() == 2) {
            System.out.println("rate");
            this.rateRepository.deleteByIdea(thought.getId());
        }
        this.partyIdeasRepository.deleteByThought(idea.getId());
        this.repository.delete(idea);
    }

    @Override
    public void updateRate(ThoughtRate thoughtRate) {
        this.rateRepository.update(thoughtRate.getRate(),
                thoughtRate.getUserRate(),thoughtRate.getId());

    }

    @Override
    public IdeaRate saveRated(ThoughtRate thoughtRate) {
        return this.rateRepository.save(new IdeaRate(thoughtRate));
    }

    @Override
    public Iterable<IdeaRate> getRatedByUser(UUID userId, UUID boardId) {
        return this.rateRepository.getRatedByUser(userId, boardId);
    }

    @Override
    public InfoProjection getThoughtRateInfo(UUID thoughtId) {
        return this.rateRepository.getIdeaRateInfo(thoughtId);
    }

    @Override
    public List<List<String>> getMatrixMap(UUID thoughtId) {
        int usersCount = this.boardMemberRepository.getMembers(this.repository.findById(thoughtId).get().getBoard().getId()).size();
        int objCount = (int) Math.ceil(Math.sqrt(usersCount));
        List<Integer> rates = this.rateRepository.getRatesForMap(thoughtId);
        List<List<String>> result = new ArrayList<List<String>>();
        List<String> tempArr = new ArrayList<String>();
        List<String> convArr;
        for (int i = objCount; i > 0; i--) {
            for (int j = objCount; j > 0; j--) {
                if(usersCount!=0) {
                    if (rates.size() != 0) {
                        int ind = (int) (Math.random() * rates.size());
                        tempArr.add(ColorConverter.getColorOnRate(rates.get(ind)));
                        rates.remove(ind);
                    } else {
                        tempArr.add("#999999");
                    }
                    usersCount--;
                }else{
                    tempArr.add(null);
                }
            }
            convArr = new ArrayList<>(tempArr.subList(0, objCount));
            result.add(convArr);
            tempArr.clear();
        }
        return result;
    }

    @Override
    public Iterable<MapListSortInfoProjection> findByNameContainingForMap(String name, UUID boardId) {
        return this.repository.findByNameContainigForMap(name, boardId);
    }

    @Override
    public Iterable<MapListSortInfoProjection> getByNumberForMap(long number, UUID boardId) {
        return this.repository.getByNumberForMap(number, boardId);
    }

    @Override
    public Iterable<MapListSortInfoProjection> getAllValidatedForMap(UUID boardId) {
        return this.repository.getAllValidatedForMap(boardId);
    }

    @Override
    public AcceptRateProjection getAcceptingInfo(UUID thoughtId) {
        //TODO переделать по возможности, т.к это жуткий костыль
        return this.acceptingRateRepository.getAcceptingInfo(thoughtId).get(0);
    }

    @Override
    public Iterable<AverageAcceptRateProjection> getAvgAcceptRate(UUID boardId) {
        return this.acceptingRateRepository.getAvgAcceptRate(boardId);
    }

    @Override
    public Map<UUID, Integer> getAcceptingInfoByUser(UUID userId, UUID boardId) {
        List<IdeaAcceptingRate> userRates = this.acceptingRateRepository.getAcceptingInfoByUser(userId, boardId);
        Map<UUID, Integer> userRatesMap = new HashMap<UUID, Integer>();
        for (IdeaAcceptingRate idea : userRates) {
            userRatesMap.put(idea.getThought().getId(), idea.getRate());
        }
        return userRatesMap;
    }

    @Override
    public ThoughtAcceptingRate saveAcceptingRate(ThoughtAcceptingRate rate) {
        List<IdeaAcceptingRate> accRate = this.acceptingRateRepository.getByIdea(rate.getThought().getId());
        for (IdeaAcceptingRate i : accRate) {
            if (i.getUser().getId().equals(rate.getUser().getId())) {
                if (i.getRate() != rate.getRate()) this.acceptingRateRepository.updateIdeaRate(rate.getRate(), rate.getThought().getId(), rate.getUser().getId());
                return rate;
            }
        }
        return this.acceptingRateRepository.save(new IdeaAcceptingRate(rate));
    }

    @Override
    public InfoProjection getThoughtPartyRateInfo(UUID thoughtId, UUID partyId) {
        return this.rateRepository.getThoughtPartyRateInfo(thoughtId, partyId);
    }
    @Override
    public AcceptRateProjection getAcceptingPartyInfo(UUID thoughtId, UUID partyId) {
        //Вы издеваетесь? Опять запрос списка, но с 1 значением, зачем?
        return this.acceptingRateRepository.getAcceptingPartyInfo(thoughtId, partyId).get(0);
    }

    @Override
    public Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(UUID boardId, UUID ideaId, UUID partyId) {
        return this.rateRepository.getPartyMaxBidInfo(boardId, ideaId, partyId);
    }
}