package com.tengu.thoughts.services;

import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardMemberRepository;
import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.parties.repositories.PartyInitiativesRepository;
import com.tengu.service.ColorConverter;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.repositories.InitiativeAcceptingRateRepository;
import com.tengu.thoughts.repositories.InitiativeIdeasRepository;
import com.tengu.thoughts.repositories.InitiativeRepository;
import com.tengu.thoughts.repositories.InitiativeRepositoryRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InitiativeService implements ThoughtService {

    
    private InitiativeRepository repository;
    
    private BoardMemberRepository boardMemberRepository;
    
    private InitiativeRepositoryRate rateRepository;
    
    private InitiativeAcceptingRateRepository acceptingRateRepository;

    private InitiativeIdeasRepository initiativeIdeasRepository;

    private PartyInitiativesRepository partyInitiativesRepository;

    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;

    public InitiativeService(InitiativeRepository initiativeRepository,
                                 InitiativeRepositoryRate repositoryRate,
                                 InitiativeAcceptingRateRepository initiativeAcceptingRateRepository,
                                 BoardMemberRepository boardMemberRepository,
                                 InitiativeIdeasRepository initiativeIdeasRepository,
                                 PartyInitiativesRepository partyInitiativesRepository){
        this.repository = initiativeRepository;
        this.rateRepository = repositoryRate;
        this.acceptingRateRepository = initiativeAcceptingRateRepository;
        this.boardMemberRepository = boardMemberRepository;
        this.initiativeIdeasRepository = initiativeIdeasRepository;
        this.partyInitiativesRepository = partyInitiativesRepository;
    }
    @Override
    public Iterable<Initiative> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Iterable<InitiativeRate> findAllRated() {
        return this.rateRepository.findAll();
    }

    @Override
    public Optional<Initiative> findById(UUID id) {
        return this.repository.findById(id);
    }

    @Override
    public Iterable<Initiative> getByStatus(int status, UUID boardId) {
        return this.repository.getByStatus(status,boardId);
    }

    @Override
    public Iterable<Initiative> findByAuthor(UUID userId, UUID boardId) {
        return this.repository.findByAuthor(userId, boardId);
    }

    @Override
    public Iterable<Initiative> boardThoughts(UUID boardId) {
        return this.repository.boardInitiatives(boardId);
    }

    @Override
    public Long getThoughtLastNumber(UUID boardId) {
        return this.repository.getLastNumber(boardId);
    }

    @Override
    public void acceptThought(UUID thoughtId) {
        Initiative initiative = this.repository.findById(thoughtId).get();
        initiative.setStatus(2);
        this.repository.save(initiative);
        this.acceptingRateRepository.deleteByInitiative(thoughtId);
    }

    @Override
    public Thought save(Thought thought) {
        Initiative in = new Initiative(thought);
        in.setStatus(1);
        in.setNumber(getThoughtLastNumber(in.getBoard().getId())+1);
        return this.repository.save(in);
    }

    @Override
    public void update(Thought thought) {
        this.repository.update(thought.getId(),thought.getTitle(), thought.getText(),thought.getStatus());
    }

    @Override
    public void deleteThought(Thought thought) {
        Initiative initiative = new Initiative(thought);
        this.initiativeIdeasRepository.deleteAllInitiativeIdeasByInitiative(initiative.getId());
        if (thought.getStatus() == 1) {
            this.acceptingRateRepository.deleteByInitiative(thought.getId());
        }
        else if (thought.getStatus() == 2) {
            this.rateRepository.deleteByInitiative(thought.getId());
        }
        this.partyInitiativesRepository.deleteByThought(initiative.getId());
        this.repository.delete(initiative);
    }

    @Override
    public void updateRate(ThoughtRate thoughtRate) {
        this.rateRepository.update(thoughtRate.getRate(),
                thoughtRate.getUserRate(),thoughtRate.getId());
    }

    @Override
    public ThoughtRate saveRated(ThoughtRate thoughtRate) {
        return this.rateRepository.save(new InitiativeRate(thoughtRate));
    }

    @Override
    public Iterable<InitiativeRate> getRatedByUser(UUID userId, UUID boardId) {
        return this.rateRepository.getRatedByUser(userId, boardId);
    }

    @Override
    public InfoProjection getThoughtRateInfo(UUID thoughtId) {
        return this.rateRepository.getInitiativeRateInfo(thoughtId);
    }

    @Override
    public List<List<String>> getMatrixMap(UUID thoughtId) {
        int usersCount = this.boardMemberRepository.getMembers(this.repository.findById(thoughtId).get().getBoard().getId()).size();
        int objCount = (int) Math.ceil(Math.sqrt(usersCount));
        List<Integer> rates = this.rateRepository.getRatesForMap(thoughtId);
        List<List<String>> result = new ArrayList<List<String>>();
        List<String> tempArr = new ArrayList<String>();
        List<String> convArr;
        for (int i = objCount; i > 0; i--) {
            for (int j = objCount; j > 0; j--) {
                if(usersCount!=0){
                    if(rates.size()!=0) {
                        int ind = (int) (Math.random() * rates.size());
                        tempArr.add(ColorConverter.getColorOnRate(rates.get(ind)));
                        rates.remove(ind);
                    }else{
                        tempArr.add("#999999");
                    }
                    usersCount--;
                }else{
                    tempArr.add(null);
                }
            }
            convArr = new ArrayList<>(tempArr.subList(0, objCount));
            result.add(convArr);
            tempArr.clear();
        }
        return result;
    }

    @Override
    public Iterable<MapListSortInfoProjection> findByNameContainingForMap(String name, UUID boardId) {
        return this.repository.findByNameContainingForMap(name, boardId);
    }

    @Override
    public Iterable<MapListSortInfoProjection> getByNumberForMap(long number, UUID boardId) {
        return this.repository.getByNumberForMap(number, boardId);
    }


    @Override
    public Iterable<MapListSortInfoProjection> getAllValidatedForMap(UUID boardId) {
        return this.repository.getAllValidatedForMap(boardId);
    }

    @Override
    public AcceptRateProjection getAcceptingInfo(UUID thoughtId) {
        //TODO переделать по возможности, т.к это жуткий костыль
        return this.acceptingRateRepository.getAcceptingInfo(thoughtId).get(0);
    }

    @Override
    public Map<UUID, Integer> getAcceptingInfoByUser(UUID userId, UUID boardId) {
        List<InitiativeAcceptingRate> userRates = this.acceptingRateRepository.getAcceptingInfoByUser(userId, boardId);
        Map<UUID, Integer> userRatesMap = new HashMap<UUID, Integer>();
        for (InitiativeAcceptingRate initiative : userRates) {
            userRatesMap.put(initiative.getThought().getId(), initiative.getRate());
        }
        return userRatesMap;
    }

    @Override
    public Iterable<AverageAcceptRateProjection> getAvgAcceptRate(UUID boardId) {
        return this.acceptingRateRepository.getAvgAcceptRate(boardId);
    }

    @Override
    public ThoughtAcceptingRate saveAcceptingRate(ThoughtAcceptingRate rate) {
        List<InitiativeAcceptingRate> accRate = this.acceptingRateRepository.getByInitiative(rate.getThought().getId());
        for (InitiativeAcceptingRate i : accRate) {
            if (i.getUser().getId().equals(rate.getUser().getId())) {
                if (i.getRate() != rate.getRate()) this.acceptingRateRepository.updateInitiativeRate(rate.getRate(), rate.getThought().getId(), rate.getUser().getId());
                return rate;
            }
        }
        return this.acceptingRateRepository.save(new InitiativeAcceptingRate(rate));
    }

    @Override
    public InfoProjection getThoughtPartyRateInfo(UUID thoughtId, UUID partyId) {
        return this.rateRepository.getThoughtPartyRateInfo(thoughtId, partyId);
    }

    @Override
    public AcceptRateProjection getAcceptingPartyInfo(UUID thoughtId, UUID partyId) {
        return this.acceptingRateRepository.getAcceptingPartyInfo(thoughtId, partyId).get(0);
    }

    @Override
    public Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(UUID boardId, UUID initiativeId, UUID partyId) {
        return this.rateRepository.getPartyMaxBidInfo(boardId, initiativeId, partyId);
    }

    public void setBrokerMessagingTemplate(SimpMessagingTemplate simpMessagingTemplate){
        this.brokerMessagingTemplate = simpMessagingTemplate;
    }
}
