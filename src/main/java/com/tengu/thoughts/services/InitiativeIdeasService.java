package com.tengu.thoughts.services;

import com.tengu.commonProjections.InfoProjection;
import com.tengu.thoughts.entities.InitiativeIdeas;
import com.tengu.thoughts.projections.InitiativeIdeasInfoProjection;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

public interface InitiativeIdeasService {
    Optional<InitiativeIdeas> findByUser(UUID userId, UUID boardId);
    Iterable<InitiativeIdeas> findByUserAndInitiative(UUID userId, UUID initiativeId);
    InitiativeIdeas saveInitiativeIdeas(InitiativeIdeas initiativeIdea) throws IOException;
    InfoProjection getInitiativesRateInfo(UUID initiativeId);
    void deleteInitiativeIdea(UUID initiativeIdeaId);
    Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasList(UUID initiativeId);
    Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesList(UUID ideaId);
    Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasPartyList(UUID initiativeId, UUID partyId);
    Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesPartyList(UUID ideaId, UUID partyId);
}
