package com.tengu.thoughts.services;

import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyIdeas;
import com.tengu.parties.entities.PartyThought;
import com.tengu.parties.repositories.PartyThoughtRepository;
import com.tengu.thoughts.dto.MaxBidInfoDTO;
import com.tengu.thoughts.entities.IdeaBid;
import com.tengu.thoughts.entities.Thought;
import com.tengu.thoughts.entities.ThoughtBid;
import com.tengu.thoughts.repositories.ThoughtBidRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public abstract class VotingResultAbstractService< E extends ThoughtBid, R extends ThoughtBidRepository<E>,
        S extends ThoughtBidCommonService<E>, PT extends PartyThought, T extends Thought,
        PTR extends PartyThoughtRepository<PT, T>> implements VotingResultCommonService{

    R repository;
    S thoughtBidService;
    PTR partyThoughtRepository;

    @Autowired
    public VotingResultAbstractService(R repository, S thoughtBidService, PTR partyThoughtRepository){
        this.repository = repository;
        this.thoughtBidService = thoughtBidService;
        this.partyThoughtRepository = partyThoughtRepository;
    }


    public Party concludeVoting(UUID thoughtId){
        Iterable<E> masIdeaBid = repository.getAllBidsForThought(thoughtId);
        E maxBid = null;
        for(E i : masIdeaBid) {
            if (maxBid == null) {
                maxBid = i;
            } else {
                if (thoughtBidService.calculateLoyalty(maxBid) < thoughtBidService.calculateLoyalty(i)) {
                    maxBid = i;
                }
            }
        }
        PT partyThought = createPartyThought(maxBid);
        if (partyThoughtRepository.existsByThoughtId(partyThought.getThought().getId()) != null)
            partyThoughtRepository.update(partyThought.getParty().getId(), partyThought.getThought().getId());
        else
            partyThoughtRepository.save(partyThought);
        repository.deleteAll(masIdeaBid);
        return maxBid.getParty();
    }

    abstract PT createPartyThought(E bid);



}
