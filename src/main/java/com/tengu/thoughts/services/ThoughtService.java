package com.tengu.thoughts.services;

import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.projections.PartyMaxBidInfoProjection;
import com.tengu.thoughts.entities.Thought;
import com.tengu.thoughts.entities.ThoughtAcceptingRate;
import com.tengu.thoughts.entities.ThoughtRate;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface ThoughtService {
   Iterable<? extends Thought> findAll();
   Iterable<? extends ThoughtRate> findAllRated();
   Optional<? extends Thought> findById(UUID id);
   Iterable<? extends Thought> getByStatus(int status, UUID boardId);
   Iterable<? extends Thought> findByAuthor(UUID userId, UUID boardId);
   Iterable<? extends Thought> boardThoughts(UUID boardId);
   Long getThoughtLastNumber(UUID boardId);
   void acceptThought(UUID thoughtId);
   Thought save(Thought thought);
   void update(Thought thought);
   void deleteThought(Thought thought);
   void updateRate(ThoughtRate thoughtRate);
   ThoughtRate saveRated(ThoughtRate thoughtRate);
   Iterable<? extends ThoughtRate> getRatedByUser(UUID userId,UUID boardId);
   InfoProjection getThoughtRateInfo(UUID thoughtId);
   List<List<String>> getMatrixMap(UUID thoughtId);
   Iterable<MapListSortInfoProjection> findByNameContainingForMap(String name, UUID boardId);
   Iterable<MapListSortInfoProjection> getByNumberForMap(long number, UUID boardId);
   Iterable<MapListSortInfoProjection> getAllValidatedForMap(UUID boardId);
   AcceptRateProjection getAcceptingInfo(UUID thoughtId);
   Iterable<AverageAcceptRateProjection> getAvgAcceptRate(UUID boardId);
   Map<UUID, Integer> getAcceptingInfoByUser(UUID userId, UUID boardId);
   ThoughtAcceptingRate saveAcceptingRate(ThoughtAcceptingRate rate);
   InfoProjection getThoughtPartyRateInfo(UUID thoughtId, UUID partyId);
   AcceptRateProjection getAcceptingPartyInfo(UUID thoughtId, UUID partyId);
   Iterable<PartyMaxBidInfoProjection> getPartyMaxBidInfo(UUID boardId, UUID ideaId, UUID partyId);
}
