package com.tengu.thoughts.services;

import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyThought;

import java.util.UUID;

public interface VotingResultCommonService {

    Party concludeVoting(UUID thoughtId);


}
