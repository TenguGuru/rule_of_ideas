package com.tengu.thoughts.services;

import com.tengu.commonProjections.InfoProjection;
import com.tengu.thoughts.entities.InitiativeIdeas;
import com.tengu.thoughts.repositories.InitiativeIdeasRepository;
import com.tengu.thoughts.projections.InitiativeIdeasInfoProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
public class InitiativeIdeasServiceImpl implements InitiativeIdeasService {

    @Autowired
    private InitiativeIdeasRepository repository;

    @Override
    public Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasList(UUID thoughtId) {
        return this.repository.getInitiativeIdeasList(thoughtId);
    }

    @Override
    public Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesList(UUID ideaId) {
        return this.repository.getIdeaInitiativesList(ideaId);
    }
    @Override
    public Iterable<InitiativeIdeasInfoProjection> getInitiativeIdeasPartyList(UUID initiativeId, UUID partyId) {
        return this.repository.getInitiativeIdeasPartyList(initiativeId, partyId);
    }
    @Override
    public Iterable<InitiativeIdeasInfoProjection> getIdeaInitiativesPartyList(UUID ideaId, UUID partyId) {
        return this.repository.getIdeaInitiativesPartyList(ideaId, partyId);
    }
    @Override
    public Optional<InitiativeIdeas> findByUser(UUID userId, UUID boardId) {
        return this.repository.findByUser(userId, boardId);
    }

    @Override
    public Iterable<InitiativeIdeas> findByUserAndInitiative(UUID userId, UUID initiativeId) {
        return this.repository.findByUserAndInitiative(userId,initiativeId);
    }

    @Override
    public InitiativeIdeas saveInitiativeIdeas(InitiativeIdeas initiativeIdea) throws IOException {
        return this.repository.save(initiativeIdea);
    }

    @Override
    public InfoProjection getInitiativesRateInfo(UUID initiativeId) {
        return this.repository.getInitiativesRateInfo(initiativeId);
    }

    @Override
    public void deleteInitiativeIdea(UUID initiativeIdeaId) {
        this.repository.deleteById(initiativeIdeaId);
    }
}
