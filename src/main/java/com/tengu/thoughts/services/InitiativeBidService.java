package com.tengu.thoughts.services;

import com.tengu.parties.services.PartyService;
import com.tengu.thoughts.entities.InitiativeBid;
import com.tengu.thoughts.repositories.InitiativeBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitiativeBidService extends ThoughtBidAbstractService<InitiativeBid, InitiativeBidRepository> {

    @Autowired
    private InitiativeBidRepository initiativeBidRepository;
    private PartyService partyService;
    private InitiativeService initiativeService;

    public InitiativeBidService(InitiativeBidRepository repository, PartyService partyService,
                                ThoughtService initiativeService) {
        super(repository, partyService, initiativeService);
    }


}
