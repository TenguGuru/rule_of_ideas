package com.tengu.thoughts.services;

import com.tengu.parties.entities.Party;
import com.tengu.parties.services.PartyService;
import com.tengu.thoughts.entities.ThoughtBid;
import com.tengu.thoughts.dto.MaxBidInfoDTO;
import com.tengu.thoughts.repositories.ThoughtBidRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public abstract class ThoughtBidAbstractService<E extends ThoughtBid, R extends ThoughtBidRepository<E>> implements ThoughtBidCommonService<E> {

    private R repository;
    private PartyService partyService;
    private ThoughtService thoughtService;


    @Autowired
    public ThoughtBidAbstractService(R repository, PartyService partyService, ThoughtService thoughtService) {
        this.repository = repository;
        this.partyService = partyService;
        this.thoughtService = thoughtService;
    }

    @Override
    public Integer getLoyaltyPercent(UUID partyId, UUID thoughtId) {
        return this.repository.getLoyaltyPercent(partyId, thoughtId);
    }

    @Override
    public void updateLoyaltyPercent(UUID partyId, UUID thoughtId, Integer loyaltyPercent) {
        this.repository.updateLoyaltyPercent(partyId, thoughtId, loyaltyPercent);
    }

    @Override
    public Integer getAttempts(UUID partyId, UUID thoughtId) {
        return this.repository.getAttempts(partyId, thoughtId);
    }

    @Override
    public E save(E thoughtBid) {
        if (thoughtBid.getAttempt() != 0) {
            thoughtBid.setAttempt(thoughtBid.getAttempt() - 1);
        }
        return this.repository.save(thoughtBid);
    }

    @Override
    public E getByPartyAndThought(UUID partyId, UUID thoughtId){
        return this.repository.getByPartyAndThought(partyId, thoughtId);
    }

    @Override
    public Iterable<Party> getPartiesByThought(UUID thoughtId){
        return this.repository.getPartiesByThought(thoughtId);
    }

    @Override
    public Map<UUID, MaxBidInfoDTO> getAllThoughtBidsForMap(UUID boardId){
        Iterable <E> thoughtBids =  this.repository.getAllThoughtBidsForMap(boardId);
        Map <UUID, MaxBidInfoDTO> maxThoughtBids = new HashMap<UUID, MaxBidInfoDTO>();
        for(E i : thoughtBids){
            /*if (!maxThoughtBids.containsKey(i.getThought().getId())){
                E maxBid = i;
                for(E maxBidThought : thoughtBids){
                    if(i.getThought().getId() == maxBidThought.getThought().getId() &&
                            (calculateLoyalty(i) < calculateLoyalty(maxBidThought) ||
                                    (calculateLoyalty(i) == calculateLoyalty(maxBidThought) &&
                                            maxBidThought.getLoyaltyPercent() < i.getLoyaltyPercent()))
                    )
                        maxBid = maxBidThought;
                }
                maxThoughtBids.put(i.getThought().getId(), new MaxBidInfoDTO(maxBid.getParty(), calculateLoyalty(maxBid)));
            }*/
            if (!maxThoughtBids.containsKey(i.getThought().getId())) {
                maxThoughtBids.put(i.getThought().getId(), new MaxBidInfoDTO(i.getParty(), calculateLoyalty(i)));
            }
            else if (maxThoughtBids.get(i.getThought().getId()).getLoyalty() < calculateLoyalty(i)) {
                maxThoughtBids.put(i.getThought().getId(), new MaxBidInfoDTO(i.getParty(), calculateLoyalty(i)));
            }
        }
        return maxThoughtBids;
    }

    public double calculateLoyalty(E i) {
        return i.getLoyaltyPercent() * partyService.getPartyMembersCount(i.getParty().getId()) *
                thoughtService.getThoughtPartyRateInfo(i.getThought().getId(), i.getParty().getId()).getRate();
    }


}
