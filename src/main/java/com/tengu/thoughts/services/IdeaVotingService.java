package com.tengu.thoughts.services;

import com.tengu.parties.entities.Party;
import com.tengu.parties.entities.PartyIdeas;
import com.tengu.parties.repositories.PartyIdeasRepository;
import com.tengu.thoughts.dto.MaxBidInfoDTO;
import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.entities.IdeaBid;
import com.tengu.thoughts.repositories.IdeaBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Service
public class IdeaVotingService extends VotingResultAbstractService<IdeaBid,
        IdeaBidRepository, IdeaBidService, PartyIdeas,
        Idea, PartyIdeasRepository> {

    @Autowired
    private IdeaBidRepository ideaBidRepository;
    @Autowired
    private PartyIdeasRepository partyThoughtRepository;

    protected IdeaVotingService(IdeaBidRepository repository, IdeaBidService thoughtBidService, PartyIdeasRepository partyThoughtRepository) {
        super(repository, thoughtBidService, partyThoughtRepository);
    }

    @Override
    PartyIdeas createPartyThought(IdeaBid bid) {
        PartyIdeas ideas = new PartyIdeas();
        ideas.setThought(bid.getThought());
        ideas.setParty(bid.getParty());
        return ideas;
    }
}
