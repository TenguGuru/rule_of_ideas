package com.tengu.service;

import com.tengu.parties.services.PartyService;
import com.tengu.thoughts.services.IdeaService;
import com.tengu.thoughts.services.InitiativeService;
import com.tengu.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/data")
public class DataController {

    @Autowired
    private UserService userService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private IdeaService ideaService;

    @Autowired
    private InitiativeService initiativeService;

    @PostMapping("/delete")
    public void delete(@RequestBody Map<String, Object> params){
        UUID entityId = UUID.fromString((String) params.get("id"));
        switch ((String) params.get("type")){
            case "user":
                this.userService.delete(userService.getUserById(entityId));
                break;
            case "party":
                this.partyService.delete(partyService.findById(entityId).get());
                break;
            case "initiative":
                this.initiativeService.deleteThought(initiativeService.findById(entityId).get());
                break;
            case "idea":
                this.ideaService.deleteThought(ideaService.findById(entityId).get());
                break;
        }
    }
}
