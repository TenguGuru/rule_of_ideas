package com.tengu.service;

public class ColorConverter {
    public static String getColorOnRate(Integer rate) {
        String result;
        switch (rate){
            case 0:{
                result = "#FFFF00";
                break;
            }
            case 1:
            case -1:{
                if(rate<0){
                    result = "#EECC00";
                }else{
                    result = "#F6FF00";
                }
                break;
            }
            case 2:
            case -2:{
                if(rate<0){
                    result="#FFBB00";
                }else{
                    result="#D6FF00";
                }
                break;
            }
            case 3:
            case -3:{
                if(rate<0){
                    result="#ffaa00";
                }else{
                    result="#C9FF00";
                }
                break;
            }
            case 4:
            case -4:{
                if(rate<0){
                    result="#ffa100";
                }else{
                    result="#bFff00";
                }
                break;
            }
            case 5:
            case -5:{
                if(rate<0){
                    result="#ff8800";
                }else{
                    result="#AFff00";
                }
                break;
            }
            case 6:
            case -6:{
                if(rate<0){
                    result="#ff6600";
                }else{
                    result="#A0ff00";
                }
                break;
            }
            case 7:
            case -7:{
                if(rate<0){
                    result="#ff5500";
                }else{
                    result="#91ff00";
                }
                break;
            }
            case 8:
            case -8:{
                if(rate<0){
                    result="#ff4400";
                }else{
                    result="#80ff00";
                }
                break;
            }
            case 9:
            case -9:{
                if(rate<0){
                    result="#ff2200";
                }else{
                    result="#50ff00";
                }
                break;
            }
            case 10:
            case -10:{
                if(rate<0){
                    result="#ff0000";
                }else{
                    result="#00ff00";
                }
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + rate);
        }
        return result;
    }
}
