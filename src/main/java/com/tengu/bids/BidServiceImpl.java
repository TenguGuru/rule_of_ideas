package com.tengu.bids;

import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardMemberRepository;
import com.tengu.parties.repositories.PartyRepository;
import com.tengu.security.jwt.UserPrinciple;
import com.tengu.thoughts.entities.Idea;
import com.tengu.thoughts.repositories.IdeaBidRepository;
import com.tengu.thoughts.repositories.InitiativeBidRepository;
import com.tengu.thoughts.services.IdeaService;
import com.tengu.thoughts.services.InitiativeService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class BidServiceImpl implements BidService{

    private IdeaBidRepository ideaBidRepository;
    private InitiativeBidRepository initiativeBidRepository;
    private PartyRepository partyRepository;
    private IdeaService ideaService;
    private InitiativeService initiativeService;
    private BoardMemberRepository boardMemberRepository;


    public BidServiceImpl(IdeaBidRepository ideaBidRepository, InitiativeBidRepository initiativeBidRepository,
                          PartyRepository partyRepository, IdeaService ideaService, InitiativeService initiativeService,
                          BoardMemberRepository boardMemberRepository){
        this.ideaBidRepository = ideaBidRepository;
        this.initiativeBidRepository = initiativeBidRepository;
        this.partyRepository = partyRepository;
        this.ideaService = ideaService;
        this.initiativeService = initiativeService;
        this.boardMemberRepository = boardMemberRepository;
    }

    @Override
    public void updateBidding(UUID boardId) {
        SecurityContext securityContext = new SecurityContextHolder().getContext();
        Authentication auth = securityContext.getAuthentication();
        if (auth != null) {
            BoardMember user = this.boardMemberRepository.findMember(((UserPrinciple) auth.getPrincipal()).getId(), boardId);
            if (user.getRole() == BoardMember.MemberRole.ORGANIZER || user.getRole() == BoardMember.MemberRole.CREATOR) {
                ideaService.boardThoughts(boardId).forEach(idea -> {
                    ideaBidRepository.deleteAll(ideaBidRepository.getAllBidsForThought(idea.getId()));
                });
                initiativeService.boardThoughts(boardId).forEach(initiative -> {
                    initiativeBidRepository.deleteAll(initiativeBidRepository.getAllBidsForThought(initiative.getId()));
                });
                partyRepository.boardParty(boardId).forEach(party -> {
                    partyRepository.restoreAllPartyLoyaltyForBidding(party.getId());
                });
            }
        }
    }
}
