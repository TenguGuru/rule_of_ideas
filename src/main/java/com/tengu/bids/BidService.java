package com.tengu.bids;

import java.util.UUID;

public interface BidService {
    void updateBidding(UUID boardId);
}
