package com.tengu.bids;

import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/bid")
public class BidController {

    private BidService bidService;

    public BidController(BidService bidService){
        this.bidService = bidService;
    }

    @GetMapping("/updateBidding/{boardId}")
    public void updateBidding(@PathVariable("boardId") UUID boardId){
        this.bidService.updateBidding(boardId);
    }
}
