package com.tengu.boards;

import java.util.UUID;

public interface BoardBiddingService {
    boolean isBidding(UUID boardId);
}
