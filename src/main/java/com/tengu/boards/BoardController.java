package com.tengu.boards;


import com.tengu.security.jwt.UserPrinciple;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
    @RequestMapping("api/boards")
public class BoardController {
    private BoardService service;
    private UserRepository userRepository;

    public BoardController(BoardService service, UserRepository userRepository){
        this.service = service;
        this.userRepository = userRepository;
    }

    @GetMapping("/{id}")
    public Optional<Board> findById(@PathVariable UUID id){
      return this.service.findById(id);
    }

    @PostMapping("/addBoard")
    public Board addBoard(@RequestBody AddBoard addBoard){
        return this.service.addBoard(addBoard);
    }

    @PostMapping("/save")
    public Board save(@RequestBody Board board){
        return this.service.save(board);
    }

    @GetMapping("/findByCreator/{creatorId}")
    public Iterable<Board> findByCreator(@PathVariable UUID creatorId){
        return this.service.findByCreator(creatorId);
    }

    @GetMapping("/findByName/{name}")
    public Optional<Board> findByName(@PathVariable String name){
        return this.service.findByName(name);
    }

    @GetMapping("/getMembers/{boardId}")
    public Set<BoardMember> getMembers(@PathVariable UUID boardId){
        return this.service.getMembers(boardId);
    }

    @GetMapping("/findMember/{userId}/{boardId}")
    public BoardMember findMember(@PathVariable("userId") UUID userId,
                                           @PathVariable("boardId") UUID boardId){
        return this.service.findMember(userId,boardId);
    }

    @GetMapping("/getBoards/{userId}")
    public Iterable<Board> getBoards(@PathVariable("userId") UUID userId){
        return this.service.getBoards(userId);
    }

    @GetMapping("/getQuota/{boardId}")
    public BoardQuota getQuota(@PathVariable("boardId") UUID boardId){
        return this.service.getBoardQuota(boardId);
    }

    @PostMapping("/changeRole/")
    public BoardMember changeRole(@RequestBody ChangeMemberRole change, Principal principal){
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            BoardMember.MemberRole memberRole = this.service.getBoardMemberUser(change.getMemberId(), user.getId()).getRole();

            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return this.service.changeMemberRole(change);
    }

    @GetMapping("/getCreator/{boardId}")
    public User getCreator(@PathVariable("boardId") UUID boardId){
        return this.service.getCreator(boardId);
    }

    @PostMapping("/bidding/update/")
    public BoardSettings updateBidding(@RequestBody BoardSettings boardSettings, Principal principal) {
        User user = this.userRepository.findByEmail(principal.getName()).get();
        if(user.getUserRole() != User.UserRole.ADMIN) {
            BoardMember.MemberRole memberRole = service.findMember(user.getId(), boardSettings.getBoard().getId()).getRole();


            if (!(memberRole == BoardMember.MemberRole.MEMBER ||
                    memberRole == BoardMember.MemberRole.ORGANIZER ||
                    memberRole == BoardMember.MemberRole.CREATOR)) {
                return null;
            }
        }
        return this.service.updateBidding(boardSettings);
    }

    @GetMapping("/bidding/getStatus/{boardId}/")
    public Boolean getBiddingStatus(@PathVariable("boardId") UUID boardId) {
        return this.service.getBiddingStatus(boardId);
    }
}
