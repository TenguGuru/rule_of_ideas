package com.tengu.boards;

import com.tengu.users.User;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface BoardService {
    Board save(Board board);
    Long getLastNumber();
    Board addBoard(AddBoard addBoard);
    Iterable<Board> findByCreator(UUID creatorId);
    Optional<Board> findById(UUID boardId);
    Optional<Board> findByName(String name);
    Optional<Board> getByCode(String code);
    Set<BoardMember> getMembers(UUID boardId);
    Iterable<Board> getBoards(UUID userId);
    BoardMember findMember(UUID userId, UUID boardId);
    BoardMember getBoardMemberUser(UUID memberId, UUID userId);
    BoardQuota getBoardQuota(UUID boardId);
    BoardMember changeMemberRole(ChangeMemberRole change);
    User getCreator(UUID boardId);
    BoardSettings updateBidding(BoardSettings boardSettings);
    Boolean getBiddingStatus(UUID boardId);
}
