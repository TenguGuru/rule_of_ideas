package com.tengu.boards;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;
@Setter
@Getter
public class AddBoard {
    private String boardCode;
    private UUID userId;
    private String role;
}
