package com.tengu.boards;

public interface BoardQuota {
    Integer getIdeas();
    Integer getUsers();
    Integer getInitiatives();
}
