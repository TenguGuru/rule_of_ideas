package com.tengu.boards;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BoardSettingsRepository extends CrudRepository<BoardSettings, UUID> {
    @Query("SELECT b from BoardSettings b where b.board.id=:boardId")
    Optional<BoardSettings> getBoardSettings(@Param("boardId") UUID boardId);

    @Transactional
    @Modifying
    @Query("UPDATE BoardSettings b set b.bidding=:biddingStatus where b.board.id=:boardId")
    void updateBidding(@Param("biddingStatus") Boolean biddingStatus, @Param("boardId") UUID boardId);
}
