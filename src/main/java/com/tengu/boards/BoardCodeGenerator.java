package com.tengu.boards;

public interface BoardCodeGenerator {
    String generateCode();
}
