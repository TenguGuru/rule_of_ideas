package com.tengu.boards;

import com.tengu.users.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BoardRepository extends CrudRepository<Board, UUID> {

    @Query("SELECT b FROM Board b where b.name =:name")
    Optional<Board> getByName(@Param("name") String name);

    @Query("SELECT b FROM Board b where b.code =:code")
    Optional<Board> getByCode(@Param("code") String code);

    @Query("SELECT b FROM Board b where b.creator.id =:creatorId")
    Iterable<Board> findByCreator(@Param("creatorId") UUID creatorId);

    @Query("SELECT count(b.number) from Board b")
    Long getLastNumber();
    @Query("SELECT " +
            "count(bm) as users," +
            "(SELECT count(id) from Idea id Where id.board.id=:boardId) as ideas," +
            "(SELECT count(i) from Initiative i Where i.board.id=:boardId) as initiatives" +
            " FROM BoardMember bm" +
            " Where bm.board.id=:boardId")
    BoardQuota getBoardQuota(@Param("boardId") UUID boardId);
    @Query("SELECT b.creator from Board b where b.id = :boardId")
    User getCreator(@Param("boardId") UUID boardId);
}
