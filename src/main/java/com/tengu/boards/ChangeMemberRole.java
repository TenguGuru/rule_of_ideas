package com.tengu.boards;

import java.util.UUID;

public class ChangeMemberRole {

   private UUID memberId;
   private String role;

    public UUID getMemberId() {
        return memberId;
    }

    public void setMemberId(UUID memberId) {
        this.memberId = memberId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
