package com.tengu.boards;

import com.tengu.security.jwt.UserPrinciple;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.nio.file.attribute.UserPrincipal;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class BoardServiceImpl implements BoardService {

    private BoardRepository repository;
    private UserRepository userRepository;
    private BoardMemberRepository boardMemberRepository;
    private BoardCodeGeneratorImpl boardCodeGenerator;
    private BoardSettingsRepository boardSettingsRepository;

    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;




    public BoardServiceImpl(BoardRepository repository,
                            UserRepository userRepository,
                            BoardMemberRepository boardMemberRepository,
                            BoardSettingsRepository boardSettingsRepository) {
        this.repository = repository;


        this.userRepository = userRepository;
        this.boardMemberRepository = boardMemberRepository;
        this.boardSettingsRepository = boardSettingsRepository;
        boardCodeGenerator = new BoardCodeGeneratorImpl(repository);
    }

    @Override
    public Long getLastNumber(){
        return this.repository.getLastNumber();
    }

    @Override
    public Board save(Board board) {
        board.setCode(boardCodeGenerator.generateCode());
        if(board.getNumber()==null) {
            board.setNumber(this.getLastNumber() + 1);
        }
        Board saved = this.repository.save(board);

        //TODO fix later
        // brokerMessagingTemplate.convertAndSend("/message", board);
        return saved;
    }


    @Override
    public Board addBoard(AddBoard addBoard) {
        Optional<Board> board = this.repository.getByCode(addBoard.getBoardCode());


        if (board.isPresent()) {
            BoardMember bmCheck = this.boardMemberRepository.findMember(addBoard.getUserId(),
                    board.get().getId());
            if(bmCheck == null){
                if(board.get().getCode().equals(addBoard.getBoardCode())) {
                    User user = this.userRepository.findById(addBoard.getUserId()).get();
                    BoardMember bm = new BoardMember();
                    bm.setUser(user);
                    bm.setBoard(board.get());
                    bm.setRole(BoardMember.MemberRole.getById(addBoard.getRole()));
                    this.boardMemberRepository.save(bm);
                    return board.get();
                }
            }
        }
        return null;
    }

    @Override
    public Iterable<Board> findByCreator(UUID creatorId) {
        return this.repository.findByCreator(creatorId);
    }

    @Override
    public Optional<Board> findByName(String name){
      return this.repository.getByName(name);
    }

    @Override
    public Set<BoardMember> getMembers(UUID boardId) {
        return this.boardMemberRepository.getMembers(boardId);
    }

    @Override
    public Iterable<Board> getBoards(UUID userId) {
        return this.boardMemberRepository.getBoards(userId);
    }

    @Override
    public BoardMember findMember(UUID userId, UUID boardId) {
        return this.boardMemberRepository.findMember(userId,boardId);
    }

    @Override
    public BoardQuota getBoardQuota(UUID boardId) {
        return this.repository.getBoardQuota(boardId);
    }

    @Override
    public BoardMember changeMemberRole(ChangeMemberRole change) {
        BoardMember bm = this.boardMemberRepository.findById(change.getMemberId()).get();
        bm.setRole(BoardMember.MemberRole.getById(change.getRole()));
        return this.boardMemberRepository.save(bm);
    }

    public BoardMember getBoardMemberUser(UUID memberId, UUID userId) {
        UUID board_id = this.boardMemberRepository.findById(memberId).get().getBoard().getId();
        return this.boardMemberRepository.findMember(userId, board_id);
    }

    @Override
    public User getCreator(UUID boardId) {
        return this.repository.getCreator(boardId);
    }

    @Override
    public Optional<Board> findById(UUID boardId) {
        return this.repository.findById(boardId);
    }

    @Override
    public Optional<Board> getByCode(String code) { return this.repository.getByCode(code); }

    @Override
    public BoardSettings updateBidding(BoardSettings boardSettings) {
        UUID boardId = boardSettings.getBoard().getId();
        if (this.boardSettingsRepository.getBoardSettings(boardId).isEmpty()) {
            BoardSettings boardSettingsToSave = new BoardSettings();
            boardSettingsToSave.setBoard(this.findById(boardId).get());
            boardSettingsToSave.setBidding(boardSettings.isBidding());
            return this.boardSettingsRepository.save(boardSettingsToSave);
        } else {
            this.boardSettingsRepository.updateBidding(boardSettings.isBidding(), boardId);
            return this.boardSettingsRepository.getBoardSettings(boardId).get();
        }
    }

    @Override
    public Boolean getBiddingStatus(UUID boardId) {
        Optional<BoardSettings> boardSettings = this.boardSettingsRepository.getBoardSettings(boardId);
        return boardSettings.isPresent()? boardSettings.get().isBidding(): false;
    }
}
