package com.tengu.boards;

import java.util.Random;

public class BoardCodeGeneratorImpl implements BoardCodeGenerator {
    public final String ALLOWED_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    public final String ALLOWED_DIGITS = "0123456789";
    public final int OUTPUT_STRING_LENGTH = 6;

    private BoardRepository boardRepository;

    public BoardCodeGeneratorImpl(BoardRepository boardRepository){
        this.boardRepository = boardRepository;
    }

    public String generateCode() {
        String boardCode;
        do {
            boardCode = getRandomString(OUTPUT_STRING_LENGTH, ALLOWED_CHARACTERS, ALLOWED_DIGITS);
        } while (boardRepository.getByCode(boardCode).isPresent());
        return boardCode;
    }

    private static String getRandomString(int length, String allowedLetters, String allowedDigits){
        int lettersCount = 0;
        int digitsCount = 0;

        StringBuilder randomString = new StringBuilder();

        for(int i = 0 ; i < length; i++){

            Random randomSymbol = new Random();
            boolean letOrDigit = new Random().nextBoolean();

            // true - letter
            // false - digit
            if (lettersCount == 3){
                letOrDigit = false;
            }
            if (digitsCount == 3){
                letOrDigit = true;
            }

            if (letOrDigit) {
                lettersCount++;
            } else {
                digitsCount++;
            }

            int randomInt;

            if (letOrDigit){
                randomInt = randomSymbol.nextInt(allowedLetters.length());
                randomString.append(allowedLetters.charAt(randomInt));
            }
            else {
                randomInt = randomSymbol.nextInt(allowedDigits.length());
                randomString.append(allowedDigits.charAt(randomInt));
            }
        }
        return randomString.toString();
    }
}
