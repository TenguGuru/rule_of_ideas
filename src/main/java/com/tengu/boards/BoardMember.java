package com.tengu.boards;

import com.tengu.users.User;
import lombok.Builder;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class BoardMember {

    public static enum MemberRole {
        OBSERVER, MEMBER, ORGANIZER, CREATOR;
        public static BoardMember.MemberRole getById(String id){
            for(BoardMember.MemberRole e : values()) {
                if(e.name().equalsIgnoreCase(id)) return e;
            }
            return MEMBER;
        }
    }

    @Id
    @GeneratedValue
    private UUID id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", columnDefinition="uuid not null")
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id", columnDefinition="uuid not null")
    private Board board;
    @Builder.Default
    private BoardMember.MemberRole role = MemberRole.MEMBER;
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public MemberRole getRole() {
        return role;
    }

    public void setRole(MemberRole role) {
        this.role = role;
    }
}
