package com.tengu.boards;

import com.tengu.users.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface BoardMemberRepository extends CrudRepository<BoardMember, UUID> {

    @Query("SELECT b from BoardMember b where b.board.id=:boardId")
    Set<BoardMember> getMembers(@Param("boardId") UUID boardId);

    @Query("SELECT b from BoardMember b where b.board.id=:boardId and b.user.id=:userId")
    BoardMember findMember(@Param("userId") UUID userId,@Param("boardId") UUID boardId);

    @Query("SELECT bm.board FROM BoardMember bm where bm.user.id =:userId")
    Iterable<Board> getBoards(@Param("userId") UUID userId);
}
