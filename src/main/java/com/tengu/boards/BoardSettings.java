package com.tengu.boards;

import javax.persistence.*;

@Entity
public class BoardSettings {
    @Id
    @GeneratedValue
    private Long id;
    private Boolean bidding;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id", columnDefinition="uuid not null")
    private Board board;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isBidding() { return bidding; }

    public void setBidding(boolean bidding) { this.bidding = bidding; }

    public Board getBoard() { return board; }

    public void setBoard(Board board) { this.board = board; }
}
