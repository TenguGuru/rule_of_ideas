package com.tengu.boards;

import com.tengu.parties.entities.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class BoardSettingsService implements BoardBiddingService {


    @Autowired
    private BoardSettingsRepository boardSettingsRepository;


    @Override
    public boolean isBidding(UUID boardId){
        return boardSettingsRepository.getBoardSettings(boardId).get().isBidding() ||
                boardSettingsRepository.getBoardSettings(boardId).isEmpty();
    }

}
