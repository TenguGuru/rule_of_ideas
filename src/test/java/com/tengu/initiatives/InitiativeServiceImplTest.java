package com.tengu.initiatives;

import com.tengu.boards.Board;
import com.tengu.boards.BoardMember;
import com.tengu.boards.BoardMemberRepository;
import com.tengu.commonProjections.AcceptRateProjection;
import com.tengu.commonProjections.AverageAcceptRateProjection;
import com.tengu.commonProjections.MapListSortInfoProjection;
import com.tengu.parties.repositories.PartyInitiativesRepository;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.projections.InitiativeIdeasInfoProjection;
import com.tengu.thoughts.repositories.InitiativeAcceptingRateRepository;
import com.tengu.thoughts.repositories.InitiativeIdeasRepository;
import com.tengu.thoughts.repositories.InitiativeRepository;
import com.tengu.thoughts.repositories.InitiativeRepositoryRate;
import com.tengu.thoughts.services.InitiativeService;
import com.tengu.users.User;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;

class InitiativeServiceImplTest {

    private  static InitiativeService initiativeService;

    @Mock
    private static InitiativeRepository mockedInitiativeRepository;
    @Mock
    private static InitiativeRepositoryRate mockedInitiativeRepositoryRate;
    @Mock
    private static InitiativeIdeasRepository mockedInitiativeIdeasRepository;
    @Mock
    private static InitiativeAcceptingRateRepository mockedInitiativeAcceptingRateRepository;
    @Mock
    private static BoardMemberRepository mockedBoardMemberRepository;
    @Mock
    private static PartyInitiativesRepository mockedPartyInitiativesRepository;

    private Board board;
    private User user;
    private Initiative initiative1;

    @BeforeAll
    public static void globalSetUp(){
        mockedInitiativeRepository = mock(InitiativeRepository.class);
        mockedInitiativeRepositoryRate = mock(InitiativeRepositoryRate.class);
        mockedInitiativeIdeasRepository = mock(InitiativeIdeasRepository.class);
        mockedInitiativeAcceptingRateRepository = mock(InitiativeAcceptingRateRepository.class);
        mockedBoardMemberRepository = mock(BoardMemberRepository.class);

        System.out.println("Настройка перед запуском класса");
    }

    @BeforeEach
    public void setUp(){
        initiativeService = new InitiativeService(mockedInitiativeRepository,
                mockedInitiativeRepositoryRate,mockedInitiativeAcceptingRateRepository,mockedBoardMemberRepository,mockedInitiativeIdeasRepository,
                mockedPartyInitiativesRepository);
        System.out.println("Перенастройка перед запуском задания");
        initiativeService.setBrokerMessagingTemplate(mock(SimpMessagingTemplate.class));
        board = new Board();
        user = new User();
        initiative1 = new Initiative(
                UUID.randomUUID(), 1l, 0, "first initiative title","Description",
                board, user);
    }

    @Test
    void testFindAllTest() {
        List<Initiative> newTestInitiatives = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestInitiatives.add(generateTestInitiative(i));
        }
        when(mockedInitiativeRepository.findAll()).thenReturn(newTestInitiatives);
        ArrayList<Initiative> initiativesFromService = (ArrayList<Initiative>) initiativeService.findAll();

        verify(mockedInitiativeRepository, atLeastOnce()).findAll();
        Assert.assertEquals(initiativesFromService, newTestInitiatives);
        System.out.println("Вызов из сервиса: " + initiativesFromService);
        System.out.println("Вызов из репозитория: " + newTestInitiatives);
    }

    @Test
    void boardInitiativesTest() {
        Map<UUID, ArrayList<Initiative>> newTestMapInitiatives = new HashMap<>();
        for (int i = 0; i < 3; i++){
            UUID randomUUID = UUID.randomUUID();
            ArrayList<Initiative> newInitiatives = new ArrayList<>();

            for (int j = 0; j < 3; j++){
                Initiative initiative = generateTestInitiative(j);
                newInitiatives.add(initiative);
            }
            newTestMapInitiatives.put(randomUUID, newInitiatives);
        }

        for (UUID uuid: newTestMapInitiatives.keySet()){
            when(mockedInitiativeRepository.boardInitiatives(uuid)).thenReturn(newTestMapInitiatives.get(uuid));
            ArrayList<Initiative> initiativesFromService = (ArrayList<Initiative>) initiativeService.boardThoughts(uuid);
            Assert.assertEquals(initiativesFromService, newTestMapInitiatives.get(uuid));
            System.out.println("Вызов из сервисас uuid " + uuid + ":" + initiativesFromService);
            System.out.println("Вызов из репозиторияс uuid " + uuid + ":" + newTestMapInitiatives.get(uuid));
        }
        verify(mockedInitiativeRepository, atLeast(3)).boardInitiatives(any(UUID.class));
    }

    @Test
    void findByAuthorTest() {
        ArrayList<Initiative> newTestInitiatives = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestInitiatives.add(generateTestInitiative(i));
        }
        when(mockedInitiativeRepository.findByAuthor(any(UUID.class), any(UUID.class))).thenReturn(newTestInitiatives);
        UUID userUUID = UUID.randomUUID();
        UUID boardUUID = UUID.randomUUID();
        ArrayList<Initiative> initiativesFromService = (ArrayList<Initiative>) initiativeService.findByAuthor(userUUID, boardUUID);

        verify(mockedInitiativeRepository, atLeastOnce()).findByAuthor(any(UUID.class), any(UUID.class));
        Assert.assertEquals(initiativesFromService, newTestInitiatives);
        System.out.println("Вызов из сервиса: " + initiativesFromService);
        System.out.println("Вызов из репозитория: " + newTestInitiatives);
    }

    @Test
    void findByIdTest() {
        Map<UUID, Initiative> newTestMapInitiatives = new HashMap<>();
        for (int i = 0; i < 3; i++){
            Initiative newTestInitiative = generateTestInitiative(i);
            newTestMapInitiatives.put(newTestInitiative.getId(), newTestInitiative);
        }

        for (UUID uuid: newTestMapInitiatives.keySet()) {
            when(mockedInitiativeRepository.findById(uuid)).thenReturn(Optional.ofNullable(newTestMapInitiatives.get(uuid)));
            Optional<Initiative> initiativeFromService = (Optional<Initiative>) initiativeService.findById(uuid);

            Assert.assertEquals(initiativeFromService, Optional.of(newTestMapInitiatives.get(uuid)));
            System.out.println("Вызов из сервиса с uuid " + uuid + ":" + initiativeFromService);
            System.out.println("Вызов из репозитория с uuid " + uuid + ":" + newTestMapInitiatives.get(uuid));
        }
        verify(mockedInitiativeRepository, atLeast(3)).findById(any(UUID.class));
    }

    @Test
    void saveTest() {
        Initiative newTestInitiative = initiative1;
        when(mockedInitiativeRepository.save(any(Initiative.class))).thenReturn(newTestInitiative);
        when(mockedInitiativeRepository.getLastNumber(any(UUID.class))).thenReturn(new Random().nextLong());

        Initiative randomInitiative = generateTestInitiative(2);
        Board newBoard = new Board();
        newBoard.setId(UUID.randomUUID());
        randomInitiative.setBoard(newBoard);
        Initiative initiativeFromService = (Initiative) initiativeService.save(randomInitiative);

        Assert.assertEquals(initiativeFromService, newTestInitiative);
        verify(mockedInitiativeRepository, atLeastOnce()).save(any(Initiative.class));
        System.out.println("Вызов из сервиса: " + initiativeFromService);
        System.out.println("Вызов из репозитория: " + newTestInitiative);
    }

    @Test
    void findAllRatedTest() {
        ArrayList<InitiativeRate> newTestRates = new ArrayList<>();

        for (int i = 0; i < 3; i++){
            newTestRates.add(generateTestInitiativeRate(i));
        }
        when(mockedInitiativeRepositoryRate.findAll()).thenReturn(newTestRates);

        ArrayList<InitiativeRate> ratesFromService = (ArrayList<InitiativeRate>)initiativeService.findAllRated();
        verify(mockedInitiativeRepositoryRate, atLeastOnce()).findAll();
        Assert.assertEquals(newTestRates, ratesFromService);
        System.out.println("Вызов из сервиса: " + ratesFromService);
        System.out.println("Вызов из репозитория: " + newTestRates);
    }

    @Test
    void saveRatedTest() {
        InitiativeRate newTestRate = generateTestInitiativeRate(1);
        when(mockedInitiativeRepositoryRate.save(any(InitiativeRate.class))).thenReturn(newTestRate);

        InitiativeRate randomRate = generateTestInitiativeRate(2);
        InitiativeRate rateFromService = (InitiativeRate) initiativeService.saveRated(randomRate);

        verify(mockedInitiativeRepositoryRate, atLeastOnce()).save(any(InitiativeRate.class));
        Assert.assertEquals(rateFromService, newTestRate);
        System.out.println("Вызов из сервиса: " + rateFromService);
        System.out.println("Вызов из репозитория: " + newTestRate);
    }

//    @Test //TODO move to InitiativeIdeasServiceImplTest
//    void getIdeasByUserTest() {
//        InitiativeIdeas testValue = new InitiativeIdeas();
//        Optional<InitiativeIdeas> newTestIdea = Optional.of(testValue);
//        when(mockedInitiativeIdeasRepository.findByUser(any(UUID.class), any(UUID.class))).thenReturn(newTestIdea);
//        UUID userUUID = UUID.randomUUID();
//        UUID boardUUID = UUID.randomUUID();
//        Optional <InitiativeIdeas> ideaFromService = initiativeService.getIdeasByUser(userUUID, boardUUID);
//
//        verify(mockedInitiativeIdeasRepository, atLeastOnce()).findByUser(any(UUID.class), any(UUID.class));
//        Assert.assertEquals(ideaFromService, newTestIdea);
//        System.out.println("Вызов из сервиса: " + ideaFromService);
//        System.out.println("Вызов из репозитория: " + newTestIdea);
//    }

//    @Test //TODO move to InitiativeIdeasServiceImplTest
//    void getByUserAndInitiativeTest() {
//        ArrayList<InitiativeIdeas> newTestIdeas = new ArrayList<>();
//        for (int i = 0; i < 3; i++){
//            newTestIdeas.add(new InitiativeIdeas());
//        }
//        when(mockedInitiativeIdeasRepository.findByUserAndInitiative(any(UUID.class), any(UUID.class))).thenReturn(newTestIdeas);
//        UUID userUUID = UUID.randomUUID();
//        UUID boardUUID = UUID.randomUUID();
//        ArrayList<InitiativeIdeas> ideasFromService = (ArrayList<InitiativeIdeas>) initiativeService.getByUserAndInitiative(userUUID, boardUUID);
//
//        verify(mockedInitiativeIdeasRepository, atLeastOnce()).findByUserAndInitiative(any(UUID.class), any(UUID.class));
//        Assert.assertEquals(ideasFromService, newTestIdeas);
//        System.out.println("Вызов из сервиса: " + ideasFromService);
//        System.out.println("Вызов из репозитория: " + newTestIdeas);
//    }

//    @Test //TODO move to InitiativeIdeasServiceImplTest
//    void saveInitiativeIdeasTest() {
//        InitiativeIdeas newTestIdea = new InitiativeIdeas();
//        when(mockedInitiativeIdeasRepository.save(any(InitiativeIdeas.class))).thenReturn(newTestIdea);
//
//        InitiativeIdeas randomIdea = new InitiativeIdeas();
//        InitiativeIdeas ideaFromService = initiativeService.saveInitiativeIdeas(randomIdea);
//
//        verify(mockedInitiativeIdeasRepository, atLeastOnce()).save(any(InitiativeIdeas.class));
//        Assert.assertEquals(ideaFromService, newTestIdea);
//        System.out.println("Вызов из сервиса: " + ideaFromService);
//        System.out.println("Вызов из репозитория: " + newTestIdea);
//    }

//    @Test //TODO move to InitiativeIdeasServiceImplTest
//    void getInitiativesRateInfoTest() {
//        InfoProjection newTestInfoProjection = mock(InfoProjection.class);
//        when(mockedInitiativeIdeasRepository.getInitiativesRateInfo(any(UUID.class))).thenReturn(newTestInfoProjection);
//        UUID randomInitiativeUUID = UUID.randomUUID();
//        InfoProjection infoProjectionFromService = initiativeService.getInitiativesRateInfo(randomInitiativeUUID);
//
//        Assert.assertEquals(infoProjectionFromService, newTestInfoProjection);
//        System.out.println("Вызов из сервиса: " + infoProjectionFromService);
//        System.out.println("Вызов из репозитория: " + newTestInfoProjection);
//    }

    @Test
    void getAllValidatedForMapTest() {
        ArrayList<MapListSortInfoProjection> newTestMapListSortInfoProjections = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestMapListSortInfoProjections.add(mock(MapListSortInfoProjection.class));
        }
        when(mockedInitiativeRepository.getAllValidatedForMap(any(UUID.class))).thenReturn(newTestMapListSortInfoProjections);
        UUID boardUUID = UUID.randomUUID();
        ArrayList<MapListSortInfoProjection> mapListSortInfoProjectionFromService =
                (ArrayList<MapListSortInfoProjection>) initiativeService.getAllValidatedForMap(boardUUID);

        Assert.assertEquals(mapListSortInfoProjectionFromService, newTestMapListSortInfoProjections);
        verify(mockedInitiativeRepository, atLeastOnce()).getAllValidatedForMap(any(UUID.class));
        System.out.println("Вызов из сервиса: " + mapListSortInfoProjectionFromService);
        System.out.println("Вызов из репозитория: " + newTestMapListSortInfoProjections);
    }

//    @Test //TODO move to InitiativeIdeasServiceImplTest
//    void deleteInitiativeIdeaTest() {
//        Map<UUID, Optional <InitiativeIdeas>> newTestMapIdeas = new HashMap<>();
//        for (int i = 0; i < 3; i++){
//            UUID randomUUID = UUID.randomUUID();
//            InitiativeIdeas initiativeIdeas = new InitiativeIdeas();
//            initiativeIdeas.setInitiative(generateTestInitiative(i));
//            Optional <InitiativeIdeas> newIdea = Optional.of(initiativeIdeas);
//            newIdea.get().setId(randomUUID);
//            newTestMapIdeas.put(randomUUID, newIdea);
//        }
//        for (UUID uuid: newTestMapIdeas.keySet()) {
//            when(mockedInitiativeIdeasRepository.findById(any(UUID.class))).thenReturn(newTestMapIdeas.get(uuid));
//            initiativeIdeasService.deleteInitiativeIdea(newTestMapIdeas.get(uuid).get().getInitiative());
//        }
//        verify(mockedInitiativeIdeasRepository, atLeast(3)).findById(any(UUID.class));
//        verify(mockedInitiativeIdeasRepository, atLeast(3)).delete(any(InitiativeIdeas.class));
//    }

//    @Test
//    void getInitiativeIdeasListTest() {
//        ArrayList<InitiativeIdeasInfoProjection> newTestIdeasInfoProjectionList = new ArrayList<>();
//        for (int i = 0; i < 3; i++){
//            newTestIdeasInfoProjectionList.add(mock(InitiativeIdeasInfoProjection.class));
//        }
//        when(mockedInitiativeIdeasRepository.getInitiativeIdeasList(any(UUID.class))).thenReturn(newTestIdeasInfoProjectionList);
//        UUID initiativeUUID = UUID.randomUUID();
//        ArrayList<InitiativeIdeasInfoProjection> ideasInfoProjectionListFromService =
//                (ArrayList<InitiativeIdeasInfoProjection>) initiativeService.getInitiativeIdeasList(initiativeUUID);
//
//        Assert.assertEquals(ideasInfoProjectionListFromService, newTestIdeasInfoProjectionList);
//        verify(mockedInitiativeIdeasRepository, atLeastOnce()).getInitiativeIdeasList(any(UUID.class));
//        System.out.println("Вызов из сервиса: " + ideasInfoProjectionListFromService);
//        System.out.println("Вызов из репозитория: " + newTestIdeasInfoProjectionList);
//    }

    @Test
    void getRatedByUserTest() {
        ArrayList<InitiativeRate> newTestRatesList = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestRatesList.add(generateTestInitiativeRate(i));
        }
        when(mockedInitiativeRepositoryRate.getRatedByUser(any(UUID.class), any(UUID.class))).thenReturn(newTestRatesList);
        UUID userUUID = UUID.randomUUID();
        UUID boardUUID = UUID.randomUUID();
        ArrayList<InitiativeRate> ratesListFromService = (ArrayList<InitiativeRate>) initiativeService.getRatedByUser(userUUID, boardUUID);

        Assert.assertEquals(ratesListFromService, newTestRatesList);
        verify(mockedInitiativeRepositoryRate, atLeastOnce()).getRatedByUser(any(UUID.class), any(UUID.class));
        System.out.println("Вызов из сервиса: " + ratesListFromService);
        System.out.println("Вызов из репозитория: " + newTestRatesList);
    }

    @Test
    void getMatrixMapTest() {
        Set<BoardMember> newTestMembers = new HashSet<>();
        for (int i = 0; i < 10; i++){
            newTestMembers.add(new BoardMember());
        }
        ArrayList<Integer> newTestRates = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            newTestRates.add((int) (Math.random() * 10 * (new Random().nextBoolean() ? 1 : -1)));
        }
        Initiative newTestInitiative = generateTestInitiative(1);
        Board newBoard = new Board();
        newBoard.setId(UUID.randomUUID());
        newTestInitiative.setBoard(newBoard);
        when(mockedInitiativeRepository.findById(any(UUID.class))).thenReturn(Optional.of(newTestInitiative));
        when(mockedBoardMemberRepository.getMembers(any(UUID.class))).thenReturn(newTestMembers);
        when(mockedInitiativeRepositoryRate.getRatesForMap(any(UUID.class))).thenReturn(newTestRates);
        List<List<String>> resultFromService = initiativeService.getMatrixMap(UUID.randomUUID());
        int notNullColors = 0;
        for (List<String> subMas : resultFromService){
            for (String color: subMas){
                if (color != null){
                    notNullColors++;
                }
            }
        }
        int squareLength = resultFromService.size();
        Assert.assertThat(notNullColors, is(10));
        Assert.assertThat(squareLength, is(4));
        verify(mockedInitiativeRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedBoardMemberRepository, atLeastOnce()).getMembers(any(UUID.class));
        verify(mockedInitiativeRepositoryRate, atLeastOnce()).getRatesForMap(any(UUID.class));
    }

    @Test
    void getByNumberForMapTest() {
        ArrayList<MapListSortInfoProjection> newTestMapListSortInfoProjections = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestMapListSortInfoProjections.add(mock(MapListSortInfoProjection.class));
        }
        when(mockedInitiativeRepository.getByNumberForMap(anyLong(), any(UUID.class))).thenReturn(newTestMapListSortInfoProjections);
        Long number = new Random().nextLong();
        UUID boardUUID = UUID.randomUUID();
        ArrayList<MapListSortInfoProjection> mapListSortInfoProjectionFromService =
                (ArrayList<MapListSortInfoProjection>) initiativeService.getByNumberForMap(number, boardUUID);

        Assert.assertEquals(mapListSortInfoProjectionFromService, newTestMapListSortInfoProjections);
        verify(mockedInitiativeRepository, atLeastOnce()).getByNumberForMap(anyLong(), any(UUID.class));
        System.out.println("Вызов из сервиса: " + mapListSortInfoProjectionFromService);
        System.out.println("Вызов из репозитория: " + newTestMapListSortInfoProjections);
    }

    @Test
    void findByNameContainingForMapTest() {
        ArrayList<MapListSortInfoProjection> newTestMapListSortInfoProjections = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestMapListSortInfoProjections.add(mock(MapListSortInfoProjection.class));
        }
        when(mockedInitiativeRepository.findByNameContainingForMap(anyString(), any(UUID.class))).thenReturn(newTestMapListSortInfoProjections);
        String name = Integer.toString(new Random().nextInt());
        UUID boardUUID = UUID.randomUUID();
        ArrayList<MapListSortInfoProjection> mapListSortInfoProjectionFromService =
                (ArrayList<MapListSortInfoProjection>) initiativeService.findByNameContainingForMap(name, boardUUID);

        Assert.assertEquals(mapListSortInfoProjectionFromService, newTestMapListSortInfoProjections);
        verify(mockedInitiativeRepository, atLeastOnce()).findByNameContainingForMap(anyString(), any(UUID.class));
        System.out.println("Вызов из сервиса: " + mapListSortInfoProjectionFromService);
        System.out.println("Вызов из репозитория: " + newTestMapListSortInfoProjections);
    }

    @Test
    void saveAcceptingRateTest() {
        InitiativeAcceptingRate randomInitiativeAcceptingRate = generateTestInitiativeAcceptingRate(4);
        when(mockedInitiativeAcceptingRateRepository.save(any(InitiativeAcceptingRate.class))).thenReturn(randomInitiativeAcceptingRate);
        InitiativeAcceptingRate acceptingRateFromService = (InitiativeAcceptingRate) initiativeService.saveAcceptingRate(randomInitiativeAcceptingRate);

        Assert.assertEquals(acceptingRateFromService, randomInitiativeAcceptingRate);
        System.out.println("Вызов из сервиса: " + acceptingRateFromService);
        System.out.println("Вызов из репозитория: " + randomInitiativeAcceptingRate);
    }

    @Test
    void acceptInitiativeTest() {
        Initiative newTestInitiative = generateTestInitiative(1);
        when(mockedInitiativeRepository.findById(any(UUID.class))).thenReturn(Optional.of(newTestInitiative));
        when(mockedInitiativeRepository.save(any(Initiative.class))).thenReturn(newTestInitiative);
        initiativeService.acceptThought(UUID.randomUUID());

        verify(mockedInitiativeRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedInitiativeAcceptingRateRepository, atLeastOnce()).deleteByInitiative(any(UUID.class));
    }

    @Test
    void getAcceptingInfoTest() {
        ArrayList<AcceptRateProjection> newTestAcceptRateProjectionList = new ArrayList<>();
        AcceptRateProjection newTestAcceptRateProjection = mock(AcceptRateProjection.class);
        newTestAcceptRateProjectionList.add(newTestAcceptRateProjection);
        when(mockedInitiativeAcceptingRateRepository.getAcceptingInfo(any(UUID.class))).thenReturn(newTestAcceptRateProjectionList);
        AcceptRateProjection acceptRateProjectionFromService = initiativeService.getAcceptingInfo(UUID.randomUUID());

        Assert.assertEquals(acceptRateProjectionFromService, newTestAcceptRateProjection);
        verify(mockedInitiativeAcceptingRateRepository, atLeastOnce()).getAcceptingInfo(any(UUID.class));
        System.out.println("Вызов из сервиса: " + acceptRateProjectionFromService);
        System.out.println("Вызов из репозитория: " + newTestAcceptRateProjection);
    }

    @Test
    void getAvgAcceptRateTest() {
        ArrayList<AverageAcceptRateProjection> newTestAverageAcceptRateProjections = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            newTestAverageAcceptRateProjections.add(mock(AverageAcceptRateProjection.class));
        }
        when(mockedInitiativeAcceptingRateRepository.getAvgAcceptRate(any(UUID.class))).thenReturn(newTestAverageAcceptRateProjections);
        ArrayList<AverageAcceptRateProjection> averageAcceptRateProjectionsFromService = (ArrayList<AverageAcceptRateProjection>) initiativeService.getAvgAcceptRate(UUID.randomUUID());

        Assert.assertEquals(averageAcceptRateProjectionsFromService, newTestAverageAcceptRateProjections);
        verify(mockedInitiativeAcceptingRateRepository, atLeastOnce()).getAvgAcceptRate(any(UUID.class));
    }
    @AfterAll
    public static void tearDown(){
        System.out.println("Тесты завершены");
    }

    private Initiative generateTestInitiative(int i) {
        return new Initiative(
                UUID.randomUUID(), Long.valueOf(i), 0, "first initiative title","Description",
                board, user
        );
    }
    private InitiativeRate generateTestInitiativeRate(int i) {
        return new InitiativeRate(
                UUID.randomUUID(), generateTestInitiative(i), user, 5, 5
        );
    }
    private InitiativeAcceptingRate generateTestInitiativeAcceptingRate(int i) {
        return new InitiativeAcceptingRate(
                UUID.randomUUID(), generateTestInitiative(i), user, 1
        );
    }
}