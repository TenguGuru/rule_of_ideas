package com.tengu.boards;


import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import java.util.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;




public class BoardServiceImplTest {



    private BoardServiceImpl boardService;


    @Mock
    private static BoardSettings boardSettings;

    @Mock
    private static BoardSettings otherBoardSettings;

    @Mock
    private static User user;

    @Mock
    private static BoardSettingsRepository boardSettingsRepository;

    @Mock
    private static BoardRepository repository;
    @Mock
    private static UserRepository userRepository;
    @Mock
    private static BoardMemberRepository boardMemberRepository;
    @Mock
    private static BoardCodeGeneratorImpl boardCodeGenerator;

    @Mock
    private static Board board;

    @Mock
    private static AddBoard addboard;

    @Mock
    private static BoardQuota boardQuota;

    @Mock
    private static BoardMember boardMember;

    @Mock
    private static ChangeMemberRole changeMemberRole;


    private Set<BoardMember> boardMemberSet = new Set<BoardMember>() {
        @Override
        public int size() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<BoardMember> iterator() {
            return (Iterator<BoardMember>) boardMember;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(BoardMember boardMember) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends BoardMember> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };


    private UUID boardId = UUID.randomUUID();
    private UUID userId = UUID.randomUUID();
    private UUID id = UUID.randomUUID();
    private UUID creatorId = UUID.randomUUID();


    private static Iterable<Board> boardIterable = new Iterable<Board>() {
        @Override
        public Iterator<Board> iterator() {
            return (Iterator<Board>) board;
        }
    };


    @BeforeEach
    public void setUp(){

        when(board.getCode()).thenReturn("12");
        when(board.getId()).thenReturn(boardId);

        when(addboard.getBoardCode()).thenReturn("12");
        when(addboard.getUserId()).thenReturn(UUID.randomUUID());
        when(addboard.getRole()).thenReturn("admin");

        when(changeMemberRole.getMemberId()).thenReturn(id);

        when(repository.getLastNumber()).thenReturn(1L);
        when(repository.getByCode(addboard.getBoardCode())).thenReturn(Optional.of(board));
        when(repository.getByName("Adolf")).thenReturn(Optional.of(board));
        when(repository.findById(boardId)).thenReturn(Optional.of(board));
        when(repository.getCreator(boardId)).thenReturn(user);
        when(repository.getBoardQuota(boardId)).thenReturn(boardQuota);
        when(repository.save(board)).thenReturn(board);

        when(boardMemberRepository.findMember(userId,boardId)).thenReturn(boardMember);
        when(boardMemberRepository.findById(changeMemberRole.getMemberId())).thenReturn(Optional.of(boardMember));
        when(boardMemberRepository.save(boardMemberRepository.findById(changeMemberRole.getMemberId()).get())).thenReturn(boardMember);

        when(boardMemberRepository.getBoards(creatorId)).thenReturn(boardIterable);
        when(boardMemberRepository.getBoards(boardId)).thenReturn(boardIterable);
        when(boardMemberRepository.getMembers(boardId)).thenReturn(boardMemberSet);

        when(userRepository.findById(addboard.getUserId())).thenReturn(Optional.of(user));

        when(boardSettings.isBidding()).thenReturn(true);
        when(boardSettingsRepository.getBoardSettings(boardId)).thenReturn(Optional.of(boardSettings));



        boardService = new BoardServiceImpl(repository,userRepository,boardMemberRepository,boardSettingsRepository);

    }

    @BeforeAll
    public static void globalSetUp() {
        repository = mock(BoardRepository.class);

        user = mock(User.class);

        userRepository = mock(UserRepository.class);
        boardMemberRepository = mock(BoardMemberRepository.class);
        boardCodeGenerator = mock(BoardCodeGeneratorImpl.class);
        board = mock(Board.class);
        boardCodeGenerator = new BoardCodeGeneratorImpl(repository);
        addboard = mock(AddBoard.class);
        boardQuota = mock(BoardQuota.class);
        boardMember = mock(BoardMember.class);
        changeMemberRole = mock(ChangeMemberRole.class);
        boardSettingsRepository = mock(BoardSettingsRepository.class);
        boardSettings = mock(BoardSettings.class);

        otherBoardSettings = mock(BoardSettings.class);


    }

    @Test
    public void getLastNumberTest(){

        assertNotNull(boardService.getLastNumber());
        assertEquals(Optional.of(boardService.getLastNumber()), Optional.of(1L));

    }

    @Test
    public void addBoardSuccessfully(){

        assertNotNull((boardService.addBoard(addboard)));
        verify(repository, atLeastOnce()).getByCode(addboard.getBoardCode());
        verify(boardMemberRepository,atLeastOnce()).save(boardMember);


    }

    @Test
    public void saveBoardSuccessfully(){

        assertNotNull(boardService.save(board));
        verify(repository, atLeastOnce()).save(board);

        assertEquals(boardService.save(board),board);

    }

    @Test
    public void findByCreatorTest(){

        assertNotNull(boardService.findByCreator(creatorId));
        verify(repository, atLeastOnce()).findByCreator(creatorId);

    }

    @Test
    public void getMembersTest(){

        assertNotNull(boardService.getMembers(boardId));
        verify(boardMemberRepository, atLeastOnce()).getMembers(boardId);

    }


    @Test
    public void getBoardsTest(){

        assertNotNull(boardService.getBoards(userId));
        verify(boardMemberRepository, atLeastOnce()).getBoards(userId);

    }

    @Test
    public void findByNameTest(){

        assertNotNull(boardService.findByName("Adolf"));
        assertEquals(boardService.findByName("Adolf"),Optional.of(board));
        verify(repository, atLeastOnce()).getByName("Adolf");

    }


    @Test
    public void getByCodeTest(){

        assertNotNull(boardService.getByCode(addboard.getBoardCode()));
        assertEquals(boardService.getByCode(addboard.getBoardCode()),Optional.of(board));
        verify(repository,atLeastOnce()).getByCode(addboard.getBoardCode());


    }

    @Test
    public void findByIdTest(){

        assertNotNull(boardService.findById(boardId));
        assertEquals(boardService.findById(boardId),Optional.of(board));
        verify(repository, atLeastOnce()).findById(boardId);

    }


    @Test
    public void getCreateTest(){

        assertNotNull(boardService.getCreator(boardId));
        assertEquals(boardService.getCreator(boardId),user);
        verify(repository, atLeastOnce()).getCreator(boardId);

    }


    @Test
    public void getBoardQuota(){

        assertNotNull(boardService.getBoardQuota(boardId));
        assertEquals(boardService.getBoardQuota(boardId),boardQuota);
        verify(repository, atLeastOnce()).getBoardQuota(boardId);

    }

    @Test
    public void findMember(){

        assertNotNull(boardService.findMember(userId,boardId));
        assertEquals(boardService.findMember(userId,boardId),boardMember);
        verify(boardMemberRepository, atLeastOnce()).findMember(userId,boardId);

    }

    @Test
    public void changeMemberRole(){

        assertNotNull(boardService.changeMemberRole(changeMemberRole));
        assertEquals(boardService.changeMemberRole(changeMemberRole),boardMember);
        verify(boardMemberRepository, atLeastOnce()).findById(changeMemberRole.getMemberId());
        verify(boardMemberRepository, atLeastOnce()).save(boardMember);

    }

    @Test
    public void getBiddingStatusTest(){

        assertNotNull(boardService.getBiddingStatus(boardId));
        assertEquals(boardService.getBiddingStatus(boardId),true);
        verify(boardSettingsRepository, atLeastOnce()).getBoardSettings(boardId);

    }

    @Test
    public void updateBiddingNotNullBoardTest(){

        when(boardSettings.getBoard()).thenReturn(board);
        assertNotNull(boardService.updateBidding(boardSettings));

        verify(boardSettingsRepository, atLeastOnce()).getBoardSettings(boardId);




    }

    @Test
    public void updateBiddingTest(){
        when(boardSettingsRepository.getBoardSettings(boardId)).thenReturn(Optional.empty());
        when(boardSettingsRepository.save(any())).thenReturn(boardSettings);
        Assertions.assertNotNull(boardService.updateBidding(boardSettings));
        verify(boardSettingsRepository, atLeastOnce()).save(argThat(argument -> argument.isBidding()));

    }

}
