package com.tengu.boards;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ВoardCodeGeneratorImplTest {

    @Mock
    private static BoardRepository mockedBoardRepository;
    private BoardCodeGenerator boardCodeGenerator;

    @BeforeEach
    public void setUp(){
        boardCodeGenerator = new BoardCodeGeneratorImpl(mockedBoardRepository);
        System.out.println("Перенастройка перед запуском задания");
    }

    @BeforeAll
    public static void globalSetUp() {
        mockedBoardRepository = mock(BoardRepository.class);
        when(mockedBoardRepository.getByCode(anyString())).thenReturn(Optional.empty());
        System.out.println("Настройка перед запуском класса");
    }

    @Test
    public void resultCodeIsNotRepeated(){
        System.out.println("Начало проверки на повторяемость кодов");
        String code1 = boardCodeGenerator.generateCode();
        String code2 = boardCodeGenerator.generateCode();
        assertNotEquals(code1, is(code2));
        System.out.println("Текущий код 1: " + code1);
        System.out.println("Текущий код 2: " + code2);
        verify(mockedBoardRepository, atLeastOnce()).getByCode(anyString());
        System.out.println("Проверка успешно завершена");
    }

    @Test
    public void contains3Digits3Letters(){
        System.out.println("Начало проверки на состав кода");
        String code = boardCodeGenerator.generateCode();
        int digitsCount = 0;
        int lettersCount = 0;
        for (int i = 0; i < code.length(); ++i){
            if (Character.isLetter(code.charAt(i))){
                lettersCount++;
            }
            if (Character.isDigit(code.charAt(i))){
                digitsCount++;
            }
        }

        assertThat(lettersCount, is(3));
        assertThat(digitsCount, is(3));
        System.out.println("Текущий код: " + code);
        verify(mockedBoardRepository, atLeastOnce()).getByCode(anyString());
        System.out.println("Проверка успешно завершена");
    }

    @AfterAll
    public static void tearDown() {
        System.out.println("Тесты завершены");
    }
}
