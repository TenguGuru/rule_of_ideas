package com.tengu.party;

import com.tengu.boards.Board;
import com.tengu.boards.BoardMemberRepository;
import com.tengu.boards.BoardSettingsService;
import com.tengu.commonProjections.InfoProjection;
import com.tengu.parties.entities.*;
import com.tengu.parties.projections.MatchingOfRates;
import com.tengu.parties.projections.PartyInfoProjection;
import com.tengu.parties.repositories.*;
import com.tengu.parties.services.PartyServiceImpl;
import com.tengu.thoughts.entities.*;
import com.tengu.thoughts.services.IdeaService;
import com.tengu.thoughts.services.InitiativeService;
import com.tengu.users.User;
import com.tengu.users.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PartyServiceImplTest {

    private PartyServiceImpl partyService;

    @Mock
    private static PartyRepository mockedPartyRepository;
    @Mock
    private static UserRepository mockedUserRepository;
    @Mock
    private static PartyMembersRepository mockedPartyMembersRepository;
    @Mock
    private static PartyElectRepository mockedPartyElectRepository;
    @Mock
    private static BoardMemberRepository mockedBoardMemberRepository;
    @Mock
    private static BoardSettingsService mockedBoardSettingsService;
    @Mock
    private static PartyBanRepository mockedBanRepository;
    @Mock
    private static IdeaService mockedIdeaService;
    @Mock
    private static InitiativeService mockedInitiativeService;
    @Mock
    private static PartyIdeasRepository mockedPartyIdeasRepository;
    @Mock
    private static PartyInitiativesRepository mockedPartyInitiativesRepository;

    @Mock
    private static Party party;
    @Mock
    private static Board board;
    @Mock
    private static User leader;
    @Mock
    private static User creator;
    @Mock
    private static User user;
    @Mock
    private static PartyMembers partyMember;
    @Mock
    private static PartyElect partyElect;
    @Mock
    private static PartyBan partyBan;
    @Mock
    private static PartyIdeas partyIdea;
    @Mock
    private static PartyInitiatives partyInitiative;



    private static UUID boardId = UUID.randomUUID();
    private static UUID partyId = UUID.randomUUID();
    private static UUID userId = UUID.randomUUID();
    private static UUID voterId = UUID.randomUUID();
    private static UUID partyMemberId = UUID.randomUUID();

    private static Iterable <Idea> ideaIterable = new Iterable <Idea>(){
        @Override
        public Iterator<Idea> iterator() { return (Iterator<Idea>) partyIdea; }
    };

    private static Iterable <Initiative> initiativeIterable = new Iterable<Initiative>() {
        @Override
        public Iterator<Initiative> iterator() { return (Iterator<Initiative>) partyInitiative; }
    };

    private static Iterable<PartyBan> partyBanIterable = new Iterable<PartyBan>(){
        @Override
        public Iterator<PartyBan> iterator() { return (Iterator<PartyBan>) partyBan; }
    };

    private static Iterable<Party> partyIterable = new Iterable<Party>() {
        @Override
        public Iterator<Party> iterator() { return (Iterator<Party>) party; }
    };

    private static Iterable<PartyMembers> partyMembersIterable = new Iterable<PartyMembers>() {
        @Override
        public Iterator<PartyMembers> iterator() { return (Iterator<PartyMembers>) partyMember; }
    };

    @BeforeAll
    public static void globalSetUp(){

        mockedPartyRepository = mock(PartyRepository.class);
        mockedUserRepository = mock(UserRepository.class);
        mockedPartyMembersRepository = mock(PartyMembersRepository.class);
        mockedPartyElectRepository = mock(PartyElectRepository.class);
        mockedBanRepository = mock(PartyBanRepository.class);
        mockedPartyIdeasRepository = mock(PartyIdeasRepository.class);
        mockedPartyInitiativesRepository = mock(PartyInitiativesRepository.class);
        mockedBoardMemberRepository = mock(BoardMemberRepository.class);
        mockedIdeaService = mock(IdeaService.class);
        mockedInitiativeService = mock(InitiativeService.class);
        mockedBoardSettingsService = mock(BoardSettingsService.class);
    }

    @BeforeEach
    public void setUp(){
        partyService = new PartyServiceImpl(mockedPartyRepository, mockedPartyMembersRepository, mockedPartyElectRepository, mockedUserRepository,
                mockedBanRepository, mockedPartyIdeasRepository, mockedPartyInitiativesRepository,
                mockedBoardMemberRepository, mockedBoardSettingsService, mockedIdeaService, mockedInitiativeService);
        board = new Board();
        leader = new User();
        creator = new User();
        party = new Party();
        partyMember = new PartyMembers();
        user = new User();
        partyBan = new PartyBan();
        partyIdea = new PartyIdeas();
        partyInitiative = new PartyInitiatives();
        party.setId(partyId);
        board.setId(boardId);
        party.setBoard(board);
    }

    @Test
    public void findAllTest(){
        when(mockedPartyRepository.findAll()).thenReturn(partyIterable);

        assertNotNull(partyService.findAll());
        verify(mockedPartyRepository, atLeastOnce()).findAll();
    }

    @Test
    public void boardPartiesTest(){
        when(mockedPartyRepository.boardParty(any(UUID.class))).thenReturn(partyIterable);

        assertNotNull(partyService.boardParties(boardId));
        verify(mockedPartyRepository, atLeastOnce()).boardParty(any(UUID.class));
    }

    @Test
    public void findByIdTest(){
        when(mockedPartyRepository.findById(any(UUID.class))).thenReturn(Optional.of(party));
        assertNotNull(partyService.findById(partyId));

        assertEquals(partyService.findById(partyId), Optional.of(party));
        verify(mockedPartyRepository, atLeastOnce()).findById(any(UUID.class));
    }

    @Test
    public void saveTest(){
        when(mockedPartyRepository.save(any(Party.class))).thenReturn(party);
        Party testParty = partyService.save(party);
        assertNotNull(testParty);

        assertEquals(testParty, party);
        verify(mockedPartyRepository, atLeastOnce()).save(any(Party.class));

    }

    @Test
    public void joinTest(){
        when(mockedPartyRepository.findById(any(UUID.class))).thenReturn(Optional.of(party));
        when(mockedUserRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(mockedBanRepository.findByUserAndParty(any(UUID.class), any(UUID.class))).thenReturn(partyBan);

        PartyBan partyBanFromService = partyService.join(partyId, userId);

        verify(mockedPartyRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedUserRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedBanRepository, atLeastOnce()).findByUserAndParty(any(UUID.class), any(UUID.class));
        assertEquals(partyBanFromService, partyBan);
    }

    @Test
    public void joinWhenPartyBanNullAndLastPartyHasNoMembersTest(){
        partyMember = new PartyMembers();
        party = new Party();
        board = new Board();
        party.setId(partyId);
        partyMember.setParty(party);
        board.setId(boardId);
        party.setBoard(board);

        ArrayList<PartyMembers> noMembers = new ArrayList<PartyMembers>();

        when(mockedPartyRepository.findById(any(UUID.class))).thenReturn(Optional.of(party));
        when(mockedUserRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(mockedBanRepository.findByUserAndParty(any(UUID.class), any(UUID.class))).thenReturn(null);
        when(mockedPartyMembersRepository.getPartyMemberByUser(any(UUID.class), any(UUID.class))).thenReturn(partyMember);
        when(mockedPartyMembersRepository.getPmForDelete(any(UUID.class), any(UUID.class))).thenReturn(noMembers);

        PartyBan partyBanFromService = partyService.join(partyId, userId);

        verify(mockedPartyRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedUserRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedBanRepository, atLeastOnce()).findByUserAndParty(any(UUID.class), any(UUID.class));
        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMemberByUser(any(UUID.class), any(UUID.class));
        verify(mockedPartyMembersRepository, atLeastOnce()).save(any(PartyMembers.class));
        verify(mockedUserRepository, atLeastOnce()).save(any(User.class));
        verify(mockedPartyMembersRepository, atLeastOnce()).getPmForDelete(any(UUID.class), any(UUID.class));
        verify(mockedPartyRepository, atLeastOnce()).delete(any(Party.class));

        assertNull(partyBanFromService);
    }

    @Test
    public void deleteTest(){
        ArrayList<PartyMembers> pms = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            pms.add(new PartyMembers());
        }

        party = new Party();
        party.setId(partyId);
        when(mockedPartyMembersRepository.getPartyMembers(any(UUID.class))).thenReturn(pms);
        partyService.delete(party);

        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMembers(any(UUID.class));
        verify(mockedPartyMembersRepository, atLeast(3)).delete(any(PartyMembers.class));
        verify(mockedPartyRepository, atLeastOnce()).delete(any(Party.class));
    }

    @Test
    public void updateTest(){
        party = new Party();
        party.setId(partyId);
        when(mockedPartyRepository.findById(party.getId())).thenReturn(Optional.of(party));
        when(mockedPartyRepository.save(any(Party.class))).thenReturn(party);

        Party partyFromService = partyService.update(party);

        verify(mockedPartyRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedPartyRepository, atLeastOnce()).save(any(Party.class));

        assertEquals(partyFromService, party);
    }

    @Test
    public void getPartyInfoTest(){
        PartyInfoProjection testPartyInfo = mock(PartyInfoProjection.class);
        when(mockedPartyRepository.getPartyInfo(any(UUID.class))).thenReturn(testPartyInfo);

        PartyInfoProjection partyInfoFromService = partyService.getPartyInfo(partyId);
        assertEquals(partyInfoFromService, testPartyInfo);
        verify(mockedPartyRepository, atLeastOnce()).getPartyInfo(any(UUID.class));
    }

    @Test
    public void getPartyMembersTest(){
        when(mockedPartyMembersRepository.getPartyMembers(any(UUID.class))).thenReturn(partyMembersIterable);

        assertNotNull(partyService.getPartyMembers(partyId));
        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMembers(any(UUID.class));
    }

    @Test
    public void savePartyMemberTest(){
        when(mockedPartyMembersRepository.save(any(PartyMembers.class))).thenReturn(partyMember);
        PartyMembers partyMemberTest = partyService.savePartyMember(partyMember);
        assertNotNull(partyMemberTest);
        assertEquals(partyMemberTest, partyMember);
        verify(mockedPartyMembersRepository, atLeastOnce()).save(any(PartyMembers.class));
    }


    @Test
    public void savePartyElectTest(){
        UUID candidateId = UUID.randomUUID();
        partyElect = new PartyElect();

        when(mockedPartyElectRepository.getByVoter(any(UUID.class))).thenReturn(partyElect);
        when(mockedPartyMembersRepository.findById(any(UUID.class))).thenReturn(Optional.of(partyMember));
        when(mockedPartyElectRepository.save(any(PartyElect.class))).thenReturn(partyElect);

        PartyElect partyElectFromService = partyService.savePartyElect(voterId, candidateId);

        verify(mockedPartyElectRepository, atLeastOnce()).getByVoter(any(UUID.class));
        verify(mockedPartyMembersRepository, times(1)).findById(any(UUID.class));
        verify(mockedPartyElectRepository, atLeastOnce()).save(any(PartyElect.class));

        assertEquals(partyElectFromService, partyElect);

        reset(this.mockedPartyMembersRepository);
    }

    @Test
    public void savePartyElectNullTest(){
        UUID voterId = UUID.randomUUID();
        UUID candidateId = UUID.randomUUID();
        partyElect = new PartyElect();

        when(mockedPartyElectRepository.getByVoter(any(UUID.class))).thenReturn(null);
        when(mockedPartyMembersRepository.findById(any(UUID.class))).thenReturn(Optional.of(partyMember));
        when(mockedPartyElectRepository.save(any(PartyElect.class))).thenReturn(partyElect);

        PartyElect partyElectFromService = partyService.savePartyElect(voterId, candidateId);

        verify(mockedPartyElectRepository, atLeastOnce()).getByVoter(any(UUID.class));
        verify(mockedPartyMembersRepository, times(2)).findById(any(UUID.class));
        verify(mockedPartyElectRepository, atLeastOnce()).save(any(PartyElect.class));

        assertEquals(partyElectFromService, partyElect);

        // делаем ресет, так как репозиторий нужен будет в другом тесте
        reset(this.mockedPartyMembersRepository);
    }

    @Test
    public void getPartyElectTest(){
        when(mockedPartyElectRepository.getByVoter(any(UUID.class))).thenReturn(partyElect);
        PartyElect partyElectFromService = partyService.getPartyElect(voterId);

        verify(mockedPartyElectRepository, atLeastOnce()).getByVoter(any(UUID.class));
        assertEquals(partyElectFromService, partyElect);
    }

    @Test
    public void deletePartyElectTest(){
        partyService.deletePartyElect(UUID.randomUUID());
        verify(mockedPartyElectRepository, atLeastOnce()).deleteById(any(UUID.class));
    }

    @Test
    public void getMatrixMapTest(){
        board = new Board();
        board.setId(boardId);

        partyService.getMatrixMap(board);

        verify(mockedBoardMemberRepository, atLeastOnce()).getMembers(board.getId());
        verify(mockedPartyRepository, atLeastOnce()).getAllPartyColors(board.getId());

    }

    @Test
    public void exitFromPartyTest(){
        partyMember = new PartyMembers();
        partyMember.setId(partyMemberId);
        partyMember.setMember(user);
        partyMember.setParty(party);
        when(mockedPartyMembersRepository.getPartyMemberByUser(any(UUID.class), any(UUID.class))).thenReturn(partyMember);
        when(mockedPartyMembersRepository.getPartyMembersCount(partyId)).thenReturn(0);

        partyService.exitFromParty(userId, boardId);

        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMemberByUser(any(UUID.class), any(UUID.class));
        verify(mockedPartyElectRepository, atLeastOnce()).deleteByPartyMember(partyMember.getId());
        verify(mockedPartyMembersRepository, atLeastOnce()).delete(any(PartyMembers.class));
        verify(mockedUserRepository, atLeastOnce()).save(partyMember.getMember());
    }

    @Test
    public void banTest(){
        partyBan = new PartyBan();
        user = new User();
        party = new Party();
        board = new Board();
        board.setId(boardId);
        party.setBoard(board);
        party.setId(partyId);
        user.setId(userId);
        partyBan.setUser(user);
        partyBan.setParty(party);
        partyMember = new PartyMembers();
        partyMember.setParty(party);

        when(mockedPartyMembersRepository.getPartyMemberByUser(partyBan.getUser().getId(), partyBan.getParty().getBoard().getId())).thenReturn(partyMember);

        partyService.ban(partyBan);

        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMemberByUser(partyBan.getUser().getId(), partyBan.getParty().getBoard().getId());
        verify(mockedUserRepository, atLeastOnce()).save(partyBan.getUser());
        verify(mockedPartyMembersRepository, atLeastOnce()).delete(any(PartyMembers.class));
        verify(mockedBanRepository, atLeastOnce()).save(any(PartyBan.class));
    }

    @Test
    public void banNullTest(){
        partyBan = new PartyBan();
        user = new User();
        party = new Party();
        board = new Board();
        board.setId(boardId);
        party.setBoard(board);
        user.setId(userId);
        partyBan.setUser(user);
        partyBan.setParty(party);

        when(mockedPartyMembersRepository.getPartyMemberByUser(partyBan.getUser().getId(), partyBan.getParty().getBoard().getId())).thenReturn(null);

        partyService.ban(partyBan);

        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMemberByUser(partyBan.getUser().getId(), partyBan.getParty().getBoard().getId());
        verify(mockedBanRepository, atLeastOnce()).save(any(PartyBan.class));
    }

    @Test
    public void deleteBanTest(){
        when(mockedBanRepository.findByUserAndParty(any(UUID.class), any(UUID.class))).thenReturn(partyBan);

        partyService.deleteBan(userId, partyId);

        verify(mockedBanRepository, atLeastOnce()).findByUserAndParty(any(UUID.class), any(UUID.class));
        verify(mockedBanRepository, atLeastOnce()).delete(any(PartyBan.class));
    }

    @Test
    public void getBannedTest(){
        when(mockedBanRepository.getBanned(any(UUID.class))).thenReturn(partyBanIterable);

        assertNotNull(partyService.getBanned(partyId));
        verify(mockedBanRepository, atLeastOnce()).getBanned(any(UUID.class));
    }

    @Test
    public void getPartyMemberTest(){
        when(mockedPartyMembersRepository.getPartyMemberByUser(any(UUID.class), any(UUID.class))).thenReturn(partyMember);

        assertEquals(partyService.getPartyMember(userId, boardId), partyMember);
        verify(mockedPartyMembersRepository, atLeastOnce()).getPartyMemberByUser(any(UUID.class), any(UUID.class));
    }

    @Test
    public void getPartyRatesMatchingTest(){
        int userIdeaCommonRate = 2;
        int userInitiativeCommonRate = 3;
        int partyIdeaCommonRate = 1;
        int partyInitiativeCommonRate = 1;


        ArrayList<IdeaRate> userIdeaRates = generateIdeaRates(userIdeaCommonRate);
        ArrayList<InitiativeRate> userInitiativeRates = generateInitiativeRates(userInitiativeCommonRate);

        InfoProjection ideaPartyRateInfo = mock(InfoProjection.class);
        when(ideaPartyRateInfo.getRate()).thenReturn(Double.valueOf(partyIdeaCommonRate));
        when(mockedIdeaService.getThoughtPartyRateInfo(any(UUID.class), any(UUID.class))).thenReturn(ideaPartyRateInfo);

        InfoProjection initiativePartyRateInfo = mock(InfoProjection.class);
        when(initiativePartyRateInfo.getRate()).thenReturn(Double.valueOf(partyInitiativeCommonRate));
        when(mockedInitiativeService.getThoughtPartyRateInfo(any(UUID.class), any(UUID.class))).thenReturn(initiativePartyRateInfo);

        when(mockedPartyRepository.findById(any(UUID.class))).thenReturn(Optional.of(party));


        when(mockedIdeaService.getRatedByUser(userId, boardId)).thenReturn(userIdeaRates);
        when(mockedInitiativeService.getRatedByUser(userId, boardId)).thenReturn(userInitiativeRates);

        MatchingOfRates partyRatesMatching = partyService.getPartyRatesMatching(userId, partyId);

        verify(mockedPartyRepository, atLeastOnce()).findById(any(UUID.class));
        verify(mockedIdeaService, atLeastOnce()).getRatedByUser(any(UUID.class), any(UUID.class));
        verify(mockedInitiativeService, atLeastOnce()).getRatedByUser(any(UUID.class), any(UUID.class));

        assertEquals(partyRatesMatching.getIdeaMatch(), userIdeaCommonRate - partyIdeaCommonRate, 0.01);
        assertEquals(partyRatesMatching.getInitiativeMatch(), userInitiativeCommonRate - partyInitiativeCommonRate, 0.01);
    }

    private ArrayList<InitiativeRate> generateInitiativeRates(int userInitiativeCommonRate) {
        ArrayList<InitiativeRate> userInitiativeRates = new ArrayList<>();

        for(int i = 0; i < 3; ++i){
            InitiativeRate rate = new InitiativeRate();
            Initiative initiative = new Initiative();
            UUID initiativeId = UUID.randomUUID();
            initiative.setId(initiativeId);
            rate.setThought(initiative);
            rate.setUserRate(userInitiativeCommonRate);
            userInitiativeRates.add(rate);
        }
        return userInitiativeRates;
    }

    private ArrayList<IdeaRate> generateIdeaRates(int userIdeaCommonRate) {
        ArrayList<IdeaRate> userIdeaRates = new ArrayList<>();
        for(int i = 0; i < 3; ++i){
            IdeaRate rate = new IdeaRate();
            Idea idea = new Idea();
            UUID ideaId = UUID.randomUUID();
            idea.setId(ideaId);
            rate.setThought(idea);
            rate.setUserRate(userIdeaCommonRate);
            userIdeaRates.add(rate);
        }
        return userIdeaRates;
    }

    @Test
    public void savePartyIdeaTest(){
        when(mockedPartyIdeasRepository.save(any(PartyIdeas.class))).thenReturn(partyIdea);

        assertEquals(partyService.savePartyIdea(partyIdea), partyIdea);
        verify(mockedPartyIdeasRepository, atLeastOnce()).save(any(PartyIdeas.class));
    }

    @Test
    public void savePartyInitiativeTest(){
        when(mockedPartyInitiativesRepository.save(any(PartyInitiatives.class))).thenReturn(partyInitiative);

        assertEquals(partyService.savePartyInitiative(partyInitiative), partyInitiative);
        verify(mockedPartyInitiativesRepository, atLeastOnce()).save(any(PartyInitiatives.class));
    }

    @Test
    public void getPartyByIdeaTest(){
        when(mockedPartyIdeasRepository.getPartyByThought((any(UUID.class)))).thenReturn(party);

        assertEquals(partyService.getPartyByIdea(UUID.randomUUID()), party);
        verify(mockedPartyIdeasRepository, atLeastOnce()).getPartyByThought(any(UUID.class));
    }

    @Test
    public void getPartyByInitiativeTest(){
        when(mockedPartyInitiativesRepository.getPartyByThought(any(UUID.class))).thenReturn(party);

        assertEquals(partyService.getPartyByInitiative(UUID.randomUUID()), party);
        verify(mockedPartyInitiativesRepository, atLeastOnce()).getPartyByThought(any(UUID.class));
    }

    @Test
    public void getPartyIdeasTest(){
        when(mockedPartyIdeasRepository.getPartyThoughts((any(UUID.class)))).thenReturn(ideaIterable);

        assertNotNull(partyService.getPartyIdeas(partyId));
        verify(mockedPartyIdeasRepository, atLeastOnce()).getPartyThoughts(any(UUID.class));
    }

    @Test
    public void getPartyInitiativesTest(){
        when(mockedPartyInitiativesRepository.getPartyThoughts(any(UUID.class))).thenReturn(initiativeIterable);

        assertNotNull(partyService.getPartyInitiatives(partyId));
        verify(mockedPartyInitiativesRepository, atLeastOnce()).getPartyThoughts(any(UUID.class));
    }

}
